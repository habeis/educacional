<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $usuario->id !!}</p>
</div>

<!-- Senha Field -->
<div class="form-group">
    {!! Form::label('senha', 'Senha:') !!}
    <p>{!! $usuario->senha !!}</p>
</div>

<!-- Login Field -->
<div class="form-group">
    {!! Form::label('login', 'Login:') !!}
    <p>{!! $usuario->login !!}</p>
</div>

<!-- Pessoa Id Field -->
<div class="form-group">
    {!! Form::label('pessoa_id', 'Pessoa Id:') !!}
    <p>{!! $usuario->pessoa_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $usuario->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $usuario->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $usuario->deleted_at !!}</p>
</div>

