@extends('layouts.app')

@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>{!! $tituloUp !!}</h3>
                <small></small>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="pesquisar...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                Buscar!
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="container col-md-5 col-sm-5 col-xs-5">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>
                            {{ $subTitulo }}
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </li>
                            <li>
                                <a class="close-link" href="{!! route('home') !!}">
                                    <i class="fa fa-close"/></i>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        @yield('content-center')

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection