<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            @include('layouts.menu.gm-menu-items', ['items' => $menu->roots()])
        </ul>
    </div>
</div>