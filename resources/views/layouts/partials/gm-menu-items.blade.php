@foreach($items as $item)
    <li><a><i class="fa fa-bug"></i> {!! $item->title !!} <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu" data-id="{{ Route::current()->getParameter('id') }}">
        <!--<ul class="nav child_menu" style="display: none">-->
            @if($item->hasChildren())
                @include('layouts.menu.gm-menu-items-li', ['items' => $item->children()])
            @endif
        </ul>
    </li>
@endforeach
