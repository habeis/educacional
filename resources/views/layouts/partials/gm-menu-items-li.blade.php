@foreach($items as $item)
    <li@lm-attrs($item) @lm-endattrs><a href="{!! $item->url() !!}">{!! $item->title !!}</a>
    </li>
    @if($item->hasChildren())
        @include('layouts.menu.gm-menu-items-li', ['items' => $item->children()])
    @endif
@endforeach