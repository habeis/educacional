@extends('layouts.app-auth')

@section('content')
<div id="login" class="animate form">
	<section class="login_content">
		@if (session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif


        @if (count($errors) > 0)
            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong>Ops! Problemas!</strong>
                <br /><br />
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif

		<form class="form-horizontal" role="form" method="POST" action="{{ route('password.email.post') }}">
			{!! csrf_field() !!}

            <h1>Redefinir Senha</h1>
			<div class="form-group">
				<div class="col-xs-12">
					<input type="email" placeholder="Seu email" class="form-control" name="email" value="{{ old('email') }}">
				</div>
			</div>

			<div class="form-group">
				<div class="col-xs-12">
					<button type="submit" class="btn btn-primary">
						Enviar Link
					</button>
				</div>
			</div>
		</form>
	</section>
</div>
@endsection
