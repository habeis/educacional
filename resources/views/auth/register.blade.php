@extends('layouts.app-auth')

@section('content')
<div class="animate form">
  <section class="login_content">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('register.post') }}">
      {!! csrf_field() !!}
      <h1>Criar Conta</h1>
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <div class="col-md-12-md-6">
              <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}">

              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

          <div class="col-md-12-md-6">
              <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

          <div class="col-md-12-md-6">
              <input type="password" class="form-control" placeholder="Senha" name="password">

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
          <div class="col-md-12-md-6">
              <input type="password" class="form-control" placeholder="Repita a senha" name="password_confirmation">

              @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      <div class="form-group">
          <div class="col-xs-12">
              <button type="submit" class="btn btn-primary">
                  <i class="fa fa-btn fa-user"></i> Salvar
              </button>
          </div>
      </div>
      <div class="clearfix"></div>
      <div class="separator">

        <p class="change_link">Já é membro?
          <a href="{{ route('login') }}" class="to_register"> Entrar </a>
        </p>
        <div class="clearfix"></div>
        <br />
        <div>
          <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Educacional!</h1>

          <p>©2016 All Rights Reserved. Educacional. Privacy and Terms</p>
        </div>
      </div>
    </form>
    <!-- form -->
  </section>
  <!-- content -->
</div>
@endsection
