@extends('layouts.app-auth')

@section('content')
<div class="animate form">

	<section class="login_content x_content">
		@if (count($errors) > 0)
			<div class="alert alert-warning alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<strong>Ops! Problemas!</strong>
				<br /><br />
				@foreach ($errors->all() as $error)
					<div>{{ $error }}</div>
				@endforeach
			</div>
		@endif
		<form class="form-horizontal" role="form" method="POST" action="{{ route('password.reset.post') }}">
			{!! csrf_field() !!}

			<h1>Redefinir Senha</h1>
			<input type="hidden" name="token" value="{{ $token }}">

			<div class="form-group">
				<div class="col-md-12-md-6">
					<input type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12-md-6">
					<input type="password" placeholder="Nova Senha" class="form-control" name="password">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12-md-6">
					<input type="password" placeholder="Repita a Nova Senha" class="form-control" name="password_confirmation">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-4 col-md-offset-4">
					<button type="submit" class="btn btn-primary"> Enviar
					</button>
				</div>
			</div>
			<p class="change_link">Novo aqui?
				<a href="{{ route('register') }}" class="to_register"> Crie sua conta </a>
			</p>
		</form>
	</section>
</div>
@endsection
