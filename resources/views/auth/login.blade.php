@extends('layouts.app-auth')

@section('content')
<div id="login" class="animate form">
  <section class="login_content">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('login.post') }}">
      {!! csrf_field() !!}
      <h1>Login</h1>
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <div class="col-md-12-md-6">
              <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <div class="col-md-12-md-6">
              <input type="password" class="form-control" placeholder="Senha" name="password">

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      {{--<div class="form-group">--}}
          {{--<div class="col-xs-12">--}}
              {{--<div class="checkbox">--}}
                  {{--<label>--}}
                      {{--<input type="checkbox" name="remember"> Remember Me--}}
                  {{--</label>--}}
              {{--</div>--}}
          {{--</div>--}}
      {{--</div>--}}
      <div class="form-group">
          <div class="col-md12">
              <button type="submit" class="btn btn-primary">
                  <i class="fa fa-btn fa-sign-in"></i> Entrar
              </button>
          </div>
          <div class="col-xs-12">
              <a class="btn btn-link col-xs-12" href="{{ route('password.email') }}">Esqueceu sua senha?</a>
          </div>
      </div>
      <div class="clearfix"></div>
      <div class="separator">

        <p class="change_link">Novo aqui?
          <a href="{{ route('register') }}" class="to_register"> Crie sua conta </a>
        </p>
        <div class="clearfix"></div>
        <br />
        
        <div>
          <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Educacional!</h1>

          <p>©2016 All Rights Reserved. Educacional. Privacy and Terms</p>
        </div>
      </div>
    </form>
    <!-- form -->
  </section>
  <!-- content -->
</div>
@endsection