<?php
return [

    "status_aluno" =>
        [
            1 => 'MATRÍCULADO',
            2 => 'PRÉ-MATRÍCULA',
            3 => 'TRANCADO' ,
            4 => 'CONCLUÍDO',
            5 => 'GRADUADO',
            6 => 'ABANDONO',
            7 => 'FALECIDO',
            8 => 'TRANSFERIDO',
            9 => 'TROCA DE CURSO',
            9 => 'CANCELADO',
        ],
        'forma_ingresso' =>
            [
                1 => 'VESTIBULAR',
                2 => 'VESTIBULAR (REINGRESSO)',
                3 => 'VESTIBULAR (RETORNO)',
                4 => 'VESTIBULAR (TROCA DE CURSO)',
                5 => 'CERTIDÃO DE ESTUDOS',
                6 => 'EXOFICIO',
                7 => 'GRADUADO',
                8 => 'PÓS-GRADUAÇÃO',
                9 => 'TRANSFERÊNCIA',
                10 => 'PROUNI',
                11 => 'VESTIBULAR AGENDADO',
                12 => 'MATRÍCULA DE DISCIPLINA ESPECIAL',
                13 => 'ENEM',
                14 => 'PEC-G',
                15 => 'PORTADOR DE DIPLOMA',
            ],
        'tipo_escola' =>
            [
                1 => 'Não dispõe da informação',
                2 => 'Privado',
                3 => 'Público'
            ],
        'grau_instrucao' =>
        [
            1 => 'ENSINO FUNDAMENTAL',
            2 => 'ENSINO MÉDIO',
            3 => 'TÉCNICO NIVEL MÉDIO',
            4 => 'TÉCNICO NIVEL SUPERIOR',
            5 => 'GRADUADO',
            6 => 'PÓS-GRADUADO',
            7 => 'MESTRE',
            8 => 'DOUTOR',
            9 => 'PÓS-GRADUADO',
        ]

];
