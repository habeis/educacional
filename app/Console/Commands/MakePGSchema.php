<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MakePGSchema extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:pgschema {name : Nome do Schema} {--create : Para criar} {--drop : Para deletar}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Schema to Postgre';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('drop')) {
            DB::statement("DROP SCHEMA {$this->argument('name')} CASCADE;");
        }

        if ($this->option('create')) {
            DB::statement("CREATE SCHEMA {$this->argument('name')};");
        }

    }
}
