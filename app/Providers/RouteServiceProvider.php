<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        // Coloca regra em todas as rotas que contenham id
        $router->pattern('id', '\d+');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['prefix' => 'admin', 'namespace' => $this->namespace], function ($router) {
            require app_path('Http/Routes/auth.php');
            require app_path('Http/routes.php');
        });

        $router->group(['prefix' => 'admin', 'namespace' => 'EduAdmin\Controllers'], function ($router) {
            require base_path() . '/modules/EduAdmin/Routes/edu-admin.php';
        });

        $router->group(['prefix' => 'admin', 'namespace' => 'Menu\Controllers'], function ($router) {
            require base_path() . '/modules/Menu/Routes/menu.php';
        });

        $router->group(['prefix' => 'admin', 'namespace' => 'ACL\Controllers'], function ($router) {
            require base_path() . '/modules/ACL/Routes/acl.php';
        });

        $router->group(['prefix' => 'admin', 'namespace' => 'Auditing\Controllers'], function ($router) {
            require base_path() . '/modules/Auditing/Routes/auditing.php';
        });

        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/Routes/home.php');
        });
        $router->group(['namespace' => 'Nucleo\Controllers'], function ($router) {
            require base_path('modules/Nucleo/Routes/nucleo.php');
        });
        $router->group(['namespace' => 'Educacional\Controllers'], function ($router) {
            require base_path('modules/Educacional/Routes/educacional.php');
        });
    }
}
