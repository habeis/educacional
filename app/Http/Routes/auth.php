<?php

use Illuminate\Support\Facades\Route;

Route::get('auth/login', 'Auth\AuthController@getLogin')->name('login');
Route::post('auth/login', 'Auth\AuthController@postLogin')->name('login.post');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('logout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister')->name('register');
Route::post('auth/register', 'Auth\AuthController@postRegister')->name('register.post');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail')->name('password.email');
Route::post('password/email', 'Auth\PasswordController@postEmail')->name('password.email.post');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset')->name('password.reset');
Route::post('password/reset', 'Auth\PasswordController@postReset')->name('password.reset.post');
