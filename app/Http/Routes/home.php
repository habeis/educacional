<?php

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('home', ['as' => 'home',  'middleware' => 'auth', function () {
    Session::put('user', Auth::user());
    return view('home');
}]);

Route::get('foo', ['middleware' => ['auth', 'needsPermission'], 'shield' => 'users.create', function () {
    dd(getenv('INSTITUTE'));
    DB::statement('CREATE DATABASE horecio;');
    Artisan::call('migrations:install');

    dd('CRIADO');
}]);
