<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Início', route('home'));
});

// Home > [Page]
Breadcrumbs::register('page', function ($breadcrumbs, $id) {
    $page = Page::findOrFail($id);
    $breadcrumbs->parent('home');
    $breadcrumbs->push($page->title, route('page', $page->id));
});

require base_path() . '/modules/Menu/menu.breadcrumb.php';
require base_path() . '/modules/EduAdmin/edu-admin.breadcrumb.php';
require base_path() . '/modules/ACL/acl.breadcrumb.php';
require base_path() . '/modules/Auditing/auditing.breadcrumb.php';
require base_path() . '/modules/Nucleo/nucleo.breadcrumb.php';
require base_path() . '/modules/Educacional/educacional.breadcrumb.php';
