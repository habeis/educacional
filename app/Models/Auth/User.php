<?php

namespace App\Models\Auth;

use ACL\Models\Permissions;
use ACL\Models\Roles;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Artesaos\Defender\Traits\HasDefender;
use OwenIt\Auditing\AuditingTrait;
use Nucleo\Models\Pessoa;

/**
 * @SWG\Definition(
 *      definition="User",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pessoa_id",
 *          description="pessoa_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, HasDefender, AuditingTrait, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'pessoa_id',
        'password'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string',
        'deleted_at' => 'datetime',
        'pessoa_id' => 'integer'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Para Update a rule é diferente por causa do email único
     *
     * @return array
     */
    public function getUpdateRules()
    {
        // Solução para email único
        $idUser = app('request')->route('users');

        return [
            'name' => 'required|max:255',
            'email' => "required|email|max:255|unique:users,email,$idUser",
        ];
    }

    /*private function getToken()
    {
        return base_convert(hexdec(uniqid(rand(), true)) + microtime(true), 10, 36);
    }*/

    public function permissions()
    {
        return $this->belongsToMany(Permissions::class, 'permission_user', 'user_id', 'permission_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Roles::class, 'role_user', 'user_id', 'role_id');
    }

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }
}
