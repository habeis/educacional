<?php

namespace App\Models;

/**
 * Usado para não passar no REQUEST os campos que não são obrigatórios na tabela
 *
 * Class AttributesMutator
 * @package App\Models
 */
trait AttributesMutator
{
    private function nullIfBlank($field)
    {
        return trim($field) !== '' ? $field : null;
    }
}
