<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('nucleo.menu', function(Blueprint $table)
		{
			$table->foreign('parent_id', 'foreign_key_menu01')->references('id')->on('nucleo.menu')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('nucleo.menu', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_menu01');
		});
	}

}
