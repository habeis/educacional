<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nucleo.menu', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 70);
			$table->string('route', 50)->nullable();
			$table->string('icon_name', 50)->nullable();
			$table->integer('menu_order');
			$table->boolean('is_active')->nullable();
			$table->boolean('is_root')->nullable();
			$table->integer('parent_id')->nullable();

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nucleo.menu');
	}

}
