<?php

namespace Menu\Repositories;

use Menu\Models\Menu;
use InfyOm\Generator\Common\BaseRepository;

class MenuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     * @return Menu
     **/
    public function model()
    {
        return Menu::class;
    }

    public function findRootMenus()
    {
        return app('menu.models.menu')
            ->where('is_active', '=', true)
            ->where('is_root', '=', true)
            ->orderBy('menu_order', 'asc')
            ->get();
    }

    public function findAllRootMenus()
    {
        return app('menu.models.menu')
            ->where('is_root', '=', true)
            ->orderBy('menu_order', 'asc')
            ->get();
    }

    public function findChildMenusArray()
    {
        $childs = $this->findWhere(['is_active' =>  true, 'is_root' => false], ['title', 'id']);

        foreach ($childs as $key => $child) {
            $result[$child->id] = $child->title;
        }

        return $result;
    }

    public function findChildMenus()
    {
        return $this->findWhere(['is_active' =>  true, 'is_root' => false]);
    }
}
