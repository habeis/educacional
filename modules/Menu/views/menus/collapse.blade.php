<div class="panel-group" id="accordion">
    @foreach($menus as $menu)
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <div class="col-md-4 {{ $menu->is_active ? '' : 'red'  }}">
                    <i class="{!! $menu->icon_name !!}"></i>
                    <a data-toggle="{!! $menu->is_active ? 'collapse' : '' !!}" data-parent="#accordion" href="#collapse1" class="{{ $menu->is_active ? '' : 'red'  }}">{{ $menu->title }}</a>
                </div>
                <div class="text-right">
                    {!! Form::open(['route' => ['admin.menus.destroy', $menu->id], 'method' => 'delete']) !!}
                        <div class='btn-group text-right'>
                            <a href="{!! route('admin.menus.show', [$menu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{!! route('admin.menus.edit', [$menu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse" >
            <ul class="list-group">
                @foreach($menu->children as $child)
                    <li class="list-group-item">
                        <div class="col-md-4">
                            {{ $child->title }}
                        </div>
                        <div class="text-right">
                            {!! Form::open(['route' => ['admin.menus.destroy', $child->id], 'method' => 'delete']) !!}
                            <div class='btn-group text-right'>
                                <a href="{!! route('admin.menus.show', [$child->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="{!! route('admin.menus.edit', [$child->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    @endforeach
</div>