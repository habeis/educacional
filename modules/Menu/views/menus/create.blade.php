@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Criar Novo Menu</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'admin.menus.store']) !!}

            @include('menus.fields')

        {!! Form::close() !!}
    </div>
</div>
@endsection