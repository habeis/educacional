@extends('layouts.app')

@section('content')

    <div class="container">

        <h1 class="pull-left">Menus</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('admin.menus.create') !!}">Novo</a>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @if($menus->isEmpty())
            <div class="well text-center">No Menus found.</div>
        @else
            @include('menus.collapse')
        @endif
        
    </div>
@endsection