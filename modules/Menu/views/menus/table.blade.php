<table class="table table-responsive">
    <thead>
        <th>Ordem</th>
        <th>Título</th>
        <th>Rota</th>
        <th>Ícone</th>
        <th>Ativo?</th>
        <th>Raiz?</th>
        <th>Menu Pai</th>
        <th colspan="3">Ações</th>
    </thead>
    <tbody>
    @foreach($menus as $menu)
        <tr>
            <td>{!! $menu->menu_order !!}</td>
            <td>{!! $menu->title !!}</td>
            <td>{!! $menu->route !!}</td>
            <td><i class="{!! $menu->icon_name !!}"></i></td>
            <td><i class="{!! $menu->is_active ? 'fa fa-eye' : 'fa fa-eye-slash' !!}"></i></td>
            <td><i class="{!! $menu->is_root ? 'fa fa-check' : '' !!}"></i></td>
            <td>{!! $menu->parent_id !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.menus.destroy', $menu->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.menus.show', [$menu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.menus.edit', [$menu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>