
<div class="menu_section">
    <h3>Menus</h3>
    @foreach($menus as $menu)
        <ul class="nav side-menu">
            <li><a><i class="{{ $menu->icon_name }}"></i> {{ $menu->title }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    @foreach($menu->children as $child)
                        <li><a href="{{ route($child->route) }}">{{ $child->title }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>
    @endforeach
</div>