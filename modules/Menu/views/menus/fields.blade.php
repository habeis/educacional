<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Route Field -->
<div class="form-group col-sm-6">
    {!! Form::label('route', 'Route:') !!}
    {!! Form::text('route', null, ['class' => 'form-control']) !!}
</div>

<!-- IconName Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon_name', 'Icon Name') !!}
    {!! Form::text('icon_name', null, ['class' => 'form-control']) !!}
</div>

<!-- IconName Field -->
<div class="form-group col-sm-6">
    {!! Form::label('menu_order', 'Ordem') !!}
    {!! Form::number('menu_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', 'Is Active:') !!}
    {!!
        Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}

    {!! Form::check('is_active', true, isset($menu->is_active) && $menu->is_active == true ? 1 : 0) !!}
</div>

<!-- Is Root Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_root', 'Is Root:') !!}
    {!!
        Form::macro("checkRoot", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}

    {!! Form::checkRoot('is_root', true, isset($menu->is_root) && $menu->is_root == true ? 1 : 0) !!}
</div>

<!-- Parent Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('parent_id', 'Parent Id:') !!}
    {!! Form::select('parent_id', $menuParent, null, ['placeholder' => 'Selecione o menu pai']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.menus.index') !!}" class="btn btn-default">Cancel</a>
</div>
