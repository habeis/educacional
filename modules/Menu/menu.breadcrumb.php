<?php

Breadcrumbs::register('admin.menus.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar Menus', route('admin.menus.index'));
});

Breadcrumbs::register('admin.menus.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Menu', route('admin.menus.create'));
});

Breadcrumbs::register('admin.menus.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Menu', route('admin.menus.edit'));
});

Breadcrumbs::register('admin.menus.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Menu', route('admin.menus.show'));
});
