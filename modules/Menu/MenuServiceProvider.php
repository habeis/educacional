<?php

namespace Menu;

use Illuminate\Support\ServiceProvider;
use Menu\Models\Menu;
use Menu\Repositories\MenuRepository;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeNavigation();

        $this->publishMigrations();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('menu.models.menu', Menu::class);
        $this->app->bind('menu.repositories.menu', MenuRepository::class);
    }

    /**
     * Cria o menu
     * Poderia criar um outro provider com o nome ViewComposerServiceProvider
     */
    public function composeNavigation()
    {
        view()->composer('menus.partials.menu', 'Menu\MenuCreator');
    }

    public function publishMigrations()
    {
        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
