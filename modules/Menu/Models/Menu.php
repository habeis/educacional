<?php

namespace Menu\Models;

use ACL\Models\Permissions;
use App\Models\AttributesMutator;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Menu",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="route",
 *          description="route",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_active",
 *          description="is_active",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Menu extends Model
{
    use AttributesMutator;

    public $table = 'menu';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'title',
        'route',
        'icon_name',
        'menu_order',
        'is_active',
        'is_root',
        'parent_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'    => 'integer',
        'title' => 'string',
        'route' => 'string',
        'icon_name' => 'string',
        'menu_order' => 'integer',
        'is_active' => 'boolean',
        'is_root'   => 'boolean',
        'parent_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'     => 'required|max:70',
        'route'     => 'string|max:50' . null,
        'icon_name' => 'max:50',
        'menu_order' => 'integer',
        'is_active' => 'boolean',
        'is_root'   => 'boolean',
        'parent_id' => 'integer' . null
    ];

    /**
     * Quando não é passado o parent_id é necessário este método para não enviar o parâmetro parent_id
     *
     * @param $parentId
     */
    public function setParentIdAttribute($parentId)
    {
        $this->attributes['parent_id'] = $this->nullIfBlank($parentId);
    }

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Menu::class, 'parent_id')
            ->where('is_active', '=', true)
            ->orderBy('menu_order', 'asc');
    }

    public function permissions()
    {
        return $this->hasMany(Permissions::class);
    }
}
