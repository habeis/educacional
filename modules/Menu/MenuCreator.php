<?php

namespace Menu;

use Illuminate\Contracts\View\View;
use Menu\Repositories\MenuRepository;

class MenuCreator
{
    private $menuRepository;

    /**
     * MenuCreator constructor.
     * @param $menuRepository
     */
    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    /**
     * Faz a pesquisa no banco e envia para view
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with(
            'menus',
            $this->menuRepository
                ->findRootMenus()
        );
    }
}
