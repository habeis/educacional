<?php

namespace Menu\Controllers;

use App\Http\Requests;
use Menu\Requests\CreateMenuRequest;
use Menu\Requests\UpdateMenuRequest;
use Menu\Repositories\MenuRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class MenuController extends AppBaseController
{
    private $titulo;

    public function __construct()
    {
        $this->titulo = 'Menu';
    }

    /**
     * Display a listing of the Menu.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $menuRepository = app('menu.repositories.menu');
        $menuRepository->pushCriteria(new RequestCriteria($request));
        $menus = $menuRepository->findAllRootMenus();

        return view('menus.index')
            ->with('menus', $menus)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Lista de ' . $this->titulo);
    }

    /**
     * Show the form for creating a new Menu.
     *
     * @return Response
     */
    public function create()
    {
        $menuParent = app('menu.repositories.menu')
            ->lists('title', 'id')
            ->all();

        return view('menus.create')
            ->with('menuParent', $menuParent)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastro de ' . $this->titulo);
    }

    /**
     * Store a newly created Menu in storage.
     *
     * @param CreateMenuRequest $request
     *
     * @return Response
     */
    public function store(CreateMenuRequest $request)
    {
        $input = $request->all();

        app('menu.repositories.menu')->create($input);

        app('flash')->success('Menu saved successfully.');

        return redirect(route('admin.menus.index'));
    }

    /**
     * Display the specified Menu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $menu = app('menu.repositories.menu')->findWithoutFail($id);

        if (empty($menu)) {
            app('flash')->error('Menu not found');

            return redirect(route('admin.menus.index'));
        }

        return view('menus.show')
            ->with('menu', $menu)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Menu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $menu = app('menu.repositories.menu')->findWithoutFail($id);
        $menuParent = $menu->lists('title', 'id')->all();

        if (empty($menu)) {
            app('flash')->error('Menu not found');

            return redirect(route('admin.menus.index'));
        }

        return view('menus.edit')
            ->with('menu', $menu)
            ->with('menuParent', $menuParent)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Editar ' . $this->titulo);
    }

    /**
     * Update the specified Menu in storage.
     *
     * @param  int              $id
     * @param UpdateMenuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMenuRequest $request)
    {
        $menuRepository = app('menu.repositories.menu');
        $menu = $menuRepository->findWithoutFail($id);

        if (empty($menu)) {
            app('flash')->error('Menu not found');

            return redirect(route('admin.menus.index'));
        }

        $menuRepository->update($request->all(), $id);

        app('flash')->success('Menu updated successfully.');

        return redirect(route('admin.menus.index'));
    }

    /**
     * Remove the specified Menu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $menuRepository = app('menu.repositories.menu');
        $menu = $menuRepository->findWithoutFail($id);

        if (empty($menu)) {
            app('flash')->error('Menu not found');

            return redirect(route('admin.menus.index'));
        }

        $menuRepository->delete($id);

        app('flash')->success('Menu deleted successfully.');

        return redirect(route('admin.menus.index'));
    }
}
