<?php

namespace Nucleo\Controllers;


use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use Nucleo\Repositories\ModeloNotaRepository;
use Nucleo\Requests\CreateModeloNotaRequest;
use Prettus\Repository\Criteria\RequestCriteria;

class ModeloNotaController extends AppBaseController
{

    /** @var  parametros */
    private $parametros;

    /** @var  titulo */
    private $titulo;

    private $modeloNotaRepository;

    public function __construct(ModeloNotaRepository $modeloNotaRepository)
    {

        $titulo = 'Modelo de Nota';

        $this->modeloNotaRepository = $modeloNotaRepository;
        $this->parametros = compact('titulo');

    }

    public function index(Request $request)
    {

        $this->modeloNotaRepository->pushCriteria(new RequestCriteria($request));

        $modeloNotas = $this->modeloNotaRepository->all();

        return view('modeloNota.index')
            ->with('tituloUp', $this->parametros['titulo'])
            ->with('subTitulo', 'Listar ' . $this->parametros['titulo'])
            ->with('modeloNotas', $modeloNotas);
    }

    public function create(Request $request)
    {

        return view('modeloNota.create')
            ->with('tituloUp', $this->parametros['titulo'])
            ->with('subTitulo', 'Criar ' . $this->parametros['titulo']);

    }

    public function store(CreateModeloNotaRequest $request)
    {
        $input = $request->all();

        // SE O ATIVO NÃO FOI SELECIONDO ENTÃO ATRIBUIR UMA VARIÁVEL ATIVA CONTENDO FALSO O VALOR
        if (!isset($input['ativo'])) {
            $input['ativo'] = false;
        }

        $this->modeloNotaRepository->create($input);

        app('flash')->success('Modelo de Nota Cadastrado com Sucesso.');

        return redirect(route('modeloNotas.index'));
    }

    public function destroy($id)
    {

        $this->modeloNotaRepository->delete($id);
        app('flash')->success('Modelo de Nota Removido com Sucesso.');

        return redirect(route('modeloNotas.index'));

    }

}