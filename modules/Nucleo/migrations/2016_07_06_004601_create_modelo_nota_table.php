<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModeloNotaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 *  Modelo de Nota que basea quantas notas o tipo de Curso pode conter
	 * @return void
	 */
	public function up()
	{
		Schema::create('nucleo.modelo_nota', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('descricao', 100);
			$table->boolean('ativo');
			
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nucleo.modelo_nota');
	}

}
