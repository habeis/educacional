<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 08/07/16
 * Time: 09:30
 */

namespace Nucleo\Requests;

use App\Http\Requests\Request;
use Nucleo\Models\ModeloNota;

class CreateModeloNotaRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ModeloNota::$rules;
    }
}