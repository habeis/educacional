@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'modeloNotas.store']) !!}

        @include('modeloNota.field')

        {!! Form::close() !!}
    </div>
@endsection