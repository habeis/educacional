<table class="table table-responsive" id="pessoas-table">

        <th>Descrição</th>
        <th>Ativo</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($modeloNotas as $modeloNota)
        <tr>
            <td>{!! $modeloNota->descricao!!}</td>
            <td>{!! $modeloNota->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['modeloNotas.destroy', $modeloNota->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('modeloNotas.show', [$modeloNota->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('modeloNotas.edit', [$modeloNota->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>