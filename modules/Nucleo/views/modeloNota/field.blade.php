<div class="table-responsive col-lg-12">
    <div class="panel-group h2">
        <span>Modelo de Nota</span>
    </div>

    <div class="form-group col-md-12 ">
        {!! Form::label('descricao', 'Descrição: ') !!}
        {!! Form::text('descricao', null, ['class'=> 'form-control', 'placeholder'=> 'Descrição', ]) !!}
    </div>
    <!-- Ativo Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('ativo', 'Ativo:') !!}
        {!! Form::checkbox('ativo', true, null,['class' => 'form-control']) !!}
    </div>
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
        <a href="{!! route('modeloNotas.index') !!}" class="btn btn-primary">Cancelar</a>
    </div>

</div>