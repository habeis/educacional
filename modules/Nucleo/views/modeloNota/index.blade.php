@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('modeloNotas.create') !!}">{{ trans("forms.button_create") }}
    </a>
    <div class="clearfix"></div>

    @if($modeloNotas->isEmpty())

        <div class="well text-center">Não foi encontrada nenhum Modelo de Nota(s).</div>
    @else

        @include('modeloNota.table')

    @endif

@endsection