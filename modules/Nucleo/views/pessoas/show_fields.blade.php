<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Código:') !!}
    {!! Form::text('id', $pessoa->id , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- CPF Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpf', 'CPF:') !!}
    {!! Form::text('cpf', $pessoa->cpf, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', $pessoa->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Rg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rg', 'Rg:') !!}
    {!! Form::text('rg', $pessoa->rg, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Data criação:') !!}
    {!! Form::text('created_at', $pessoa->created_at, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Data de atualização:') !!}
    {!! Form::text('updated_at', $pessoa->updated_at, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Sexo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sexo', 'Sexo:') !!}
    {!! Form::text('sexo', $pessoa->sexo, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Raca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raca', 'Raça:') !!}
    {!! Form::text('raca', $pessoa->raca, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Data Nascimento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_nascimento', 'Data nascimento:') !!}
    {!! Form::text('data_nascimento', $pessoa->data_nascimento, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Estado Civil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado_civil', 'Estado civil:') !!}
    {!! Form::text('estado_civil', $pessoa->estado_civil, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Naturalidade Field -->
<div class="form-group col-sm-6">
    {!! Form::label('naturalidade', 'Naturalidade:') !!}
    {!! Form::text('naturalidade', $pessoa->naturalidade, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Nacionalidade Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nacionalidade', 'Nacionalidade:') !!}
    {!! Form::text('nacionalidade', $pessoa->nacionalidade, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Pai Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pai_nome', 'Nome do Pai:') !!}
    {!! Form::text('pai_nome', $pessoa->pai_nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Pai Cpf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pai_cpf', 'CPF do Pai:') !!}
    {!! Form::text('pai_cpf', $pessoa->pai_cpf, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Mae Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mae_nome', 'Nome da Mãe:') !!}
    {!! Form::text('mae_nome', $pessoa->pai_cpf, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Mae Cpf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mae_cpf', 'CPF da Mãe:') !!}
    {!! Form::text('mae_cpf', $pessoa->mae_cpf, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', $pessoa->email, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Telefone Celular Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefone_celular', 'Telefone celular:') !!}
    {!! Form::text('telefone_celular', $pessoa->telefone_celular, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Telefone Residencial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefone_residencial', 'Telefone Residencial:') !!}
    {!! Form::text('telefone_residencial', $pessoa->telefone_residencial, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Telefone Comercial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefone_comercial', 'Telefone Comercial:') !!}
    {!! Form::text('telefone_comercial', $pessoa->telefone_comercial , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Ramal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ramal', 'Ramal:') !!}
    {!! Form::text('ramal', $pessoa->ramal, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fax', 'Fax:') !!}
    {!! Form::text('fax', $pessoa->fax, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Ativo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ativo', 'Ativo:') !!}
    {!! Form::text('ativo', $pessoa->ativo, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Data Emissao Rf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_emissao_rf', 'Data de emissao RG:') !!}
    {!! Form::text('data_emissao_rf', $pessoa->data_emissao_rf, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>
