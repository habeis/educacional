<div class="table-responsive col-lg-12">
    <div class="panel-group h2">
        <span>Dados Pessoais</span>
    </div>

    <div class="form-group col-md-6  ">
        {!! Form::label('cpf', 'Cpf: ') !!}
        {!! Form::text('cpf', $pessoa->cpf, ['class'=> 'form-control', 'data-inputmask'=> "'mask': '999.999.999-99'", 'placeholder'=> 'CPF', 'disabled'=>'disabled']) !!}
    </div>

    <!-- Nome Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nome', 'Nome: ') !!}
        {!! Form::text('nome', $pessoa->nome, ['class' => 'form-control']) !!}
    </div>

    <!-- Rg Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('rg', 'Rg:') !!}
        {!! Form::text('rg', $pessoa->rg, ['class' => 'form-control']) !!}
    </div>

    <!-- Sexo Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('sexo', 'Sexo:') !!}
        {!! Form::select('sexo', trans("forms.sexo"), $pessoa->sexo, ['placeholder' => 'Selecione o Sexo', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
    </div>

    <!-- Raca Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('raca', 'Raca:') !!}
        {!! Form::text('raca', $pessoa->raca, ['class' => 'form-control']) !!}
    </div>

    <!-- Data Nascimento Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('data_nascimento', 'Data Nascimento:') !!}
        {!! Form::date('data_nascimento', $pessoa->data_nascimento, ['class' => 'form-control']) !!}
    </div>

    <!-- Estado Civil Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('estado_civil', 'Estado Civil:') !!}
        {!! Form::text('estado_civil', $pessoa->estado_civil, ['class' => 'form-control']) !!}
    </div>

    <!-- Naturalidade Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('naturalidade', 'Naturalidade:') !!}
        {!! Form::text('naturalidade', $pessoa->naturalidade, ['class' => 'form-control']) !!}
    </div>

    <!-- Nacionalidade Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('nacionalidade', 'Nacionalidade:') !!}
        {!! Form::text('nacionalidade', $pessoa->nacionalidade, ['class' => 'form-control']) !!}
    </div>

    <!-- Pai Nome Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pai_nome', 'Pai Nome:') !!}
        {!! Form::text('pai_nome', $pessoa->pai_nome, ['class' => 'form-control']) !!}
    </div>

    <!-- Pai Cpf Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pai_cpf', 'Pai Cpf:') !!}
        {!! Form::text('pai_cpf', $pessoa->pai_cpf, ['class' => 'form-control']) !!}
    </div>

    <!-- Mae Nome Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('mae_nome', 'Mae Nome:') !!}
        {!! Form::text('mae_nome', $pessoa->mae_nome, ['class' => 'form-control']) !!}
    </div>

    <!-- Mae Cpf Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('mae_cpf', 'Mae Cpf:') !!}
        {!! Form::text('mae_cpf', $pessoa->mae_cpf, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::email('email', $pessoa->email, ['class' => 'form-control']) !!}
    </div>

    <!-- Telefone Celular Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('telefone_celular', 'Telefone Celular:') !!}
        {!! Form::text('telefone_celular', $pessoa->telefone_celular, ['class' => 'form-control']) !!}
    </div>

    <!-- Telefone Residencial Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('telefone_residencial', 'Telefone Residencial:') !!}
        {!! Form::text('telefone_residencial', $pessoa->telefone_residencial, ['class' => 'form-control']) !!}
    </div>

    <!-- Telefone Comercial Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('telefone_comercial', 'Telefone Comercial:') !!}
        {!! Form::text('telefone_comercial', $pessoa->telefone_comercial, ['class' => 'form-control']) !!}
    </div>

    <!-- Ramal Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('ramal', 'Ramal:') !!}
        {!! Form::text('ramal', $pessoa->ramal, ['class' => 'form-control']) !!}
    </div>

    <!-- Fax Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fax', 'Fax:') !!}
        {!! Form::text('fax', $pessoa->fax, ['class' => 'form-control']) !!}
    </div>

    <!-- Data Emissao RG Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('data_emissao_rf', 'Data Emissao RG:') !!}
        {!! Form::date('data_emissao_rf', $pessoa->data_emissao_rf, ['class' => 'form-control']) !!}
    </div>

    <!-- Ativo Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('ativo', 'Ativo:') !!}
        {!! Form::checkbox('ativo', $pessoa->ativo, ['class' => 'form-control']) !!}
    </div>
    
</div>

<!-- Javascript -->
<script src="{{ asset('js/input_mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $(":input").inputmask();
    });
</script>
