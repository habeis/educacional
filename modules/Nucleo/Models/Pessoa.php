<?php

namespace Nucleo\Models;

use Educacional\Models\Endereco;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Pessoa",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nome",
 *          description="nome",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpf",
 *          description="cpf",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rg",
 *          description="rg",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sexo",
 *          description="sexo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="raca",
 *          description="raca",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="data_nascimento",
 *          description="data_nascimento",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="estado_civil",
 *          description="estado_civil",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="naturalidade",
 *          description="naturalidade",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nacionalidade",
 *          description="nacionalidade",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pai_nome",
 *          description="pai_nome",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pai_cpf",
 *          description="pai_cpf",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mae_nome",
 *          description="mae_nome",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mae_cpf",
 *          description="mae_cpf",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefone_celular",
 *          description="telefone_celular",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefone_residencial",
 *          description="telefone_residencial",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefone_comercial",
 *          description="telefone_comercial",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ramal",
 *          description="ramal",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fax",
 *          description="fax",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ativo",
 *          description="ativo",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="data_emissao_rf",
 *          description="data_emissao_rf",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="endereco_id",
 *          description="endereco_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Pessoa extends Model
{
    use SoftDeletes;

    public $table = 'nucleo.pessoa';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome',
        'cpf',
        'rg',
        'sexo',
        'raca',
        'data_nascimento',
        'estado_civil',
        'naturalidade',
        'nacionalidade',
        'pai_nome',
        'pai_cpf',
        'mae_nome',
        'mae_cpf',
        'email',
        'telefone_celular',
        'telefone_residencial',
        'telefone_comercial',
        'ramal',
        'fax',
        'ativo',
        'data_emissao_rf',
        'endereco_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'cpf' => 'string',
        'rg' => 'string',
        'sexo' => 'string',
        'raca' => 'string',
        'data_nascimento' => 'date',
        'estado_civil' => 'string',
        'naturalidade' => 'string',
        'nacionalidade' => 'string',
        'pai_nome' => 'string',
        'pai_cpf' => 'string',
        'mae_nome' => 'string',
        'mae_cpf' => 'string',
        'email' => 'string',
        'telefone_celular' => 'string',
        'telefone_residencial' => 'string',
        'telefone_comercial' => 'string',
        'ramal' => 'string',
        'fax' => 'string',
        'ativo' => 'boolean',
        'data_emissao_rf' => 'date',
        'endereco_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function endereco()
    {
        return $this->belongsTo(Endereco::class);
    }

}
