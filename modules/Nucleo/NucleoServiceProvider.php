<?php

namespace Nucleo;

use Illuminate\Support\ServiceProvider;
use Nucleo\Models\Pessoa;

class NucleoServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishMigrations();
    }

    public function register()
    {

        $this->app->bind('models.pessoa', Pessoa::class);
    }

    public function publishMigrations()
    {
        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
