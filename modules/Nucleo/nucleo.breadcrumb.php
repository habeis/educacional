<?php

// Aluno
Breadcrumbs::register('pessoas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Pessoas', route('pessoas.index'));
});

Breadcrumbs::register('pessoas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Pessoa', route('pessoas.create'));
});

Breadcrumbs::register('pessoas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Pessoa', route('pessoas.edit'));
});

Breadcrumbs::register('pessoas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Pessoa', route('pessoas.show'));
});

// Modelo Nota
Breadcrumbs::register('modeloNotas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Modelo(s) de Nota', route('modeloNotas.index'));
});

Breadcrumbs::register('modeloNotas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Modelo de Nota', route('modeloNotas.create'));
});

Breadcrumbs::register('pessoas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Modelo de Nota', route('modeloNotas.edit'));
});

Breadcrumbs::register('modeloNotas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Modelo de Nota', route('modeloNotas.show'));
});
