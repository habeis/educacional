<?php

namespace Nucleo\Repositories;

use Nucleo\Models\Pessoa;
use InfyOm\Generator\Common\BaseRepository;

class PessoaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pessoa::class;
    }

    //Busca a Pessoa pelo cpf
    public function buscarPessoaCpf($cpf)
    {

        $pessoa = Pessoa::where('cpf', '=', $cpf)->first();
        // Se não conter no banco então apenas cria uma instância dos objetos para não irem NULL
        if (empty($pessoa)) {
            $pessoa = app('nucleo.pessoa');
        }
        return $pessoa;
    }

    public function atualizarCriar($input)
    {
        $pessoa = $this->findWhere(['cpf' => $input['cpf']])->first();

        if (!empty($pessoa)) {
            // Se conter no banco de dados atualizar se não conter Cria um novo.
            $pessoa = $this->update($input, $pessoa->id);
        } else {
            $pessoa = $this->create($input);
        } 
        
        return $pessoa;
    }
}
