<?php

namespace Nucleo\Repositories;

use Nucleo\Models\ModeloNota;
use InfyOm\Generator\Common\BaseRepository;

class ModeloNotaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ModeloNota::class;
    }

}
