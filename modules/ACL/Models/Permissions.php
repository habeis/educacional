<?php

namespace ACL\Models;

use App\Models\Auth\User;
use Eloquent as Model;
use Menu\Models\Menu;

/**
 * @SWG\Definition(
 *      definition="Permissions",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="readable_name",
 *          description="readable_name",
 *          type="string"
 *      )
 * )
 */
class Permissions extends Model
{

    public $table = 'permissions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'name',
        'readable_name',
        'menu_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'readable_name' => 'string',
        'menu_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function role()
    {
        return $this->belongsToMany(Roles::class, 'permission_role', 'permission_id', 'role_id');
    }
    
    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'permission_user', 'permisson_id', 'user_id');
    }
}
