<?php

Route::resource('roles', 'RolesController');

Route::resource('permissions', 'PermissionsController');
