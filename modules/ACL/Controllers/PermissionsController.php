<?php

namespace ACL\Controllers;

use App\Http\Requests;
use ACL\Requests\CreatePermissionsRequest;
use ACL\Requests\UpdatePermissionsRequest;
use ACL\Repositories\PermissionsRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class PermissionsController extends AppBaseController
{
    /** @var  PermissionsRepository */
    private $permissionsRepository;

    private $titulo;

    public function __construct(PermissionsRepository $permissionsRepo)
    {
        $this->titulo = 'Privilégio';
        
        $this->permissionsRepository = $permissionsRepo;
    }

    /**
     * Display a listing of the Permissions.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->permissionsRepository->pushCriteria(new RequestCriteria($request));
        $permissions = $this->permissionsRepository->all();

        return view('permissions.index')
            ->with('permissions', $permissions)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Lista de ' . $this->titulo);
    }

    /**
     * Show the form for creating a new Permissions.
     *
     * @return Response
     */
    public function create()
    {
        $menus = app('menu.repositories.menu')
            ->findChildMenusArray();
        
        return view('permissions.create')
            ->with('menus', $menus)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastro de ' . $this->titulo);
    }

    /**
     * Store a newly created Permissions in storage.
     *
     * @param CreatePermissionsRequest $request
     *
     * @return Response
     */
    public function store(CreatePermissionsRequest $request)
    {
        $input = $request->all();

        $this->permissionsRepository->create($input);

        app('flash')->success('Permissions saved successfully.');

        return redirect(route('admin.permissions.index'));
    }

    /**
     * Display the specified Permissions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $permissions = $this->permissionsRepository->findWithoutFail($id);

        if (empty($permissions)) {
            app('flash')->error('Permissions not found');

            return redirect(route('admin.permissions.index'));
        }

        return view('permissions.show')
            ->with('permissions', $permissions)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Permissions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $permissions = $this->permissionsRepository->findWithoutFail($id);

        if (empty($permissions)) {
            app('flash')->error('Permissions not found');

            return redirect(route('admin.permissions.index'));
        }

        $menus = app('menu.repositories.menu')->findChildMenusArray();

        return view('permissions.edit')
            ->with('menus', $menus)
            ->with('permissions', $permissions)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Editar ' . $this->titulo);
    }

    /**
     * Update the specified Permissions in storage.
     *
     * @param  int              $id
     * @param UpdatePermissionsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePermissionsRequest $request)
    {
        $permissions = $this->permissionsRepository->findWithoutFail($id);

        if (empty($permissions)) {
            app('flash')->error('Permissions not found');

            return redirect(route('admin.permissions.index'));
        }

        $permissions = $this->permissionsRepository->update($request->all(), $id);

        app('flash')->success('Permissions updated successfully.');

        return redirect(route('admin.permissions.index'));
    }

    /**
     * Remove the specified Permissions from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $permissions = $this->permissionsRepository->findWithoutFail($id);

        if (empty($permissions)) {
            app('flash')->error('Permissions not found');

            return redirect(route('admin.permissions.index'));
        }

        $this->permissionsRepository->delete($id);

        app('flash')->success('Permissions deleted successfully.');

        return redirect(route('admin.permissions.index'));
    }
}
