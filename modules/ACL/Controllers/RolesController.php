<?php

namespace ACL\Controllers;

use App\Http\Requests;
use ACL\Requests\CreateRolesRequest;
use ACL\Requests\UpdateRolesRequest;
use ACL\Repositories\RolesRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class RolesController extends AppBaseController
{
    /** @var  RolesRepository */
    private $rolesRepository;

    private $titulo;

    public function __construct(RolesRepository $rolesRepo)
    {
        $this->titulo = 'Perfil';
        
        $this->rolesRepository = $rolesRepo;
    }

    /**
     * Display a listing of the Roles.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rolesRepository->pushCriteria(new RequestCriteria($request));
        $roles = $this->rolesRepository->all();

        return view('roles.index')
            ->with('roles', $roles)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Lista de ' . $this->titulo);
    }

    /**
     * Show the form for creating a new Roles.
     *
     * @return Response
     */
    public function create()
    {
        $menuGroups = app('menu.repositories.menu')
            ->findChildMenus();
        
        return view('roles.create')
            ->with('menuGroups', $menuGroups)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastro de ' . $this->titulo);
    }

    /**
     * Store a newly created Roles in storage.
     *
     * @param CreateRolesRequest $request
     *
     * @return Response
     */
    public function store(CreateRolesRequest $request)
    {
        $input = $request->all();

        $this->rolesRepository->create($input);

        app('flash')->success('Roles saved successfully.');

        return redirect(route('admin.roles.index'));
    }

    /**
     * Display the specified Roles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            app('flash')->error('Roles not found');

            return redirect(route('admin.roles.index'));
        }

        return view('roles.show')
            ->with('roles', $roles)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Roles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);
        $permissionsIds = $roles->permissions()->getRelatedIds()->toArray();
        
        $menuGroups = app('menu.repositories.menu')
            ->findChildMenus();

        if (empty($roles)) {
            app('flash')->error('Roles not found');

            return redirect(route('admin.roles.index'));
        }

        return view('roles.edit')
            ->with('roles', $roles)
            ->with('menuGroups', $menuGroups)
            ->with('permissionsIds', $permissionsIds)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastro de ' . $this->titulo);
    }

    /**
     * Update the specified Roles in storage.
     *
     * @param  int              $id
     * @param UpdateRolesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRolesRequest $request)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            app('flash')->error('Roles not found');

            return redirect(route('admin.roles.index'));
        }

        $this->rolesRepository->update($request->all(), $id);

        app('flash')->success('Roles updated successfully.');

        return redirect(route('admin.roles.index'));
    }

    /**
     * Remove the specified Roles from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            app('flash')->error('Roles not found');

            return redirect(route('admin.roles.index'));
        }

        $this->rolesRepository->delete($id);

        app('flash')->success('Roles deleted successfully.');

        return redirect(route('admin.roles.index'));
    }
}
