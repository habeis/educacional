<?php

namespace ACL;

use ACL\Models\Permissions;
use ACL\Models\Roles;
use ACL\Repositories\PermissionsRepository;
use ACL\Repositories\RolesRepository;
use Illuminate\Support\ServiceProvider;

class AclServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishMigrations();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('acl.models.roles', Roles::class);
        $this->app->bind('acl.models.permissions', Permissions::class);
        $this->app->bind('acl.repositories.roles', RolesRepository::class);
        $this->app->bind('acl.repositories.permissions', PermissionsRepository::class);
    }

    public function publishMigrations()
    {
        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
