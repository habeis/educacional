<table class="table table-responsive">
    <thead>
        <th>Nome</th>
        <th>Nome para Leitura</th>
        <th>Menu grupo</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($permissions as $permissions)
        <tr>
            <td>{!! $permissions->name !!}</td>
            <td>{!! $permissions->readable_name !!}</td>
            <td>{!! $permissions->menu->title !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.permissions.destroy', $permissions->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.permissions.show', [$permissions->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.permissions.edit', [$permissions->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>