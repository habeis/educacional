@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($permissions, ['route' => ['admin.permissions.update', $permissions->id], 'method' => 'patch']) !!}

        @include('permissions.fields')

        {!! Form::close() !!}
    </div>
@endsection