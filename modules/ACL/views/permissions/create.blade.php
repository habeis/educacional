@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'author.store']) !!}

            @include('permissions.fields')

        {!! Form::close() !!}
    </div>
@endsection