<!-- Parent Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('menu_id', 'Menu:') !!}
    {!! Form::select('menu_id', $menus, null, ['class' => 'form-control', 'placeholder' => 'Selecione o menu']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Readable Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('readable_name', 'Nome para Leitura:') !!}
    {!! Form::text('readable_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.permissions.index') !!}" class="btn btn-default">Cancelar</a>
</div>
