@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('admin.permissions.create') !!}">{{ trans("forms.button_create") }}
    </a>
    <div class="clearfix"></div>

    @if($permissions->isEmpty())
        <div class="well text-center">No Permissions found.</div>
    @else
        @include('permissions.table')
    @endif
@endsection