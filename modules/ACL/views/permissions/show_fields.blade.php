<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $permissions->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $permissions->name !!}</p>
</div>

<!-- Readable Name Field -->
<div class="form-group">
    {!! Form::label('readable_name', 'Readable Name:') !!}
    <p>{!! $permissions->readable_name !!}</p>
</div>

