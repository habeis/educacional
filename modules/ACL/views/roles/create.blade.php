@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'admin.roles.store']) !!}

            @include('roles.fields')

        {!! Form::close() !!}
    </div>
@endsection