@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($roles, ['route' => ['admin.roles.update', $roles->id], 'method' => 'patch']) !!}

        @include('roles.fields')

        {!! Form::close() !!}
    </div>
@endsection