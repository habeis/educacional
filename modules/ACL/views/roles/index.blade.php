@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('admin.roles.create') !!}">{{ trans("forms.button_create") }}
    </a>
    <div class="clearfix"></div>

    @if($roles->isEmpty())
        <div class="well text-center">No Roles found.</div>
    @else
        @include('roles.table')
    @endif
@endsection