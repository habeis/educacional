<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

@foreach($menuGroups as $group)
<div class="col-md-3 col-xs-12 widget widget_tally_box">
    <div class="x_panel fixed_height_200">
        <div class="x_title">
            <h2>{{ $group->title }}</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            @foreach($group->permissions as $permission)
            <ul class="legend list-unstyled">
                <li>
                    <p>
                        {!! Form::label('permission_id', $permission->readable_name) !!}
                        {!! Form::checkbox('permission_id[]', $permission->id, isset($permissionsIds) && in_array($permission->id, $permissionsIds) ? true : null) !!}
                    </p>
                </li>
            </ul>

            <div class="divider"></div>
            @endforeach

        </div>
    </div>
</div>
@endforeach

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.roles.index') !!}" class="btn btn-default">Cancel</a>
</div>
