<?php

namespace ACL\Repositories;

use ACL\Models\Roles;
use InfyOm\Generator\Common\BaseRepository;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class RolesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Roles::class;
    }

    public function create(array $attributes)
    {
        $role =  parent::create($attributes);

        if (isset($attributes['permission_id'])) {
            return $role->permissions()
                ->attach($attributes['permission_id']);
        }

        return $role;

        // Salva passando objetos
//        return $role->permissions()->saveMany([1, 2]);
    }

    public function update(array $attributes, $id)
    {
        $role = parent::update($attributes, $id);

        return $role->permissions()
            ->sync($attributes['permission_id']);
    }
}
