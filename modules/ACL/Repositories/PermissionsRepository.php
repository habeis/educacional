<?php

namespace ACL\Repositories;

use ACL\Models\Permissions;
use InfyOm\Generator\Common\BaseRepository;

class PermissionsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Permissions::class;
    }
}
