<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenuReferenceInPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nucleo.permissions', function (Blueprint $table) {
            $table->unsignedInteger('menu_id');
            $table->foreign('menu_id', 'foreign_key_menu_id_02')->references('id')->on('nucleo.menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nucleo.permissions', function (Blueprint $table) {
    
            $table->dropForeign('foreign_key_menu_id_02');
            $table->dropColumn('menu_id');
        });
    }
}
