<?php

// Perfis
Breadcrumbs::register('admin.roles.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar Perfis', route('admin.roles.index'));
});

Breadcrumbs::register('admin.roles.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Perfil', route('admin.roles.create'));
});

Breadcrumbs::register('admin.roles.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar Perfis', route('admin.roles.edit'));
});

Breadcrumbs::register('admin.roles.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Perfil', route('admin.roles.show'));
});

// Privilégios
Breadcrumbs::register('admin.permissions.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar Privilégios', route('admin.permissions.index'));
});

Breadcrumbs::register('admin.permissions.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Privilégio', route('admin.permissions.create'));
});

Breadcrumbs::register('admin.permissions.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Ediatr Privilégio', route('admin.permissions.edit'));
});

Breadcrumbs::register('admin.permissions.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Privilégio', route('admin.permissions.show'));
});
