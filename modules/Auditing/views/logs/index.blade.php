@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('admin.logs.create') !!}">{{ trans("forms.button_create") }}
    </a>
    <div class="clearfix"></div>

        @if($logs->isEmpty())
            <div class="well text-center">No Logs found.</div>
        @else
            @include('logs.table')
        @endif
@endsection