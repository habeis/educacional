<table class="table table-responsive">
    <thead>
        <th>User Id</th>
        <th>Owner Type</th>
        <th>Owner Id</th>
        <th>Old Value</th>
        <th>New Value</th>
        <th>Type</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($logs as $logs)
        <tr>
            <td>{!! $logs->user_id !!}</td>
            <td>{!! $logs->owner_type !!}</td>
            <td>{!! $logs->owner_id !!}</td>
            <td>{!! $logs->old_value !!}</td>
            <td>{!! $logs->new_value !!}</td>
            <td>{!! $logs->type !!}</td>
            <td>{!! $logs->created_at !!}</td>
            <td>{!! $logs->updated_at !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.logs.destroy', $logs->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.logs.show', [$logs->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>