<?php

namespace Auditing\Controllers;

use App\Http\Requests;
use Auditing\Repositories\LogsRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class LogsController extends AppBaseController
{
    /** @var  LogsRepository */
    private $logsRepository;

    private $titutlo;

    public function __construct(LogsRepository $logsRepo)
    {
        $this->logsRepository = $logsRepo;

        $this->titulo = 'Auditoria';
    }

    /**
     * Display a listing of the Logs.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->logsRepository->pushCriteria(new RequestCriteria($request));
        $logs = $this->logsRepository->all();

        return view('logs.index')
            ->with('logs', $logs)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Lista de ' . $this->titulo);
    }

    /**
     * Display the specified Logs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $logs = $this->logsRepository->findWithoutFail($id);

        if (empty($logs)) {
            app('flash')->error('Logs not found');

            return redirect(route('admin.logs.index'));
        }

        return view('logs.show')
            ->with('logs', $logs)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }
}
