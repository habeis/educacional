<?php

// Home > Listar logs de usuários
Breadcrumbs::register('admin.logs.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar Logs de Usuários', route('admin.logs.index'));
});

Breadcrumbs::register('admin.logs.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Log', route('admin.logs.show'));
});
