<?php

namespace Auditing\Repositories;

use Auditing\Models\Logs;
use InfyOm\Generator\Common\BaseRepository;

class LogsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Logs::class;
    }
}
