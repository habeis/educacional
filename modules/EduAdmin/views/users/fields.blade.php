{{--{!! Form::hidden('id', $user->id) !!}--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <div class="col-md-12-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}

        @if ($errors->has('name'))
            <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

    <div class="col-md-12-md-6">
        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}

        @if ($errors->has('email'))
            <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
        @endif
    </div>
</div>

<div class="col-md-12 divider"></div>

<!-- Parent Id Field -->
<section class="row">
    <div class="form-group col-sm-12">
        {!! Form::label('role_id', 'Perfis:') !!}
        {!! Form::select('role_id[]', $roles, isset($rolesIds) ? $rolesIds : null, ['class' => 'user-role2 form-control', 'id' => 'tag_list', 'multiple']) !!}
    </div>
</section>

<div class="divider"></div>
<section class="row">
    @foreach($menuGroups as $group)
        <div class="col-md-3 col-xs-12 widget widget_tally_box">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $group->title }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    @foreach($group->permissions as $permission)
                        <ul class="legend list-unstyled">
                            <li>
                                <p>
                                    {!! Form::label('permission_id', $permission->readable_name) !!}
                                    {!! Form::checkbox('permission_id[]', $permission->id, isset($permissionsIds) && in_array($permission->id, $permissionsIds) ? true : null) !!}
                                </p>
                            </li>
                        </ul>

                        <div class="divider"></div>
                    @endforeach

                </div>
            </div>
        </div>
    @endforeach
</section>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.users.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('post-script')
    <script type="text/javascript">
        $(".user-role2").select2();
    </script>
@stop

