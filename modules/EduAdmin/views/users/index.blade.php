@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('admin.permissions.create') !!}">{{ trans("forms.button_create") }}
    </a>
    <div class="clearfix"></div>

    @if($users->isEmpty())
        <div class="well text-center">No Users found.</div>
    @else
        @include('users.table')
    @endif
@endsection