@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'admin.users.store']) !!}

            @include('users.fields')

        {!! Form::close() !!}
    </div>
@endsection