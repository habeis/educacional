<?php

namespace EduAdmin\Repositories;

use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Common\BaseRepository;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    public function create(array $attributes)
    {
        return parent::create($attributes);

        $user = parent::create($attributes);

        if (isset($attributes['permission_id'])) {
            $user->permissions()
                ->attach($attributes['permission_id']);
        }

        if (isset($attributes['role_id'])) {
            $user->permissions()
                ->attach($attributes['role_id']);
        }

        return $role;
    }

    public function update(array $attributes, $id)
    {
        $user = parent::update($attributes, $id);

        if (isset($attributes['permission_id'])) {
            $user->permissions()
                ->sync($attributes['permission_id']);
        }


        if (isset($attributes['role_id'])) {
            $user->roles()
                ->sync($attributes['role_id']);
        }
        return $user;
    }

    public function atualizarCriar($input)
    {
        // Pesquisa para ver se essa pessoa já contém User no banco
        $user = $this->findWhere(['pessoa_id' => $input['pessoa_id']])->first();

        //Se contém o usuário então apenas atualiza
        if (isset($user)) {

            $user = $this->update($input, $user->id);
        } else {

            // O sistema não pode permitir uma pessoa cadastra o mesmo email mais que 1 vez
            $user = $this->findWhere(['email' => $input['email']])->first();

            if (isset($user)) {

                // o email pega o cpf até o usuário atualizar pois já contem no banco de dados esse email
                $input['email'] = $input['cpf'];
                Flash::error('Email precisa ser atualizado pois já contém cadastrado no sistema.');
            }

            $input['name'] = $input['cpf'];
            $input['password'] = Hash::make($input['pessoa_id']);

            $user = $this->create($input);
        }
        
        return $user;
        
    }

}
