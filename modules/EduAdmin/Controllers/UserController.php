<?php

namespace EduAdmin\Controllers;

use App\Http\Requests;
use EduAdmin\Requests\CreateUserRequest;
use EduAdmin\Requests\UpdateUserRequest;
use EduAdmin\Repositories\UserRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    private $titulo = 'Usuário';

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->all();

        return view('users.index')
            ->with('users', $users)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Lista de ' . $this->titulo);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $menuGroups = app('menu.repositories.menu')
            ->findChildMenus();

        $roles = app('acl.repositories.roles')
            ->lists('name', 'id')
            ->all();

        return view('users.create')
            ->with('menuGroups', $menuGroups)
            ->with('roles', $roles)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastro de ' . $this->titulo);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        $this->userRepository->create($input);

        app('flash')->success('User saved successfully.');

        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            app('flash')->error('User not found');

            return redirect(route('admin.users.index'));
        }

        return view('users.show')
            ->with('user', $user)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);
        $permissionsIds = $user->permissions()->getRelatedIds()->toArray();
        $rolesIds = $user->roles()->getRelatedIds()->toArray();
        
        $roles = app('acl.repositories.roles')
            ->lists('name', 'id')
            ->all();
        
        $menuGroups = app('menu.repositories.menu')
            ->findChildMenus();

        if (empty($user)) {
            app('flash')->error('User not found');

            return redirect(route('admin.users.index'));
        }

        return view('users.edit')
            ->with('user', $user)
            ->with('menuGroups', $menuGroups)
            ->with('permissionsIds', $permissionsIds)
            ->with('roles', $roles)
            ->with('rolesIds', $rolesIds)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Editar ' . $this->titulo);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            app('flash')->error('User not found');

            return redirect(route('admin.users.index'));
        }

        $this->userRepository->update($request->all(), $id);

        app('flash')->success('User updated successfully.');

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            app('flash')->error('User not found');

            return redirect(route('admin.users.index'));
        }

        $this->userRepository->delete($id);

        app('flash')->success('User deleted successfully.');

        return redirect(route('admin.users.index'));
    }
}
