<?php

// Home > Listar usuários
Breadcrumbs::register('admin.users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar Usuários', route('admin.users.index'));
});

// Home > Criar usuário
Breadcrumbs::register('admin.users.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Usuário', route('admin.users.create'));
});

// Home > Editar usuário
Breadcrumbs::register('admin.users.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Usuário', route('admin.users.edit'));
});

// Home > Detalhes usuário
Breadcrumbs::register('admin.users.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Usuário', route('admin.users.show'));
});
