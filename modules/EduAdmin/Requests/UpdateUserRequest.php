<?php

namespace EduAdmin\Requests;

use App\Http\Requests\Request;
use App\Models\Auth\User;

class UpdateUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @param $user \App\Models\Auth\User
     * @return array
     */
    public function rules(User $user)
    {
        return $user->getUpdateRules();
    }
}
