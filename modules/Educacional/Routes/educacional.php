<?php

Route::resource('professor-disciplina', 'ProfessorDisciplinaController');

Route::get('/professor-disciplina-curso', ['as' => 'professor-disciplina-curso','uses'=>'ProfessorDisciplinaController@curso']);

Route::get('/professor-cpf', ['as' => 'professor-cpf','uses'=>'ProfessorController@professorCpf']);

Route::resource('professores', 'ProfessorController');

Route::get('/aluno-historico-detalhado', ['as' => 'aluno-historico-detalhado','uses'=>'AlunoHistoricoController@detalhado']);

Route::resource('aluno-historico', 'AlunoHistoricoController');

Route::resource('alunoTurmas', 'AlunoTurmaController');

Route::get('/aluno-cpf', ['as' => 'aluno-cpf','uses'=>'AlunoTurmaController@alunoCpf']);

Route::get('/aluno-turma-calouro', ['as' => 'aluno-turma-calouro','uses'=>'AlunoTurmaController@alunoTurmaCalouro']);

Route::get('/alunos-turma', ['as' => 'alunos-turma','uses'=>'AlunoTurmaController@alunosTurnma']);

Route::resource('alunoTurmaDisciplinas', 'AlunoTurmaDisciplinaController');

Route::resource('modalidades', 'ModalidadeController');

Route::resource('portarias', 'PortariaController');

Route::resource('turnos', 'TurnoController');

Route::resource('coordenacaos', 'CoordenacaoController');

Route::post('post-tipoCurso', 'ColigadaController@adicionarTipoCurso');

Route::resource('turmaDisciplinas', 'TurmaDisciplinaController');

Route::resource('anoSemestres', 'AnoSemestreController');

Route::resource('matrizCurriculars', 'MatrizCurricularController');

Route::resource('tipoDisciplinas', 'TipoDisciplinaController');

Route::resource('disciplinas', 'DisciplinaController');

Route::resource('creditoValors', 'CreditoValorController');

Route::resource('matrizCurricularDisciplinas', 'MatrizCurricularDisciplinaController');

Route::resource('periodos', 'PeriodoController');

Route::resource('turmas', 'TurmaController');

Route::resource('habilitacaos', 'HabilitacaoController');

Route::resource('cursos', 'CursoController');

Route::resource('coligadas', 'ColigadaController');

/**
 * Rota que traz o Semestre Letivo e também todos os Cursos
 */
Route::get('/semestre-cursos', ['as' => 'semestre-cursos', function (\Educacional\Repositories\CursoRepository $cursoRepository, \Educacional\Repositories\AnoSemestreRepository $anoSemestreRepository) {

    $cursos = $cursoRepository->lists('nome', 'id');

    $anoSemestre = $anoSemestreRepository->lists('ano_semestre', 'id');

    return View::make('turmas.cursos')
        ->with('anoSemestre', $anoSemestre)
        ->with('cursos', $cursos)
        ->with('tituloUp', 'Turmas')
        ->with('subTitulo', 'Listar de Cursos');

}]);

/**
 * Consulta das Grade via Ajax
 * @retorn Json Grades
 */
Route::get('/semestre-cursos-grade', function (\Educacional\Repositories\MatrizCurricularRepository $matrizCurricularRepository) {

    $cursoId = Input::get('curso_id');
    /**
     * Retorna a Grade(Matriz_curricular) de um determinado Curso
     */
    $grades = \Educacional\Models\MatrizCurricular::where('curso_id', '=', $cursoId)
        ->join('educacional.curso', 'curso_id', '=', 'educacional.curso.id')
        ->select('educacional.matriz_curricular.id', 'educacional.matriz_curricular.versao', 'educacional.curso.nome')
        ->get();
    return Response::json($grades);
});

/**
 * Consulta das Grade via Ajax
 * @retorn Json Grades
 */
Route::get('/turmas-semestre', function () {

    $cursoId = Input::get('curso_id');

    $anoSemestreId = Input::get('semestreLetivo_id');

    /**
     * Retorna a Grade(Matriz_curricular) de um determinado Curso
     */
    $turmas = \Educacional\Models\Turma::where('ano_semestre_id', '=', $anoSemestreId)
        ->where('curso_id', '=', $cursoId)
        ->rightJoin('educacional.periodo', 'periodo_id', '=', 'educacional.periodo.id')
        ->get(
            [
                'turma.nome as nomeTurma',
                'turma.id as turmaId',
                'educacional.periodo.id as periodoId',
                'educacional.periodo.descricao as descricaoPeriodo'
            ]);
    return Response::json($turmas);
});

/**
 * Consulta Curso de um Tipo de coligada_tipo_curso
 * @retorn Json Grades
 */
Route::get('/curso-tipo-curso', function (\Educacional\Repositories\MatrizCurricularRepository $matrizCurricularRepository) {

    $tipoCursoId = Input::get('tipo_curso_id');

    $cursos = \Educacional\Models\Curso::from('educacional.curso')
        ->join('educacional.coligada_tipo_curso', 'coligada_tipo_curso_id', '=', 'educacional.coligada_tipo_curso.id')
        ->where('educacional.coligada_tipo_curso.tipo_curso_id', '=', $tipoCursoId)
        ->get();
    return Response::json($cursos);
});

/**
 * Retorna o semestre letivo de um determinado tipo de curso
 */
Route::get('/ano-semestre-tipo-curso', function (\Educacional\Repositories\MatrizCurricularRepository $matrizCurricularRepository) {

    $tipoCursoId = Input::get('tipo_curso_id');

    $grades = \Educacional\Models\AnoSemestre::from('educacional.ano_semestre')
        ->join('educacional.coligada_tipo_curso', 'coligada_tipo_curso_id', '=', 'educacional.coligada_tipo_curso.id')
        ->where('educacional.coligada_tipo_curso.tipo_curso_id', '=', $tipoCursoId)
        ->orderBy('educacional.ano_semestre.created_at', 'desc')
        ->get(['educacional.ano_semestre.id', 'educacional.ano_semestre.ano_semestre']);
    return Response::json($grades);
});


Route::resource('modoCursos', 'ModoCursoController');

Route::resource('alunos', 'AlunoController');

