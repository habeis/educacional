<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Models\Coligada;
use Educacional\Requests\CreateCursoRequest;
use Educacional\Requests\UpdateCursoRequest;
use Educacional\Repositories\CursoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Educacional\Models\Modalidade;
use Educacional\Models\Turno;
use Educacional\Models\Habilitacao;
use Educacional\Models\ModoCurso;
use Educacional\Models\Coordenacao;
use Educacional\Models\Portaria;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class CursoController extends AppBaseController
{
    /** @var  CursoRepository */
    private $cursoRepository;

    private $modalidades;

    private $turnos;

    private $habilistacaos;

    private $modoCursos;

    private $coordenacaos;

    private $portarias;

    private $coligada;

    public function __construct(CursoRepository $cursoRepo)
    {
        $this->cursoRepository = $cursoRepo;

        $this->beforeFilter('@findAllModalidade', ['only' => ['create']]);

    }

    public function findAllModalidade()
    {
        $this->modalidades = Modalidade::lists('descricao', 'id');

        $this->coligadaTipoCurso = Coligada::first()->tipoCursos()->get();

        $this->turnos = Turno::lists('descricao', 'id');
        
        $this->habilistacaos = Habilitacao::lists('descricao', 'id');

        $this->modoCursos = ModoCurso::lists('descricao', 'id');

        $this->coordenacaos = Coordenacao::lists('nome', 'id');

        $this->portarias = Portaria::lists('descricao', 'id');
    }

    /**
     * Display a listing of the Curso.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->cursoRepository->pushCriteria(new RequestCriteria($request));

        $cursos = $this->cursoRepository->all();

        return view('cursos.index')
            ->with('cursos', $cursos)
            ->with('tituloUp', 'Curso')
            ->with('subTitulo', 'Lista de Cursos');
    }

    /**
     * Show the form for creating a new Curso.
     *
     * @return Response
     */
    public function create()
    {
        return view('cursos.create')
            ->with('modalidades', $this->modalidades)
            ->with('habilistacaos', $this->habilistacaos)
            ->with('modoCursos', $this->modoCursos)
            ->with('coordenacaos', $this->coordenacaos)
            ->with('portarias', $this->portarias)
            ->with('turnos', $this->turnos)
            ->with('coligadaTipoCurso', $this->coligadaTipoCurso)
            ->with('tituloUp', 'Curso')
            ->with('subTitulo', 'Cadastrar novo Curso');
    }

    /**
     * Store a newly created Curso in storage.
     *
     * @param CreateCursoRequest $request
     *
     * @return Response
     */
    public function store(CreateCursoRequest $request)
    {
        $input = $request->all();

        $this->cursoRepository->create($input);

        app('flash')->success('Curso saved successfully.');

        return redirect(route('cursos.index'));
    }

    /**
     * Display the specified Curso.
     *
     * @param  int $id
     *
     * @return Responseed
     */
    public function show($id)
    {
        $curso = $this->cursoRepository->findWithoutFail($id);

        if (empty($curso)) {
            app('flash')->error('Curso not found');

            return redirect(route('cursos.index'));
        }

        return view('cursos.show')
            ->with('curso', $curso)
            ->with('tituloUp', 'Curso')
            ->with('subTitulo', 'Visualizar Curso');
    }

    /**
     * Show the form for editing the specified Curso.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->findAllModalidade();
        $curso = $this->cursoRepository->findWithoutFail($id);

        if (empty($curso)) {
            app('flash')->error('Curso not found');

            return redirect(route('cursos.index'));
        }

        return view('cursos.edit')
            ->with('curso', $curso)
            ->with('modalidades', $this->modalidades)
            ->with('habilistacaos', $this->habilistacaos)
            ->with('modoCursos', $this->modoCursos)
            ->with('coordenacaos', $this->coordenacaos)
            ->with('portarias', $this->portarias)
            ->with('turnos', $this->turnos)
            ->with('coligadaTipoCurso', $this->coligadaTipoCurso)
            ->with('tituloUp', 'Curso')
            ->with('subTitulo', 'Editar Curso');
    }

    /**
     * Update the specified Curso in storage.
     *
     * @param  int $id
     * @param UpdateCursoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCursoRequest $request)
    {
        $curso = $this->cursoRepository->findWithoutFail($id);

        if (empty($curso)) {
            app('flash')->error('Curso not found');

            return redirect(route('cursos.index'));
        }

        $this->cursoRepository->update($request->all(), $id);

        app('flash')->success('Curso updated successfully.');

        return redirect(route('cursos.index'));
    }

    /**
     * Remove the specified Curso from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $curso = $this->cursoRepository->findWithoutFail($id);

        if (empty($curso)) {
            app('flash')->error('Curso not found');

            return redirect(route('cursos.index'));
        }

        $this->cursoRepository->delete($id);

        app('flash')->success('Curso deleted successfully.');

        return redirect(route('cursos.index'));
    }
}
