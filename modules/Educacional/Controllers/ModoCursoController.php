<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateModoCursoRequest;
use Educacional\Requests\UpdateModoCursoRequest;
use Educacional\Repositories\ModoCursoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class ModoCursoController extends AppBaseController
{
    /** @var  ModoCursoRepository */
    private $modoCursoRepository;

    public function __construct(ModoCursoRepository $modoCursoRepo)
    {
        $this->modoCursoRepository = $modoCursoRepo;
    }

    /**
     * Display a listing of the ModoCurso.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->modoCursoRepository->pushCriteria(new RequestCriteria($request));
        $modoCursos = $this->modoCursoRepository->all();

        return view('modoCursos.index')
            ->with('modoCursos', $modoCursos);
    }

    /**
     * Show the form for creating a new ModoCurso.
     *
     * @return Response
     */
    public function create()
    {
        return view('modoCursos.create');
    }

    /**
     * Store a newly created ModoCurso in storage.
     *
     * @param CreateModoCursoRequest $request
     *
     * @return Response
     */
    public function store(CreateModoCursoRequest $request)
    {
        $input = $request->all();

        $this->modoCursoRepository->create($input);

        app('flash')->success('ModoCurso saved successfully.');

        return redirect(route('modoCursos.index'));
    }

    /**
     * Display the specified ModoCurso.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $modoCurso = $this->modoCursoRepository->findWithoutFail($id);

        if (empty($modoCurso)) {
            app('flash')->error('ModoCurso not found');

            return redirect(route('modoCursos.index'));
        }

        return view('modoCursos.show')->with('modoCurso', $modoCurso);
    }

    /**
     * Show the form for editing the specified ModoCurso.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $modoCurso = $this->modoCursoRepository->findWithoutFail($id);

        if (empty($modoCurso)) {
            app('flash')->error('ModoCurso not found');

            return redirect(route('modoCursos.index'));
        }

        return view('modoCursos.edit')->with('modoCurso', $modoCurso);
    }

    /**
     * Update the specified ModoCurso in storage.
     *
     * @param  int              $id
     * @param UpdateModoCursoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateModoCursoRequest $request)
    {
        $modoCurso = $this->modoCursoRepository->findWithoutFail($id);

        if (empty($modoCurso)) {
            app('flash')->error('ModoCurso not found');

            return redirect(route('modoCursos.index'));
        }

        $this->modoCursoRepository->update($request->all(), $id);

        app('flash')->success('ModoCurso updated successfully.');

        return redirect(route('modoCursos.index'));
    }

    /**
     * Remove the specified ModoCurso from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $modoCurso = $this->modoCursoRepository->findWithoutFail($id);

        if (empty($modoCurso)) {
            app('flash')->error('ModoCurso not found');

            return redirect(route('modoCursos.index'));
        }

        $this->modoCursoRepository->delete($id);

        app('flash')->success('ModoCurso deleted successfully.');

        return redirect(route('modoCursos.index'));
    }
}
