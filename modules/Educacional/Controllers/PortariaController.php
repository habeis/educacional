<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreatePortariaRequest;
use Educacional\Requests\UpdatePortariaRequest;
use Educacional\Repositories\PortariaRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class PortariaController extends AppBaseController
{
    /** @var  PortariaRepository */
    private $portariaRepository;

    public function __construct(PortariaRepository $portariaRepo)
    {
        $this->portariaRepository = $portariaRepo;
    }

    /**
     * Display a listing of the Portaria.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->portariaRepository->pushCriteria(new RequestCriteria($request));
        $portarias = $this->portariaRepository->all();

        return view('portarias.index')
            ->with('portarias', $portarias);
    }

    /**
     * Show the form for creating a new Portaria.
     *
     * @return Response
     */
    public function create()
    {
        return view('portarias.create');
    }

    /**
     * Store a newly created Portaria in storage.
     *
     * @param CreatePortariaRequest $request
     *
     * @return Response
     */
    public function store(CreatePortariaRequest $request)
    {
        $input = $request->all();

        $this->portariaRepository->create($input);

        app('flash')->success('Portaria saved successfully.');

        return redirect(route('portarias.index'));
    }

    /**
     * Display the specified Portaria.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $portaria = $this->portariaRepository->findWithoutFail($id);

        if (empty($portaria)) {
            app('flash')->error('Portaria not found');

            return redirect(route('portarias.index'));
        }

        return view('portarias.show')->with('portaria', $portaria);
    }

    /**
     * Show the form for editing the specified Portaria.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $portaria = $this->portariaRepository->findWithoutFail($id);

        if (empty($portaria)) {
            app('flash')->error('Portaria not found');

            return redirect(route('portarias.index'));
        }

        return view('portarias.edit')->with('portaria', $portaria);
    }

    /**
     * Update the specified Portaria in storage.
     *
     * @param  int              $id
     * @param UpdatePortariaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePortariaRequest $request)
    {
        $portaria = $this->portariaRepository->findWithoutFail($id);

        if (empty($portaria)) {
            app('flash')->error('Portaria not found');

            return redirect(route('portarias.index'));
        }

        $portaria = $this->portariaRepository->update($request->all(), $id);

        app('flash')->success('Portaria updated successfully.');

        return redirect(route('portarias.index'));
    }

    /**
     * Remove the specified Portaria from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $portaria = $this->portariaRepository->findWithoutFail($id);

        if (empty($portaria)) {
            app('flash')->error('Portaria not found');

            return redirect(route('portarias.index'));
        }

        $this->portariaRepository->delete($id);

        app('flash')->success('Portaria deleted successfully.');

        return redirect(route('portarias.index'));
    }
}
