<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateModalidadeRequest;
use Educacional\Requests\UpdateModalidadeRequest;
use Educacional\Repositories\ModalidadeRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class ModalidadeController extends AppBaseController
{
    /** @var  ModalidadeRepository */
    private $modalidadeRepository;

    public function __construct(ModalidadeRepository $modalidadeRepo)
    {
        $this->modalidadeRepository = $modalidadeRepo;
    }

    /**
     * Display a listing of the Modalidade.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->modalidadeRepository->pushCriteria(new RequestCriteria($request));
        $modalidades = $this->modalidadeRepository->all();

        return view('modalidades.index')
            ->with('modalidades', $modalidades);
    }

    /**
     * Show the form for creating a new Modalidade.
     *
     * @return Response
     */
    public function create()
    {
        return view('modalidades.create');
    }

    /**
     * Store a newly created Modalidade in storage.
     *
     * @param CreateModalidadeRequest $request
     *
     * @return Response
     */
    public function store(CreateModalidadeRequest $request)
    {
        $input = $request->all();

        $this->modalidadeRepository->create($input);

        app('flash')->success('Modalidade saved successfully.');

        return redirect(route('modalidades.index'));
    }

    /**
     * Display the specified Modalidade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $modalidade = $this->modalidadeRepository->findWithoutFail($id);

        if (empty($modalidade)) {
            app('flash')->error('Modalidade not found');

            return redirect(route('modalidades.index'));
        }

        return view('modalidades.show')->with('modalidade', $modalidade);
    }

    /**
     * Show the form for editing the specified Modalidade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $modalidade = $this->modalidadeRepository->findWithoutFail($id);

        if (empty($modalidade)) {
            app('flash')->error('Modalidade not found');

            return redirect(route('modalidades.index'));
        }

        return view('modalidades.edit')->with('modalidade', $modalidade);
    }

    /**
     * Update the specified Modalidade in storage.
     *
     * @param  int $id
     * @param UpdateModalidadeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateModalidadeRequest $request)
    {
        $modalidade = $this->modalidadeRepository->findWithoutFail($id);

        if (empty($modalidade)) {
            app('flash')->error('Modalidade not found');

            return redirect(route('modalidades.index'));
        }

        $modalidade = $this->modalidadeRepository->update($request->all(), $id);

        app('flash')->success('Modalidade updated successfully.');

        return redirect(route('modalidades.index'));
    }

    /**
     * Remove the specified Modalidade from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $modalidade = $this->modalidadeRepository->findWithoutFail($id);

        if (empty($modalidade)) {
            app('flash')->error('Modalidade not found');

            return redirect(route('modalidades.index'));
        }

        $this->modalidadeRepository->delete($id);

        app('flash')->success('Modalidade deleted successfully.');

        return redirect(route('modalidades.index'));
    }
}
