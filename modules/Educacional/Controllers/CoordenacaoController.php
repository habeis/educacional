<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateCoordenacaoRequest;
use Educacional\Requests\UpdateCoordenacaoRequest;
use Educacional\Repositories\CoordenacaoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Routing\Route;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class CoordenacaoController extends AppBaseController
{
    /** @var  CoordenacaoRepository */
    private $coordenacaoRepository;

    private $coordenacao;

    private $titulo;

    public function __construct(CoordenacaoRepository $coordenacaoRepo)
    {

        $this->titulo = "Coordenação";
        $this->coordenacaoRepository = $coordenacaoRepo;

        $this->beforeFilter('@findCoodenacao', ['only' => ['edit','update','destroy','show']]);
    }

    public function findCoodenacao(Route $route)
    {

        $this->coordenacao = $this->coordenacaoRepository->findWithoutFail($route->getParameter('coordenacaos'));
    }
    /**
     * Display a listing of the Coordenacao.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->coordenacaoRepository->pushCriteria(new RequestCriteria($request));
        $coordenacaos = $this->coordenacaoRepository->all();

        return view('coordenacaos.index')
            ->with('coordenacaos', $coordenacaos)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for creating a new Coordenacao.
     *
     * @return Response
     */
    public function create()
    {
        return view('coordenacaos.create')
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Store a newly created Coordenacao in storage.
     *
     * @param CreateCoordenacaoRequest $request
     *
     * @return Response
     */
    public function store(CreateCoordenacaoRequest $request)
    {
        $input = $request->all();

        $this->coordenacaoRepository->create($input);

        app('flash')->success('Coordenação salva com Sucesso.');

        return redirect(route('coordenacaos.index'));
    }

    /**
     * Display the specified Coordenacao.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show()
    {
        //$coordenacao = $this->coordenacaoRepository->findWithoutFail($id);

        if (empty($this->coordenacao)) {
            app('flash')->error('Coordenacao not found');

            return redirect(route('coordenacaos.index'));
        }

        return view('coordenacaos.show')
            ->with('coordenacao', $this->coordenacao)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Coordenacao.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit()
    {
        //$coordenacao = $this->coordenacaoRepository->findWithoutFail($id);

        if (empty($this->coordenacao)) {
            app('flash')->error('Coordenacao not found');

            return redirect(route('coordenacaos.index'));
        }

        return view('coordenacaos.edit')
            ->with('coordenacao', $this->coordenacao)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Update the specified Coordenacao in storage.
     *
     * @param  int              $id
     * @param UpdateCoordenacaoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCoordenacaoRequest $request)
    {
        //$coordenacao = $this->coordenacaoRepository->findWithoutFail($id);

        if (empty($this->coordenacao)) {
            app('flash')->error('Coordenacao not found');

            return redirect(route('coordenacaos.index'));
        }

        $this->coordenacao = $this->coordenacaoRepository->update($request->all(), $id);

        app('flash')->success('Coordenacao updated successfully.');

        return redirect(route('coordenacaos.index'));
    }

    /**
     * Remove the specified Coordenacao from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //$coordenacao = $this->coordenacaoRepository->findWithoutFail($id);

        if (empty($this->coordenacao)) {
            app('flash')->error('Coordenacao not found');

            return redirect(route('coordenacaos.index'));
        }

        $this->coordenacaoRepository->delete($id);

        app('flash')->success('Coordenacao deleted successfully.');

        return redirect(route('coordenacaos.index'));
    }
}
