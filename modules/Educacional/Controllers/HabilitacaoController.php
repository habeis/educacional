<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateHabilitacaoRequest;
use Educacional\Requests\UpdateHabilitacaoRequest;
use Educacional\Repositories\HabilitacaoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class HabilitacaoController extends AppBaseController
{
    /** @var  HabilitacaoRepository */
    private $habilitacaoRepository;

    public function __construct(HabilitacaoRepository $habilitacaoRepo)
    {
        $this->habilitacaoRepository = $habilitacaoRepo;
    }

    /**
     * Display a listing of the Habilitacao.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->habilitacaoRepository->pushCriteria(new RequestCriteria($request));
        $habilitacaos = $this->habilitacaoRepository->all();

        return view('habilitacaos.index')
            ->with('habilitacaos', $habilitacaos);
    }

    /**
     * Show the form for creating a new Habilitacao.
     *
     * @return Response
     */
    public function create()
    {
        return view('habilitacaos.create');
    }

    /**
     * Store a newly created Habilitacao in storage.
     *
     * @param CreateHabilitacaoRequest $request
     *
     * @return Response
     */
    public function store(CreateHabilitacaoRequest $request)
    {
        $input = $request->all();

        $this->habilitacaoRepository->create($input);

        app('flash')->success('Habilitacao saved successfully.');

        return redirect(route('habilitacaos.index'));
    }

    /**
     * Display the specified Habilitacao.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $habilitacao = $this->habilitacaoRepository->findWithoutFail($id);

        if (empty($habilitacao)) {
            app('flash')->error('Habilitacao not found');

            return redirect(route('habilitacaos.index'));
        }

        return view('habilitacaos.show')->with('habilitacao', $habilitacao);
    }

    /**
     * Show the form for editing the specified Habilitacao.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $habilitacao = $this->habilitacaoRepository->findWithoutFail($id);

        if (empty($habilitacao)) {
            app('flash')->error('Habilitacao not found');

            return redirect(route('habilitacaos.index'));
        }

        return view('habilitacaos.edit')->with('habilitacao', $habilitacao);
    }

    /**
     * Update the specified Habilitacao in storage.
     *
     * @param  int              $id
     * @param UpdateHabilitacaoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHabilitacaoRequest $request)
    {
        $habilitacao = $this->habilitacaoRepository->findWithoutFail($id);

        if (empty($habilitacao)) {
            app('flash')->error('Habilitacao not found');

            return redirect(route('habilitacaos.index'));
        }

        $habilitacao = $this->habilitacaoRepository->update($request->all(), $id);

        app('flash')->success('Habilitacao updated successfully.');

        return redirect(route('habilitacaos.index'));
    }

    /**
     * Remove the specified Habilitacao from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $habilitacao = $this->habilitacaoRepository->findWithoutFail($id);

        if (empty($habilitacao)) {
            app('flash')->error('Habilitacao not found');

            return redirect(route('habilitacaos.index'));
        }

        $this->habilitacaoRepository->delete($id);

        app('flash')->success('Habilitacao deleted successfully.');

        return redirect(route('habilitacaos.index'));
    }
}
