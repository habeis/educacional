<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Repositories\CursoRepository;
use Educacional\Repositories\DisciplinaRepository;
use Educacional\Repositories\MatrizCurricularRepository;
use Educacional\Repositories\PeriodoRepository;
use Educacional\Repositories\TipoDisciplinaRepository;
use Educacional\Requests\CreateMatrizCurricularDisciplinaRequest;
use Educacional\Requests\UpdateMatrizCurricularDisciplinaRequest;
use Educacional\Repositories\MatrizCurricularDisciplinaRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Educacional\Repositories\CreditoValorRepository;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class MatrizCurricularDisciplinaController extends AppBaseController
{
    /** @var  MatrizCurricularDisciplinaRepository */
    private $matrizCuDisRepo;

    private $curso;

    public function __construct(
        MatrizCurricularDisciplinaRepository $matrizDisciplinaRepo,
        Request $request,
        CursoRepository $cursoRepository
    ) {
        $this->curso = $cursoRepository->findWithoutFail($request['cursoId']);

        $this->matrizCuDisRepo = $matrizDisciplinaRepo;
    }

    /**
     * Display a listing of the MatrizCurricularDisciplina.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, MatrizCurricularRepository $matrizCuRepository)
    {

        $matrizCurricular = $matrizCuRepository->find($request->get('matriz_curricular'));

        $this->matrizCuDisRepo->pushCriteria(new RequestCriteria($request));

        $matrizCurDisciplinas = $this->matrizCuDisRepo->findMatrizCurricular($matrizCurricular->id);

        return view('matrizCurricularDisciplinas.index')
            ->with('curso', $this->curso)
            ->with('matrizCurricularDisciplinas', $matrizCurDisciplinas)
            ->with('tituloUp', 'Matriz Curricular')
            ->with('subTitulo', 'Listar Disciplinas da Matriz Curricular')
            ->with('matrizCurricular', $matrizCurricular);
    }

    /**
     * Show the form for creating a new MatrizCurricularDisciplina.
     *
     * @return Response
     */
    public function create(
        DisciplinaRepository $discRepository,
        Request $request,
        MatrizCurricularRepository $matrizCuRepository,
        CreditoValorRepository $credValorRepository,
        PeriodoRepository $periodoRepository,
        TipoDisciplinaRepository $tipoDisRepository
    ) {

        $disciplina = $discRepository->lists('nome', 'id');

        $creditoValor = $credValorRepository->lists('valor', 'id');

        $periodos = $periodoRepository->lists('descricao', 'id');

        $tipoDisciplinas = $tipoDisRepository->lists('descricao', 'id');

        $matrizCurricular = $matrizCuRepository->find($request->get('matriz_curricular'));

        return view('matrizCurricularDisciplinas.create')
            ->with('curso', $this->curso)
            ->with('tituloUp', 'Disciplina(s) da Matriz Curricular')
            ->with('subTitulo', 'Cadastrar nova Matriz Curricular Disciplina')
            ->with('matrizCurricular', $matrizCurricular)
            ->with('creditoValor', $creditoValor)
            ->with('periodos', $periodos)
            ->with('tipoDisciplinas', $tipoDisciplinas)
            ->with('disciplina', $disciplina);
    }

    /**
     * Store a newly created MatrizCurricularDisciplina in storage.
     *
     * @param CreateMatrizCurricularDisciplinaRequest $request
     *
     * @return Response
     */
    public function store(CreateMatrizCurricularDisciplinaRequest $request, DisciplinaRepository $discRepository)
    {
        $input = $request->all();

        // Pesquisa a disciplina para colocar o nome da mesma na MatrizCurricularDisciplina
        $disciplina = $discRepository->find($input['disciplina_id']);

        $input['nome_disciplina'] = $disciplina->nome;

        $this->matrizCuDisRepo->create($input);

        app('flash')->success('Disciplina na Matriz Curricular cadastrada com sucesso.');

        return redirect(
            route(
                'matrizCurricularDisciplinas.index',
                ['matriz_curricular' => $input ['matriz_curricular_id'], 'cursoId'=> $this->curso->id]
            )
        );
    }

    /**
     * Display the specified MatrizCurricularDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request, MatrizCurricularRepository $matrizCuRepository)
    {
        $matrizCurDisciplina = $this->matrizCuDisRepo->findWithoutFail($id);

        $matrizCurricular = $matrizCuRepository->find($request->get('matriz_curricular'));

        if (empty($matrizCurDisciplina)) {
            app('flash')->error('Nenhuma Matriz Curricular disciplina encontrada');

            return redirect(route('matrizCurricularDisciplinas.index'));
        }

        return view('matrizCurricularDisciplinas.show')
            ->with('curso', $this->curso)
            ->with('matrizCurricularDisciplina', $matrizCurDisciplina)
            ->with('matrizCurricular', $request['matriz_curricular'])
            ->with('tituloUp', 'Matriz Curricular: ' . $matrizCurricular->curso->nome . " Versão: " . $matrizCurricular->versao)
            ->with('subTitulo', 'Visualizar Matriz Curricular');
    }

    /**
     * Show the form for editing the specified MatrizCurricularDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit(
        $id,
        Request $request,
        MatrizCurricularRepository $matrizCuRepository,
        DisciplinaRepository $discRepository,
        PeriodoRepository $periodoRepository,
        TipoDisciplinaRepository $tipoDisRepository,
        CreditoValorRepository $credValorRepository
    ) {

        $matrizCurricular = $matrizCuRepository->find($request->get('matriz_curricular'));

        $disciplina = $discRepository->lists('nome', 'id');

        $periodos = $periodoRepository->lists('descricao', 'id');

        $tipoDisciplinas = $tipoDisRepository->lists('descricao', 'id');

        $creditoValor = $credValorRepository->lists('valor', 'id');

        $matrizCurDisciplina = $this->matrizCuDisRepo->findWithoutFail($id);

        if (empty($matrizCurDisciplina)) {
            app('flash')->error('Nenhuma Matriz Curricular disciplina encontrada');

            return redirect(route('matrizCurricularDisciplinas.index'));
        }

        return view('matrizCurricularDisciplinas.edit')
            ->with('curso', $this->curso)
            ->with('matrizCurricularDisciplina', $matrizCurDisciplina)
            ->with('matrizCurricular', $matrizCurricular)
            ->with('disciplina', $disciplina)
            ->with('periodos', $periodos)
            ->with('tipoDisciplinas', $tipoDisciplinas)
            ->with('creditoValor', $creditoValor)
            ->with('tituloUp', 'Disciplina(s) da Matriz Curricular')
            ->with('subTitulo', 'Editar Matriz Curricular Disciplina');
    }

    /**
     * Update the specified MatrizCurricularDisciplina in storage.
     *
     * @param  int $id
     * @param UpdateMatrizCurricularDisciplinaRequest $request
     *
     * @return Response
     */
    public function update(
        $id,
        UpdateMatrizCurricularDisciplinaRequest $request,
        MatrizCurricularRepository $matrizCuRepository
    ) {
        $matrizCurDisciplina = $this->matrizCuDisRepo->findWithoutFail($id);

        $matrizCurricular = $matrizCuRepository->find($request->get('matriz_curricular'));

        if (empty($matrizCurDisciplina)) {
            app('flash')->error('Nenhuma Matriz Curricular disciplina encontrada');

            return redirect(route('matrizCurricularDisciplinas.index'));
        }

        $this->matrizCuDisRepo->update($request->all(), $id);

        app('flash')->success('Disciplina na Matriz Curricular Atualizada com sucesso.');

        return redirect(
            route(
                'matrizCurricularDisciplinas.index',
                ['matriz_curricular' => $matrizCurricular->id,'cursoId' => $this->curso->id]
            )
        );
    }

    /**
     * Remove the specified MatrizCurricularDisciplina from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {

        $matrizCurDisciplina = $this->matrizCuDisRepo->findWithoutFail($id);

        if (empty($matrizCurDisciplina)) {
            app('flash')->error('Nenhuma Disciplina na Matriz Curricular foi encontrada');

            return redirect(route('matrizCurricularDisciplinas.index'));
        }

        $this->matrizCuDisRepo->delete($id);

        app('flash')->success('Disciplina na Matriz Curricular deletado com sucesso.');


        return redirect(route('matrizCurricularDisciplinas.index', ['matriz_curricular' => $request['matriz_curricular'], 'cursoId' => $this->curso->id]));
    }
}
