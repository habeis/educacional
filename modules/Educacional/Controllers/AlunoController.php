<?php

namespace Educacional\Controllers;

use Educacional\Models\Aluno;
use Hash;
use App\Http\Requests;
use EduAdmin\Repositories\UserRepository;
use Educacional\Repositories\EnderecoRepository;
use Educacional\Requests\CreateAlunoRequest;
use Educacional\Requests\UpdateAlunoRequest;
use Educacional\Repositories\AlunoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Nucleo\Repositories\PessoaRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class AlunoController extends AppBaseController
{
    /** @var  AlunoRepository */
    private $alunoRepository;

    private $titulo;

    private $endereco;

    private $user;

    private $pessoa;

    private $userRepository;

    private $pessoaRepository;

    private $enderecoRepository;

    public function __construct(
        AlunoRepository $alunoRepo,
        EnderecoRepository $enderecoRepository,
        PessoaRepository $pessoaRepository,
        UserRepository $userRepository
    )
    {
        $this->alunoRepository = $alunoRepo;
        $this->enderecoRepository = $enderecoRepository;
        $this->pessoaRepository = $pessoaRepository;
        $this->userRepository = $userRepository;

        $this->titulo = 'Aluno';
    }

    /**
     * Display a listing of the Aluno.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->alunoRepository->pushCriteria(new RequestCriteria($request));
        $alunos = $this->alunoRepository->all();

        return view('alunos.index')
            ->with('alunos', $alunos)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Lista de ' . $this->titulo . 's');
    }

    /**
     * Show the form for creating a new Aluno.
     *
     * @return Response
     */
    public function create()
    {
        return view('alunos.create')
            ->with('endereco', app('models.aendereco'))
            ->with('pessoa', app('nucleo.pessoa'))
            ->with('aluno', app('educacional.aluno'))
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastrar Novo ' . $this->titulo);
    }

    /**
     * Store a newly created Aluno in storage.
     *
     * @param CreateAlunoRequest $request
     *
     * @return Response
     */
    public function store(CreateAlunoRequest $request)
    {
        $input = $request->all();

        $endereco = $this->enderecoRepository->create($input);

        $input['endereco_id'] = $endereco->id;

        $pessoa = $this->pessoaRepository->create($input);

        $input['pessoa_id'] = $pessoa->id;

        $input['password'] = Hash::make($input['pessoa_id']);

        $input['name'] = $input['cpf'];

        $user = $this->userRepository->create($input);

        $input['user_id'] = $user->id;

        $this->alunoRepository->create($input);

        app('flash')->success('Aluno salvo com sucesso.');

        return redirect(route('alunos.index'));
    }

    /**
     * Display the specified Aluno.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $aluno = $this->alunoRepository->findWithoutFail($id);

        if (empty($aluno)) {
            app('flash')->error('Aluno não encontrado');

            return redirect(route('alunos.index'));
        }

        $this->user = $this->userRepository->findWithoutFail($aluno->user_id);

        $this->pessoa = $this->pessoaRepository->findWithoutFail($this->user->pessoa_id);

        $this->endereco = $this->enderecoRepository->findWithoutFail($this->pessoa->endereco_id);

        return view('alunos.show')
            ->with('endereco', $this->endereco)
            ->with('pessoa', $this->pessoa)
            ->with('usuario', $this->user)
            ->with('aluno', $aluno)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Aluno.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $aluno = $this->alunoRepository->findWithoutFail($id);

        if (empty($aluno)) {

            app('flash')->error('Aluno não encontrado');

            return redirect(route('alunos.index'));
        }

        $this->user = $this->userRepository->findWithoutFail($aluno->user_id);

        $this->pessoa = $this->pessoaRepository->findWithoutFail($this->user->pessoa_id);

        $this->endereco = $this->enderecoRepository->findWithoutFail($this->pessoa->endereco_id);

        return view('alunos.edit')
            ->with('endereco', $this->endereco)
            ->with('pessoa', $this->pessoa)
            ->with('usuario', $this->user)
            ->with('aluno', $aluno)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Editar ' . $this->titulo);
    }

    /**
     * Update the specified Aluno in storage.
     *
     * @param  int $id
     * @param UpdateAlunoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlunoRequest $request)
    {

        $aluno = $this->alunoRepository->findWithoutFail($id);

        if (empty($aluno)) {
            app('flash')->error('Aluno não encontrado');

            return redirect(route('alunos.index'));
        }

        $aluno = $this->alunoRepository->update($request->all(), $id);
        $this->user = $this->userRepository->update($request->all(), $aluno->user_id);
        $this->pessoa = $this->pessoaRepository->update($request->all(), $this->user->pessoa_id);
        $this->endereco = $this->enderecoRepository->update($request->all(), $this->pessoa->endereco_id);


        app('flash')->success('Aluno atualizado com sucesso.');

        return redirect(route('alunos.index'));
    }

    /**
     * Remove the specified Aluno from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $aluno = $this->alunoRepository->findWithoutFail($id);

        if (empty($aluno)) {
            app('flash')->error('Aluno not found');

            return redirect(route('alunos.index'));
        }

        $this->alunoRepository->delete($id);

        app('flash')->success('Aluno deletado com sucesso.');

        return redirect(route('alunos.index'));
    }
}
