<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateMatrizCurricularRequest;
use Educacional\Requests\UpdateMatrizCurricularRequest;
use Educacional\Repositories\MatrizCurricularRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Educacional\Repositories\CursoRepository;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class MatrizCurricularController extends AppBaseController
{
    /** @var  MatrizCurricularRepository */
    private $matrizCurricularRepository;

    private $curso;


    public function __construct(
        MatrizCurricularRepository $matrizCurricularRepo,
        Request $request,
        CursoRepository $cursoRepository
    ) {
        $this->curso = $cursoRepository->findWithoutFail($request['cursoId']);

        $this->matrizCurricularRepository = $matrizCurricularRepo;
    }

    /**
     * Display a listing of the MatrizCurricular.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->matrizCurricularRepository->pushCriteria(new RequestCriteria($request));

        $matrizCurriculars = $this->matrizCurricularRepository->findWhere(['curso_id' => $this->curso->id]);

        return view('matrizCurriculars.index')
            ->with('matrizCurriculars', $matrizCurriculars)
            ->with('curso', $this->curso)
            ->with('tituloUp', 'Matriz Curricular')
            ->with('subTitulo', 'Lista de Matriz Curricular');
    }

    /**
     * Show the form for creating a new MatrizCurricular.
     *
     * @return Response
     */
    public function create()
    {
        
        return view('matrizCurriculars.create')
            ->with('curso', $this->curso)
            ->with('tituloUp', 'Matriz Curricular')
            ->with('subTitulo', 'Adicionar nova Matriz Curricular');
    }

    /**
     * Store a newly created MatrizCurricular in storage.
     *
     * @param CreateMatrizCurricularRequest $request
     *
     * @return Response
     */
    public function store(CreateMatrizCurricularRequest $request)
    {
        $input = $request->all();

        $this->matrizCurricularRepository->create($input);

        app('flash')->success('Matriz Curricular Salvo com Sucesso.');

        return redirect(route('matrizCurriculars.index', ['cursoId' => $this->curso->id]));
    }

    /**
     * Display the specified MatrizCurricular.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $matrizCurricular = $this->matrizCurricularRepository->findWithoutFail($id);

        if (empty($matrizCurricular)) {
            app('flash')->error('MatrizCurricular not found');

            return redirect(route('matrizCurriculars.index'));
        }

        return view('matrizCurriculars.show')
            ->with('curso', $this->curso)
            ->with('matrizCurricular', $matrizCurricular)
            ->with('tituloUp', 'Matriz Curricular')
            ->with('subTitulo', 'Visualizar de Matriz Curricular');
    }

    /**
     * Show the form for editing the specified MatrizCurricular.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $matrizCurricular = $this->matrizCurricularRepository->findWithoutFail($id);

        if (empty($matrizCurricular)) {
            app('flash')->error('MatrizCurricular not found');

            return redirect(route('matrizCurriculars.index'));
        }

        return view('matrizCurriculars.edit')
            ->with('matrizCurricular', $matrizCurricular)
            ->with('curso', $this->curso)
            ->with('tituloUp', 'Matriz Curricular')
            ->with('subTitulo', 'Editar Matriz Curricular');
    }

    /**
     * Update the specified MatrizCurricular in storage.
     *
     * @param  int $id
     * @param UpdateMatrizCurricularRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMatrizCurricularRequest $request)
    {
        $matrizCurricular = $this->matrizCurricularRepository->findWithoutFail($id);

        if (empty($matrizCurricular)) {
            app('flash')->error('MatrizCurricular not found');

            return redirect(route('matrizCurriculars.index'));
        }

        $this->matrizCurricularRepository->update($request->all(), $id);

        app('flash')->success('MatrizCurricular updated successfully.');

        return redirect(route('matrizCurriculars.index', ['cursoId' => $this->curso->id]));
    }

    /**
     * Remove the specified MatrizCurricular from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $matrizCurricular = $this->matrizCurricularRepository->findWithoutFail($id);

        if (empty($matrizCurricular)) {
            app('flash')->error('MatrizCurricular not found');

            return redirect(route('matrizCurriculars.index'));
        }

        $this->matrizCurricularRepository->delete($id);

        app('flash')->success('MatrizCurricular deleted successfully.');

        return redirect(route('matrizCurriculars.index', ['cursoId' => $this->curso->id]));
    }
}
