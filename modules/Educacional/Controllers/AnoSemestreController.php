<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Models\Coligada;
use Educacional\Requests\CreateAnoSemestreRequest;
use Educacional\Requests\UpdateAnoSemestreRequest;
use Educacional\Repositories\AnoSemestreRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class AnoSemestreController extends AppBaseController
{
    /** @var  AnoSemestreRepository */
    private $anoSemestreRepository;

    private $coligadaTipoCurso;

    public function __construct(AnoSemestreRepository $anoSemestreRepo)
    {
        $this->anoSemestreRepository = $anoSemestreRepo;
    }

    /**
     * Display a listing of the AnoSemestre.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->anoSemestreRepository->pushCriteria(new RequestCriteria($request));
        $anoSemestres = $this->anoSemestreRepository->all();

        return view('anoSemestres.index')
            ->with('anoSemestres', $anoSemestres);
    }

    /**
     * Show the form for creating a new AnoSemestre.
     *
     * @return Response
     */
    public function create()
    {
        return view('anoSemestres.create');
    }

    /**
     * Store a newly created AnoSemestre in storage.
     *
     * @param CreateAnoSemestreRequest $request
     *
     * @return Response
     */
    public function store(CreateAnoSemestreRequest $request)
    {
        $input = $request->all();

        $this->anoSemestreRepository->create($input);

        app('flash')->success('AnoSemestre saved successfully.');

        return redirect(route('anoSemestres.index'));
    }

    /**
     * Display the specified AnoSemestre.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $anoSemestre = $this->anoSemestreRepository->findWithoutFail($id);

        if (empty($anoSemestre)) {
            app('flash')->error('AnoSemestre not found');

            return redirect(route('anoSemestres.index'));
        }

        return view('anoSemestres.show')->with('anoSemestre', $anoSemestre);
    }

    /**
     * Show the form for editing the specified AnoSemestre.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $anoSemestre = $this->anoSemestreRepository->findWithoutFail($id);

        if (empty($anoSemestre)) {
            app('flash')->error('AnoSemestre not found');

            return redirect(route('anoSemestres.index'));
        }
        $this->coligadaTipoCurso = Coligada::first()->tipoCursos()->get();

        return view('anoSemestres.edit')
            ->with('anoSemestre', $anoSemestre)
            ->with('coligadaTipoCurso', $this->coligadaTipoCurso);
    }

    /**
     * Update the specified AnoSemestre in storage.
     *
     * @param  int $id
     * @param UpdateAnoSemestreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnoSemestreRequest $request)
    {
        $anoSemestre = $this->anoSemestreRepository->findWithoutFail($id);

        if (empty($anoSemestre)) {
            app('flash')->error('AnoSemestre not found');

            return redirect(route('anoSemestres.index'));
        }

        $anoSemestre = $this->anoSemestreRepository->update($request->all(), $id);

        app('flash')->success('AnoSemestre updated successfully.');

        return redirect(route('anoSemestres.index'));
    }

    /**
     * Remove the specified AnoSemestre from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $anoSemestre = $this->anoSemestreRepository->findWithoutFail($id);

        if (empty($anoSemestre)) {
            app('flash')->error('AnoSemestre not found');

            return redirect(route('anoSemestres.index'));
        }

        $this->anoSemestreRepository->delete($id);

        app('flash')->success('AnoSemestre deleted successfully.');

        return redirect(route('anoSemestres.index'));
    }
}
