<?php

namespace Educacional\Controllers;

use Educacional\Repositories\AlunoTurmaRepository;
use InfyOm\Generator\Controller\AppBaseController;
use App\Http\Requests;
use Illuminate\Http\Request;
use Response;
use Flash;

/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05/07/16
 * Time: 23:47
 */
class AlunoHistoricoController extends AppBaseController
{

    private $alunoTurmaRepository;
    /** @var  titulo */
    private $titulo;

    /** @var  $matricula */
    private $matricula;

    public function __construct(Request $request, AlunoTurmaRepository $alunoTurmaRepository)
    {
        $this->alunoTurmaRepository = $alunoTurmaRepository;

        $this->matricula = $request['matricula'];
        $this->titulo = 'Histórico ';
    }

    /**
     * Show the form for creating a new AlunoTurma.
     *
     * @return Response
     */
    public function index(Request $request)
    {


        return view('alunos.historico.index')
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Histórico');

    }

    /**
     * Show the form for creating a new AlunoTurma.
     *
     * @return Response
     */
    public function historicoDetalhado(Request $request)
    {

        return view('alunos.historico.detalhado');

    }

    /**
     * Display the specified AlunoTurma.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show()
    {

        return view('alunos.historico.detalhado')
            ->with('matricula', $this->matricula);
    }

    public function detalhado()
    {

        // Consulta Todas Disciplinas que o aluno Cursou
        $alunoTurma = $this->alunoTurmaRepository->consultaDisciplinasCursada($this->matricula);
        //dd($disciplinas);
        return view('alunos.historico.detalhado')
            ->with('matricula', $this->matricula)
            ->with('alunoTurmas', $alunoTurma)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Histórico Detalhado');
    }
}