<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateCreditoValorRequest;
use Educacional\Requests\UpdateCreditoValorRequest;
use Educacional\Repositories\CreditoValorRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class CreditoValorController extends AppBaseController
{
    /** @var  CreditoValorRepository */
    private $creditoValorRepository;

    public function __construct(CreditoValorRepository $creditoValorRepo)
    {
        $this->creditoValorRepository = $creditoValorRepo;
    }

    /**
     * Display a listing of the CreditoValor.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->creditoValorRepository->pushCriteria(new RequestCriteria($request));
        $creditoValors = $this->creditoValorRepository->all();

        return view('creditoValors.index')
            ->with('creditoValors', $creditoValors)
            ->with('tituloUp', 'Valor Credito')
            ->with('subTitulo', 'Listar valor de Credito');
    }

    /**
     * Show the form for creating a new CreditoValor.
     *
     * @return Response
     */
    public function create()
    {
        return view('creditoValors.create')
            ->with('tituloUp', 'Valor Credito')
            ->with('subTitulo', 'Cadastrar Valor de Credito');
    }

    /**
     * Store a newly created CreditoValor in storage.
     *
     * @param CreateCreditoValorRequest $request
     *
     * @return Response
     */
    public function store(CreateCreditoValorRequest $request)
    {
        $input = $request->all();

        $this->creditoValorRepository->create($input);

        app('flash')->success('CreditoValor saved successfully.');

        return redirect(route('creditoValors.index'));
    }

    /**
     * Display the specified CreditoValor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $creditoValor = $this->creditoValorRepository->findWithoutFail($id);

        if (empty($creditoValor)) {
            app('flash')->error('CreditoValor not found');

            return redirect(route('creditoValors.index'));
        }

        return view('creditoValors.show')
            ->with('creditoValor', $creditoValor)
            ->with('tituloUp', 'Valor Credito')
            ->with('subTitulo', 'Visualizar valor do Credito.');
    }

    /**
     * Show the form for editing the specified CreditoValor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $creditoValor = $this->creditoValorRepository->findWithoutFail($id);

        if (empty($creditoValor)) {
            app('flash')->error('CreditoValor not found');

            return redirect(route('creditoValors.index'));
        }

        return view('creditoValors.edit')
            ->with('creditoValor', $creditoValor)
            ->with('tituloUp', 'Valor Credito')
            ->with('subTitulo', 'Editar valor do Credito');
    }

    /**
     * Update the specified CreditoValor in storage.
     *
     * @param  int $id
     * @param UpdateCreditoValorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCreditoValorRequest $request)
    {
        $creditoValor = $this->creditoValorRepository->findWithoutFail($id);

        if (empty($creditoValor)) {
            app('flash')->error('CreditoValor not found');

            return redirect(route('creditoValors.index'));
        }

        $creditoValor = $this->creditoValorRepository->update($request->all(), $id);

        app('flash')->success('CreditoValor updated successfully.');

        return redirect(route('creditoValors.index'));
    }

    /**
     * Remove the specified CreditoValor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $creditoValor = $this->creditoValorRepository->findWithoutFail($id);

        if (empty($creditoValor)) {
            app('flash')->error('CreditoValor not found');

            return redirect(route('creditoValors.index'));
        }

        $this->creditoValorRepository->delete($id);

        app('flash')->success('CreditoValor deleted successfully.');

        return redirect(route('creditoValors.index'));
    }
}
