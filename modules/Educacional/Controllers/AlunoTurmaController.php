<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use EduAdmin\Repositories\UserRepository;
use Educacional\Models\AlunoTurma;
use Educacional\Repositories\AlunoRepository;
use Educacional\Repositories\AlunoTurmaDisciplinaRepository;
use Educacional\Repositories\ColigadaRepository;
use Educacional\Repositories\CursoRepository;
use Educacional\Repositories\EnderecoRepository; 
use Educacional\Repositories\TurmaDisciplinaRepository;
use Educacional\Repositories\TurmaRepository;
use Educacional\Requests\CreateAlunoTurmaRequest;
use Educacional\Requests\UpdateAlunoTurmaRequest;
use Educacional\Repositories\AlunoTurmaRepository;
use Illuminate\Http\Request;
use Flash; 
use InfyOm\Generator\Controller\AppBaseController;
use Nucleo\Models\Pessoa;
use Nucleo\Repositories\PessoaRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response; 

class AlunoTurmaController extends AppBaseController
{
    /** @var  AlunoTurmaRepository */
    private $alunoTurmaRepository;

    /** @var  alunoTurma */
    private $alunoTurma;

    /** @var  titulo*/
    private $titulo;

    /** @var  parametros*/
    private $parametros;

    public function __construct(AlunoTurmaRepository $alunoTurmaRepo, Request $request)
    {

        $tipoCursoId = $request['tipoCursoId'];
        $matricula = $request['matricula'];
        $semestreLetivoId = $request['semestreLetivoId'];
        $cursoId = $request['cursoId'];
        $turmaId = $request['turmaId'];
        // Compacta todos os Parametros
        $this->parametros = compact('tipoCursoId', 'semestreLetivoId', 'cursoId', 'turmaId', 'matricula');

        $this->titulo = 'Matrícula de Aluno(s)';

        $this->alunoTurmaRepository = $alunoTurmaRepo;

    }

    public function alunoCpf(Request $request)
    {
        $tipoCursoId = $request['tipoCursoId'];
        $semestreLetivoId = $request['semestreLetivoId'];
        $cursoId = $request['cursoId'];
        $turmaId = $request['turmaId'];

        return view('.alunoTurmas.aluno_cpf')
            ->with('tituloUp', $this->titulo)
            ->with('tipoCursoId', $tipoCursoId)
            ->with('semestreLetivoId', $semestreLetivoId)
            ->with('cursoId', $cursoId)
            ->with('turmaId', $turmaId)
            ->with('parametros', $this->parametros)
            ->with('subTitulo', 'Matrícula de Calouro');
    }

    /**
     * Show the form for creating a new AlunoTurma.
     *
     * @return Response
     */
    public function create(
        Request $request,
        TurmaRepository $turmaRepository,
        TurmaDisciplinaRepository $turmaDisciplinaRepository)
    {

        $cpf = $request['cpf'];

        // Faz uma consulta se o pessoa contém no banco de dados para preencher o Formulário
        if (empty($cpf)) {
            Flash::error('Não contém valor no CPF.');

            return redirect(route('alunoTurmas.index'));
        }
        //Busca a Pessoa
        $pessoa = Pessoa::where('cpf', '=', $cpf)->first();
        // Se não conter no banco então apenas cria uma instância dos objetos para não irem NULL
        if (empty($pessoa)) {
            $pessoa = app('nucleo.pessoa');

        }

        if (!empty($pessoa)) {
            // Busca o Endereço da pessoa
            $endereco = $pessoa->endereco()->first();
        }

        if (empty($endereco)) {
            $endereco = app('models.endereco');
        }
        $turma = $turmaRepository->find($request['turmaId']);

        if (empty($turma)) {

            Flash::error('Turma Não encontrada');
            return redirect(route('alunoTurmas.index'));
        }
        //Disciplinas da Turma

        // Busca todas as disciplinas daquela Matriz e daquele período selecionando anterior
        //$disciplinaPeriodos = $matCurDisRepository->findWhere(['periodo_id' => $turma->periodo->id, 'matriz_curricular_id' => $this->gradeId]);

        //Disciplinas da Turma
        $selecDiscPeriodo = $turmaDisciplinaRepository->findWhere(['turma_id' => $turma->id],
            ['matrriz_curricular_disciplina_id as turmaDisciplinaId',
                'nome_disciplina',
                'turma_id',
            ]);

        $pessoa->cpf = $cpf;
        //dd($selecDiscPeriodo );

        return view('alunoTurmas.create')
            ->with('alunoTurma', app('educacional.alunoTurma'))
            ->with('tituloUp', $this->titulo)
            ->with('parametros', $this->parametros)
            ->with('endereco', $endereco)
            ->with('pessoa', $pessoa)
            ->with('turma', $turma)
            ->with('aluno', app('educacional.aluno'))
            ->with('semestreLetivoId', $this->parametros['semestreLetivoId'])
            ->with('cursoId', $this->parametros['cursoId'])
            ->with('turmaId', $this->parametros['turmaId'])
            ->with('tipoCursoId', $this->parametros['tipoCursoId'])
            ->with('disciplinaPeriodo', $selecDiscPeriodo)
            ->with('subTitulo', '' . $this->titulo);
    }

    /**
     * Display a listing of the AlunoTurma.
     *
     * @param Request $request
     * @return Response
     */
    public function alunosTurnma(
        Request $request,
        TurmaRepository $turmaRepository,
        TurmaDisciplinaRepository $turmaDisciplinaRepository
    )
    {

        $this->alunoTurmaRepository->pushCriteria(new RequestCriteria($request));

        // Se Não conter valor em Matrícula irá realizar matrícula de Calouro
        if (empty($this->parametros['matricula'])) {


            $alunoTurmas = $this->alunoTurmaRepository->paginate(15);

            $turma = $turmaRepository->find($request['turmaId']);

            return view('alunoTurmas.aluno_turma')
                ->with('alunoTurmas', $alunoTurmas)
                ->with('turma', $turma)
                ->with('tituloUp', $this->titulo)
                ->with('parametros', $this->parametros)
                ->with('subTitulo', 'Matrícula de Calouro');
        }

        // Se não irá realizar matrícula de Veterano
        // Pesquisa aqui para ver em qual turma o aluno está
        // lista todos disciplinas das turmas formadas que contém na grade que o aluno não cursou ainda

        $alunoTurma = $this->alunoTurmaRepository->findByField('matricula', $this->parametros['matricula']);

        //Disciplinas da Turma lista todas disciplians que o aluno ainda não cursou

        // Pega o curso da Turma para pegar todas as disciplinas da Turma
        $this->alunoTurma = $this->alunoTurmaRepository->findWhere(['matricula' => $this->parametros['matricula']])
            ->first();

        // Consulta Todas Disciplinas que o aluno Cursou
        $disciplinaRetirar = $this->alunoTurmaRepository->consultaDisciplinasCursadaId($this->parametros['matricula']);

        // Consulta todas disciplinas das turmas de um determinado Curso e do semestre atual e retira todas que o aluno já cursou
        $turmasDisciplinas = $turmaDisciplinaRepository->disciplinasCursar(
            $this->parametros['semestreLetivoId'],
            $this->alunoTurma->turma->curso_id,
            $disciplinaRetirar
        );

        $this->alunoTurmaRepository->retirarDisciplinaArra($turmasDisciplinas, $disciplinaRetirar);

        //tirar as disciplinas
        return view('alunoTurmas.veterano.matricula_veterano')
            ->with('alunoTurma', $alunoTurma->first())
            ->with('aluno', $alunoTurma->first()->aluno)
            ->with('endereco', $alunoTurma->first()->aluno->user->pessoa->endereco)
            ->with('pessoa', $alunoTurma->first()->aluno->user->pessoa)
            ->with('turma', $alunoTurma->first()->turma)            
            ->with('parametros', $this->parametros)
            ->with('semestreLetivoId', $this->parametros['semestreLetivoId'])
            ->with('cursoId', $this->parametros['cursoId'])
            ->with('turmaId', $this->parametros['turmaId'])
            ->with('tipoCursoId', $this->parametros['tipoCursoId'])
            ->with('disciplinaPeriodo', $turmasDisciplinas)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Matrícula de Veterano');

    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function index(Request $request, ColigadaRepository $coligadaRepository)
    {

        $this->alunoTurmaRepository->pushCriteria(new RequestCriteria($request));

        //Busca a Coligada que está cadastrada
        $coligada = $coligadaRepository->first();

        return view('alunoTurmas.index')
            ->with('coligada', $coligada)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Matrícula Calouro / Veterano');
    }

    /**
     * Store a newly created AlunoTurma in storage.
     *
     * @param CreateAlunoTurmaRequest $request
     *
     * @return Response
     */
    public function store(
        CreateAlunoTurmaRequest $request,
        PessoaRepository $pessoaRepository,
        AlunoRepository $alunoRepository,
        EnderecoRepository $enderecoRepository,
        UserRepository $userRepository,
        TurmaRepository $turmaRepository,
        CursoRepository $cursoRepository,
        AlunoTurmaDisciplinaRepository $alunoTurmaDisciplinaRepository)
    {

        $input = $request->all();

        /*$endereco = $this->enderecoRepository->create($input);

        $input['endereco_id'] = $endereco->id;

        $pessoa = $this->pessoaRepository->create($input);

        $input['pessoa_id'] = $pessoa->id;

        $input['password'] = Hash::make($input['pessoa_id']);

        $input['name'] = $input['cpf'];

        $user = $this->userRepository->create($input);

        $input['user_id'] = $user->id;

        $aluno = $this->alunoRepository->create($input);

        $input['aluno_id'] = $aluno->id;
        */
        $aluno = $alunoRepository->salvarAtualizarAluno
        (
            $input,
            $pessoaRepository,
            $enderecoRepository,
            $userRepository
        );

        // Busca um curso de uma determinada Turma
        $curso = $cursoRepository->buscarTurmaCurso($input['turmaId']);
        // Pegando o código da turma
        $input['turma_id'] = $input['turmaId'];

        ///$cursoRepository->update($curso, $curso[0]->id);
        $input['aluno_id'] = $aluno->id;

        if (empty($curso)) {
            Flash::error('Curso Não encontrado da Turma, Entre em contato com Administrador');

            return redirect(route('alunoTurmas.index'));
        }

        if (empty($this->parametros['matricula'])) {

            // Soma mais 1 do que a matrícula que está no banco de dados..
            $curso[0]->matricula = $curso[0]->matricula + 1;

            // A matrícula do aluno é de acordo o curso que o mesmo está cursando.
            $input['matricula'] = $curso[0]->matricula;

            // Atualiza a matrícula do somando +1 o numero da matŕicula do cursoPrecisa atualizar o curso
            $cursoRepository->atualizaMatriculaCurso($curso[0]);

            $alunoTurma = $this->alunoTurmaRepository->create($input);

            // Salva as disciplinas para o aluno
            $alunoTurmaDisciplinaRepository->salvarDisciplinasAluno($input['disciplinaPeriodos'], $alunoTurma->id);

            Flash::success('Aluno Matrículado na Turma com Sucesso.');

            return redirect(route('alunos-turma', $this->parametros));
        }
        $alunoTurma = $this->alunoTurmaRepository->update($input, $input['alunoTurmaId']);


        // Salva as disciplinas para o aluno Veterano
        $alunoTurmaDisciplinaRepository->salvarDisciplinasAluno($input['disciplinaPeriodos'], $alunoTurma->id);

        Flash::success('Aluno Matrículado na Turma com Sucesso.');

        return redirect(route('alunos-turma', $this->parametros));


    }

    /**
     * Display the specified AlunoTurma.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $alunoTurma = $this->alunoTurmaRepository->findWithoutFail($id);

        if (empty($alunoTurma)) {
            Flash::error('AlunoTurma not found');

            return redirect(route('alunoTurmas.index'));
        }

        return view('alunoTurmas.show')
            ->with('parametros', $this->parametros)
            ->with('alunoTurma', $alunoTurma);
    }

    /**
     * Show the form for editing the specified AlunoTurma.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $alunoTurma = $this->alunoTurmaRepository->findWithoutFail($id);

        if (empty($alunoTurma)) {
            Flash::error('AlunoTurma not found');

            return redirect(route('alunoTurmas.index'));
        }

        return view('alunoTurmas.edit')
            ->with('parametros', $this->parametros)
            ->with('alunoTurma', $alunoTurma);
    }

    /**
     * Update the specified AlunoTurma in storage.
     *
     * @param  int $id
     * @param UpdateAlunoTurmaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlunoTurmaRequest $request)
    {
        $alunoTurma = $this->alunoTurmaRepository->findWithoutFail($id);

        if (empty($alunoTurma)) {
            Flash::error('AlunoTurma not found');

            return redirect(route('alunoTurmas.index'));
        }

        $alunoTurma = $this->alunoTurmaRepository->update($request->all(), $id);

        Flash::success('AlunoTurma updated successfully.');

        return redirect(route('alunoTurmas.index'));
    }

    /**
     * Remove the specified AlunoTurma from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $alunoTurma = $this->alunoTurmaRepository->findWithoutFail($id);

        if (empty($alunoTurma)) {
            Flash::error('AlunoTurma not found');

            return redirect(route('alunoTurmas.index'));
        }

        $this->alunoTurmaRepository->delete($id);

        Flash::success('AlunoTurma deleted successfully.');

        return redirect(route('alunoTurmas.index'));
    }
}
