<?php

namespace Educacional\Controllers;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Requests;
use EduAdmin\Repositories\UserRepository;
use Educacional\Repositories\EnderecoRepository;
use Educacional\Repositories\ProfessorRepository;
use Educacional\Requests\CreateProfessorRequest;
use Educacional\Requests\UpdateProfessorRequest;
use Flash;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use Nucleo\Models\Pessoa;
use Nucleo\Repositories\PessoaRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProfessorController extends AppBaseController
{
    /** @var  ProfessorRepository */
    private $professorRepository;

    /** @var  Titulo View */
    private $titulo;

    /** @var  UserRepository */
    private $userRepository;

    /** @var  PessoaRepository */
    private $pessoaRepository;

    /** @var  EnderecoRepository */
    private $enderecoRepository;


    public function __construct(
        ProfessorRepository $professorRepo,
        EnderecoRepository $enderecoRepository,
        PessoaRepository $pessoaRepository,
        UserRepository $userRepository

    )
    {
        $this->pessoaRepository = $pessoaRepository;

        $this->enderecoRepository = $enderecoRepository;

        $this->professorRepository = $professorRepo;

        $this->userRepository = $userRepository;

        $this->titulo = "Professor";
    }

    /**
     * Display a listing of the Professor.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->professorRepository->pushCriteria(new RequestCriteria($request));
        $professores = $this->professorRepository->paginate(10);

        return view('professores.index')
            ->with('professores', $professores)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Listar ' . $this->titulo);
    }


    public function professorCpf()
    {
        return view('professores.professor_cpf')
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for creating a new Professor.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $cpf = $request['cpf'];

        // Faz uma consulta se o pessoa contém no banco de dados para preencher o Formulário
        if (empty($cpf)) {
            Flash::error('Não contém valor no CPF.');

            return redirect(route('professores.index'));
        }
        //Busca a Pessoa
        $pessoa = Pessoa::where('cpf', '=', $cpf)->first();
        // Se não conter no banco então apenas cria uma instância dos objetos para não irem NULL
        if (empty($pessoa)) {
            $pessoa = app('nucleo.pessoa');

        }

        if (!empty($pessoa)) {
            // Busca o Endereço da pessoa
            $endereco = $pessoa->endereco()->first();
        }

        if (empty($endereco)) {
            $endereco = app('models.endereco');
        }

        $pessoa->cpf = $cpf;

        return view('professores.create')
            ->with('pessoa', $pessoa)
            ->with('endereco', $endereco)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastrar Novo ' . $this->titulo);
    }

    /**
     * Store a newly created Professor in storage.
     *
     * @param CreateProfessorRequest $request
     *
     * @return Response
     */
    public function store(CreateProfessorRequest $request)
    {
        $input = $request->all();

        $endereco = $this->enderecoRepository->atualizarCriar($input);
        $input['endereco_id'] = $endereco->id;

        $pessoa = $this->pessoaRepository->atualizarCriar($input);
        $input['pessoa_id'] = $pessoa->id;

        $user = $this->userRepository->atualizarCriar($input);
        $input['user_id'] = $user->id;

        $this->professorRepository->create($input);

        app('flash')->success('Professor salvo com sucesso.');

        return redirect(route('professores.index'));
    }

    /**
     * Display the specified Professor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não Encontrado.');

            return redirect(route('professores.index'));
        }

        return view('professores.show')
            ->with('pessoa', $professor->user->pessoa)
            ->with('endereco', $professor->user->pessoa->endereco)
            ->with('tituloUp', $this->titulo)
            ->with('professor', $professor)
            ->with('subTitulo', 'Visualizar Cadastro de ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Professor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não encontrado.');

            return redirect(route('professores.index'));
        }

        return view('professores.edit')
            ->with('pessoa', $professor->user->pessoa)
            ->with('endereco', $professor->user->pessoa->endereco)
            ->with('tituloUp', $this->titulo)
            ->with('professor', $professor)
            ->with('subTitulo', 'Editar Cadastro de ' . $this->titulo);

    }

    /**
     * Update the specified Professor in storage.
     *
     * @param  int $id
     * @param UpdateProfessorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfessorRequest $request)
    {

        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não encontrado.');

            return redirect(route('professores.index'));
        }

        $input = $request->all();

        $endereco = $this->enderecoRepository->atualizarCriar($input);
        $input['endereco_id'] = $endereco->id;

        $pessoa = $this->pessoaRepository->atualizarCriar($input);
        $input['pessoa_id'] = $pessoa->id;

        $user = $this->userRepository->atualizarCriar($input);
        $input['user_id'] = $user->id;


        $this->professorRepository->update($request->all(), $id);

        app('flash')->success('Professor atualizado com Sucesso.');

        return redirect(route('professores.index'));
    }

    /**
     * Remove the specified Professor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não Encontrado.');

            return redirect(route('professores.index'));
        }

        $this->professorRepository->delete($id);

        app('flash')->success('Professor deletado com Sucesso.');

        return redirect(route('professores.index'));
    }
}
