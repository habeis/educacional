<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateTipoDisciplinaRequest;
use Educacional\Requests\UpdateTipoDisciplinaRequest;
use Educacional\Repositories\TipoDisciplinaRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class TipoDisciplinaController extends AppBaseController
{
    /** @var  TipoDisciplinaRepository */
    private $tipoDisciplinaRepository;

    public function __construct(TipoDisciplinaRepository $tipoDisciplinaRepo)
    {
        $this->tipoDisciplinaRepository = $tipoDisciplinaRepo;
    }

    /**
     * Display a listing of the TipoDisciplina.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tipoDisciplinaRepository->pushCriteria(new RequestCriteria($request));
        $tipoDisciplinas = $this->tipoDisciplinaRepository->all();

        return view('tipoDisciplinas.index')
            ->with('tipoDisciplinas', $tipoDisciplinas)
            ->with('tituloUp', 'Tipo de Disciplina')
            ->with('subTitulo', 'Listar tipo de Disciplina');
    }

    /**
     * Show the form for creating a new TipoDisciplina.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipoDisciplinas.create')
            ->with('tituloUp', 'Tipo de Disciplina')
            ->with('subTitulo', 'Adicionar tipo de Disciplina');
    }

    /**
     * Store a newly created TipoDisciplina in storage.
     *
     * @param CreateTipoDisciplinaRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoDisciplinaRequest $request)
    {
        $input = $request->all();

        $this->tipoDisciplinaRepository->create($input);

        app('flash')->success('TipoDisciplina saved successfully.');

        return redirect(route('tipoDisciplinas.index'));
    }

    /**
     * Display the specified TipoDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoDisciplina = $this->tipoDisciplinaRepository->findWithoutFail($id);

        if (empty($tipoDisciplina)) {
            app('flash')->error('TipoDisciplina not found');

            return redirect(route('tipoDisciplinas.index'));
        }

        return view('tipoDisciplinas.show')
            ->with('tipoDisciplina', $tipoDisciplina)
            ->with('tituloUp', 'Tipo de Disciplina')
            ->with('subTitulo', 'Visualizar tipo de Disciplina');
    }

    /**
     * Show the form for editing the specified TipoDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoDisciplina = $this->tipoDisciplinaRepository->findWithoutFail($id);

        if (empty($tipoDisciplina)) {
            app('flash')->error('TipoDisciplina not found');

            return redirect(route('tipoDisciplinas.index'));
        }

        return view('tipoDisciplinas.edit')
            ->with('tipoDisciplina', $tipoDisciplina)
            ->with('tituloUp', 'Tipo de Disciplina')
            ->with('subTitulo', 'Editar tipo de Disciplina');
    }

    /**
     * Update the specified TipoDisciplina in storage.
     *
     * @param  int              $id
     * @param UpdateTipoDisciplinaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoDisciplinaRequest $request)
    {
        $tipoDisciplina = $this->tipoDisciplinaRepository->findWithoutFail($id);

        if (empty($tipoDisciplina)) {
            app('flash')->error('TipoDisciplina not found');

            return redirect(route('tipoDisciplinas.index'));
        }

        $tipoDisciplina = $this->tipoDisciplinaRepository->update($request->all(), $id);

        app('flash')->success('TipoDisciplina updated successfully.');

        return redirect(route('tipoDisciplinas.index'));
    }

    /**
     * Remove the specified TipoDisciplina from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoDisciplina = $this->tipoDisciplinaRepository->findWithoutFail($id);

        if (empty($tipoDisciplina)) {
            app('flash')->error('TipoDisciplina not found');

            return redirect(route('tipoDisciplinas.index'));
        }

        $this->tipoDisciplinaRepository->delete($id);

        app('flash')->success('TipoDisciplina deleted successfully.');

        return redirect(route('tipoDisciplinas.index'));
    }
}
