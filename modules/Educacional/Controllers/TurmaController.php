<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Repositories\AnoSemestreRepository;
use Educacional\Repositories\MatrizCurricularDisciplinaRepository;
use Educacional\Repositories\PeriodoRepository;
use Educacional\Repositories\TurmaDisciplinaRepository;
use Educacional\Requests\CreateTurmaRequest;
use Educacional\Requests\UpdateTurmaRequest;
use Educacional\Repositories\TurmaRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Educacional\Repositories\CursoRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class TurmaController extends AppBaseController
{
    /** @var  TurmaRepository */
    private $turmaRepository;

    /** @var  $semestreId */
    private $semestreId;

    /** @var  $cursoId - Cursos */
    private $cursoId;

    /** @var  $gradeId - Grade pertence a tabela matriz_curricular */
    private $gradeId;

    public function __construct(TurmaRepository $turmaRepo, Request $request)
    {
        $this->semestreId = $request['semestreLetivoId'];
        $this->cursoId = $request['cursoId'];
        $this->gradeId = $request['gradeId'];

        $this->turmaRepository = $turmaRepo;
    }

    /**
     * Listar Cursos
     *
     * @param Request $request
     * @return Response
     */
    public function cursos(CursoRepository $cursoRepository, AnoSemestreRepository $anoSeRepository)
    {

        $cursos = $cursoRepository->lists('nome', 'id');

        $anoSemestre = $anoSeRepository->lists('ano_semestre', 'id');

        return view('turmas.cursos')
            ->with('anoSemestre', $anoSemestre)
            ->with('cursos', $cursos)
            ->with('tituloUp', 'Turmas')
            ->with('subTitulo', 'Listar de Cursos');
    }

    /**
     * Display a listing of the Turma.
     *
     * @param Request $request
     * @return Response
     */
    public function index(
        Request $request,
        CursoRepository $cursoRepository,
        AnoSemestreRepository $anoSeRepository
    ) {
        if (empty($this->semestreId) or empty($this->cursoId) or empty($this->gradeId)) {
            app('flash')->error('Precisa selecionar o Semestre Letivo -> Curso -> Grade.');

            return redirect(route('semestre-cursos'));
        }

        $this->turmaRepository->pushCriteria(new RequestCriteria($request));

        $curso = $cursoRepository->findWithoutFail($this->cursoId);

        $anoSemestre = $anoSeRepository->findWithoutFail($this->semestreId);

        $turmas = $this->turmaRepository->findWhere(['curso_id' => $this->cursoId, 'ano_semestre_id' => $this->semestreId]);

        return view('turmas.index')
            ->with('turmas', $turmas)
            ->with('anoSemestre', $this->semestreId)
            ->with('cursoId', $this->cursoId)
            ->with('gradeId', $this->gradeId)
            ->with('curso', $curso)
            ->with('semestre', $anoSemestre)
            ->with('tituloUp', 'Turmas')
            ->with('subTitulo', 'Listar Turmas');
    }

    /**
     * Show the form for creating a new Turma.
     *
     * @return Response
     */
    public function create(
        AnoSemestreRepository $anoSeRepository,
        CursoRepository $cursoRepository,
        PeriodoRepository $periodoRepository
    ) {
        /**
         * Consulta no qual traz os
         * periodos daquele curso
         * que contém disciplinas cadastradas.
         */

        $periodos = $periodoRepository->periodoMatrizCurridularId($this->gradeId);

        // Retorna o curso para forma uma turma de um determinado curso
        $curso = $cursoRepository->findWithoutFail($this->cursoId);

        // Retorna o semestre Letivo onde no qual está formando a Turma
        $anoSemestre = $anoSeRepository->findWithoutFail($this->semestreId);

        return view('turmas.create')
            ->with('tituloUp', 'Turmas')
            ->with('curso', $curso)
            ->with('anoSemestre', $anoSemestre)
            ->with('periodos', $periodos)
            ->with('gradeId', $this->gradeId)
            ->with('subTitulo', 'Criar nova Turmas');
    }

    /**
     * Store a newly created Turma in storage.
     *
     * @param CreateTurmaRequest $request
     *
     * @return Response
     */
    public function store(
        CreateTurmaRequest $request,
        MatrizCurricularDisciplinaRepository $matrizCuDiscRepo,
        TurmaDisciplinaRepository $turmaDisRepository
    ) {
        $input = $request->all();

        $turma = $this->turmaRepository->create($input);

        $input['turma_id'] = $turma->id;

        // Busca todas disicplinas de uma determinada Matriz Curricular
        $matrizCuDisTurmas = $matrizCuDiscRepo->disciplinasPeriodoIdMatrizId($input['periodo_id'], $input['gradeId']);

        // Salva todas disicplinas de forma automática de um determinado Período, Grade, na Turma
        $turmaDisRepository->createDisciplinaTurma($matrizCuDisTurmas, $input);

        app('flash')->success('Turma Cadastrada com sucesso.');

        return redirect(
            route(
                'turmas.index',
                [
                    "semestreLetivoId" => $input['ano_semestre_id'],
                    'cursoId' => $input['curso_id'],
                    'gradeId' => $input['gradeId']
                ]
            )
        );
    }

    /**
     * Display the specified Turma.
     *
     * @param  int $id
     *
     * @return Response
     */

    public function show(
        $id,
        AnoSemestreRepository $anoSeRepository,
        CursoRepository $cursoRepository,
        PeriodoRepository $periodoRepository
    ) {
        $turma = $this->turmaRepository->findWithoutFail($id);

        if (empty($turma)) {
            app('flash')->error('Nenhuma Turma encontrada.');

            return redirect(route('turmas.index'));
        }

        $periodos = $periodoRepository->periodoMatrizCurridularId($this->gradeId);

        // Retorna o curso para forma uma turma de um determinado curso
        $curso = $cursoRepository->findWithoutFail($this->cursoId);

        // Retorna o semestre Letivo onde no qual está formando a Turma
        $anoSemestre = $anoSeRepository->findWithoutFail($this->semestreId);

        return view('turmas.show')
            ->with('turma', $turma)
            ->with('curso', $curso)
            ->with('anoSemestre', $anoSemestre)
            ->with('periodos', $periodos)
            ->with('gradeId', $this->gradeId)
            ->with('tituloUp', 'Turmas')
            ->with('subTitulo', 'Visualizar Turma');
    }

    /**
     * Show the form for editing the specified Turma.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit(
        $id,
        AnoSemestreRepository $anoSeRepository,
        CursoRepository $cursoRepository,
        PeriodoRepository $periodoRepository
    ) {
        $turma = $this->turmaRepository->findWithoutFail($id);

        if (empty($turma)) {
            app('flash')->error('Nenhuma Turma encontrada.');

            return redirect(route('turmas.index'));
        }

        $periodos = $periodoRepository->periodoMatrizCurridularId($this->gradeId);

        // Retorna o curso para forma uma turma de um determinado curso
        $curso = $cursoRepository->findWithoutFail($this->cursoId);

        // Retorna o semestre Letivo onde no qual está formando a Turma
        $anoSemestre = $anoSeRepository->findWithoutFail($this->semestreId);

        return view('turmas.edit')
            ->with('turma', $turma)
            ->with('tituloUp', 'Turmas')
            ->with('curso', $curso)
            ->with('anoSemestre', $anoSemestre)
            ->with('periodos', $periodos)
            ->with('gradeId', $this->gradeId)
            ->with('subTitulo', 'Edita Turma');
    }

    /**
     * Update the specified Turma in storage.
     *
     * @param  int $id
     * @param UpdateTurmaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTurmaRequest $request)
    {

        $turma = $this->turmaRepository->findWithoutFail($id);

        if (empty($turma)) {
            app('flash')->error('Nenhuma Turma encontrada.');

            return redirect(route('turmas.index'));
        }

        $this->turmaRepository->update($request->all(), $id);

        app('flash')->success('Turma atualizada com sucesso.');

        return redirect(
            route(
                'turmas.index',
                [
                    "semestreLetivoId" => $request['ano_semestre_id'],
                    'cursoId' => $request['curso_id'],
                    'gradeId' => $request['gradeId']
                ]
            )
        );
    }

    /**
     * Remove the specified Turma from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {

        $turma = $this->turmaRepository->findWithoutFail($id);

        if (empty($turma)) {
            app('flash')->error('Nenhuma Turma encontrada.');

            return redirect(route(
                'turmas.index',
                [
                    'semestreLetivoId' => $request['semestreLetivoId'],
                    'cursoId' => $request['cursoId'],
                    'gradeId' => $request['gradeId']
                ]
            ));
        }

        $this->turmaRepository->delete($id);

        app('flash')->success('Turma deletada com sucesso.');

        return redirect(
            route(
                'turmas.index',
                [
                    'semestreLetivoId' => $request['semestreLetivoId'],
                    'cursoId' => $request['cursoId'],
                    'gradeId' => $request['gradeId']
                ]
            )
        );
    }
}
