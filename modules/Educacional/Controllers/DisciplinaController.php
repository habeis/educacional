<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateDisciplinaRequest;
use Educacional\Requests\UpdateDisciplinaRequest;
use Educacional\Repositories\DisciplinaRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class DisciplinaController extends AppBaseController
{
    /** @var  DisciplinaRepository */
    private $disciplinaRepository;

    public function __construct(DisciplinaRepository $disciplinaRepo)
    {
        $this->disciplinaRepository = $disciplinaRepo;
    }

    /**
     * Display a listing of the Disciplina.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->disciplinaRepository->pushCriteria(new RequestCriteria($request));
        $disciplinas = $this->disciplinaRepository->all();

        return view('disciplinas.index')
            ->with('disciplinas', $disciplinas)
            ->with('tituloUp', 'Disciplina')
            ->with('subTitulo', 'Listar disciplina(s)');
    }

    /**
     * Show the form for creating a new Disciplina.
     *
     * @return Response
     */
    public function create()
    {
        return view('disciplinas.create')
            ->with('tituloUp', 'Criar Disciplinas')
            ->with('subTitulo', 'Adicionar nova disciplina');
    }

    /**
     * Store a newly created Disciplina in storage.
     *
     * @param CreateDisciplinaRequest $request
     *
     * @return Response
     */
    public function store(CreateDisciplinaRequest $request)
    {
        $input = $request->all();

        $this->disciplinaRepository->create($input);

        app('flash')->success('Disciplina saved successfully.');

        return redirect(route('disciplinas.index'));
    }

    /**
     * Display the specified Disciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $disciplina = $this->disciplinaRepository->findWithoutFail($id);

        if (empty($disciplina)) {
            app('flash')->error('Disciplina not found');

            return redirect(route('disciplinas.index'));
        }

        return view('disciplinas.show')->with('disciplina', $disciplina)
            ->with('tituloUp', 'Disciplina')
            ->with('subTitulo', 'Visualizar disciplina');
    }

    /**
     * Show the form for editing the specified Disciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $disciplina = $this->disciplinaRepository->findWithoutFail($id);

        if (empty($disciplina)) {
            app('flash')->error('Disciplina not found');

            return redirect(route('disciplinas.index'));
        }

        return view('disciplinas.edit')->with('disciplina', $disciplina)
            ->with('tituloUp', 'Disciplina')
            ->with('subTitulo', 'Editar disciplina');
    }

    /**
     * Update the specified Disciplina in storage.
     *
     * @param  int              $id
     * @param UpdateDisciplinaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDisciplinaRequest $request)
    {
        $disciplina = $this->disciplinaRepository->findWithoutFail($id);

        if (empty($disciplina)) {
            app('flash')->error('Disciplina not found');

            return redirect(route('disciplinas.index'));
        }

        $disciplina = $this->disciplinaRepository->update($request->all(), $id);

        app('flash')->success('Disciplina updated successfully.');

        return redirect(route('disciplinas.index'));
    }

    /**
     * Remove the specified Disciplina from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $disciplina = $this->disciplinaRepository->findWithoutFail($id);

        if (empty($disciplina)) {
            app('flash')->error('Disciplina not found');

            return redirect(route('disciplinas.index'));
        }

        $this->disciplinaRepository->delete($id);

        app('flash')->success('Disciplina deleted successfully.');

        return redirect(route('disciplinas.index'));
    }
}
