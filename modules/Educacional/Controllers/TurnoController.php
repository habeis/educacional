<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateTurnoRequest;
use Educacional\Requests\UpdateTurnoRequest;
use Educacional\Repositories\TurnoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class TurnoController extends AppBaseController
{
    /** @var  TurnoRepository */
    private $turnoRepository;

    public function __construct(TurnoRepository $turnoRepo)
    {
        $this->turnoRepository = $turnoRepo;
    }

    /**
     * Display a listing of the Turno.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->turnoRepository->pushCriteria(new RequestCriteria($request));
        $turnos = $this->turnoRepository->all();

        return view('turnos.index')
            ->with('turnos', $turnos);
    }

    /**
     * Show the form for creating a new Turno.
     *
     * @return Response
     */
    public function create()
    {
        return view('turnos.create');
    }

    /**
     * Store a newly created Turno in storage.
     *
     * @param CreateTurnoRequest $request
     *
     * @return Response
     */
    public function store(CreateTurnoRequest $request)
    {
        $input = $request->all();

        $this->turnoRepository->create($input);

        app('flash')->success('Turno salvo com sucesso.');

        return redirect(route('turnos.index'));
    }

    /**
     * Display the specified Turno.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $turno = $this->turnoRepository->findWithoutFail($id);
         
        if (empty($turno)) {
            app('flash')->error('Turno não encontrado');

            return redirect(route('turnos.index'));
        }

        return view('turnos.show')->with('turno', $turno);
    }

    /**
     * Show the form for editing the specified Turno.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $turno = $this->turnoRepository->findWithoutFail($id);

        if (empty($turno)) {
            app('flash')->error('Turno não encontrado');

            return redirect(route('turnos.index'));
        }

        return view('turnos.edit')->with('turno', $turno);
    }

    /**
     * Update the specified Turno in storage.
     *
     * @param  int              $id
     * @param UpdateTurnoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTurnoRequest $request)
    {
        $turno = $this->turnoRepository->findWithoutFail($id);

        if (empty($turno)) {
            app('flash')->error('Turno não encontrado');

            return redirect(route('turnos.index'));
        }
        
        $turno = $this->turnoRepository->update($request->all(), $id);

        app('flash')->success('Turno atualizado com Sucesso.');

        return redirect(route('turnos.index'));
    }

    /**
     * Remove the specified Turno from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $turno = $this->turnoRepository->findWithoutFail($id);

        if (empty($turno)) {
            app('flash')->error('Turno not found');

            return redirect(route('turnos.index'));
        }

        $this->turnoRepository->delete($id);

        app('flash')->success('Turno deletado com Sucesso.');

        return redirect(route('turnos.index'));
    }
}
