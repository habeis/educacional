<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Repositories\MatrizCurricularDisciplinaRepository;
use Educacional\Repositories\TurmaRepository;
use Educacional\Requests\CreateTurmaDisciplinaRequest;
use Educacional\Requests\UpdateTurmaDisciplinaRequest;
use Educacional\Repositories\TurmaDisciplinaRepository;
use InfyOm\Generator\Controller\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class TurmaDisciplinaController extends AppBaseController
{
    /** @var  TurmaDisciplinaRepository */
    private $turmaDiscRepository;
    private $turmaId;
    private $semestreLetivoId;
    private $cursoId;
    private $gradeId;

    public function __construct(Request $request, TurmaDisciplinaRepository $turmaDisciplinaRepo)
    {
        $this->turmaId = $request['turmaId'];
        $this->semestreLetivoId = $request['semestreLetivoId'];
        $this->cursoId = $request['cursoId'];
        $this->gradeId = $request['gradeId'];

        $this->turmaDiscRepository = $turmaDisciplinaRepo;
    }

    /**
     * Display a listing of the TurmaDisciplina.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->turmaDiscRepository ->pushCriteria(new RequestCriteria($request));

        // Retorna todas disciplinas de uma determinada Turma
        $turmaDisciplinas = $this->turmaDiscRepository->findWhere(['turma_id' => $this->turmaId]);


        return view('turmaDisciplinas.index')
            ->with('turmaId', $this->turmaId)
            ->with('semestreLetivoId', $this->semestreLetivoId)
            ->with('cursoId', $this->cursoId)
            ->with('gradeId', $this->gradeId)
            ->with('turmaDisciplinas', $turmaDisciplinas)
            ->with('tituloUp', 'Turma Disciplina')
            ->with('subTitulo', 'Lista de disciplinas da Turma');
    }

    /**
     * Show the form for creating a new TurmaDisciplina.
     *
     * @return Response
     */
    public function create(
        TurmaRepository $turmaRepository,
        MatrizCurricularDisciplinaRepository $matCurDisRepository,
        TurmaDisciplinaRepository $turmaDiscRepository
    ) {
        if (empty($this->turmaId)) {
            app('flash')->error('Turma Não encontrada para adicionar Disciplinas');
            return redirect(route('turmaDisciplinas.index'));
        }
        $turma = $turmaRepository->find($this->turmaId);

        // Busca todas as disciplinas daquela Matriz e daquele período selecionando anterior
        $disciplinaPeriodos = $matCurDisRepository->findWhere(['periodo_id' => $turma->periodo->id, 'matriz_curricular_id' => $this->gradeId]);

        $selecDiscPeriodo = $turmaDiscRepository->findWhere(['turma_id' => $turma->id]);

        return view('turmaDisciplinas.create')
            ->with('turma', $turma)
            ->with('disciplinaPeriodos', $disciplinaPeriodos)
            ->with('turmaId', $this->turmaId)
            ->with('semestreLetivoId', $this->semestreLetivoId)
            ->with('cursoId', $this->cursoId)
            ->with('gradeId', $this->gradeId)
            ->with('selecDisciplinaPeriodo', $selecDiscPeriodo)
            ->with('tituloUp', 'Turma Disciplina')
            ->with('subTitulo', 'Adicionar nova disciplinas para Turma');
    }

    /**
     * Store a newly created TurmaDisciplina in storage.
     *
     * @param CreateTurmaDisciplinaRequest $request
     *
     * @return Response
     */
    public function store(CreateTurmaDisciplinaRequest $request, MatrizCurricularDisciplinaRepository $matrizCuDiRepository)
    {
        $input = $request->all();

        //Busca todas as disciplinas da turma Remove todas
        $turmaDisciplina = $this->turmaDiscRepository ->findWhere(['turma_id' => $input['turmaId']]);
        //unset

        foreach ($turmaDisciplina as $key2) {
            // Se a disciplina estiver na lista das novas então não deleta
            if (!in_array($key2->matrriz_curricular_disciplina_id, $input['disciplinaPeriodos'])) {
                $this->turmaDiscRepository ->delete($key2->id);
            }
        }

        // Percorre a lista que contém os códigos das disciplinas par serem cadatradas uma por vez
        foreach ($input['disciplinaPeriodos'] as $value) {
            // Se a disciplina estiver na lista das antigas então não cadastra
            if (!in_array($value, array_pluck($turmaDisciplina, 'matrriz_curricular_disciplina_id'))) {
                $matrizCD = $matrizCuDiRepository->find($value);

                $input['nome_disciplina'] = $matrizCD->disciplina->nome;
                $input['matrriz_curricular_disciplina_id'] = $matrizCD->id;
                $input['turma_id'] = $this->turmaId;

                $this->turmaDiscRepository ->create($input);
            }
        }

        app('flash')->success('TurmaDisciplina saved successfully.');

        return redirect(
            route(
                'turmaDisciplinas.index',
                [
                    'turmaId' => $this->turmaId,
                    'semestreLetivoId' => $this->semestreLetivoId,
                    'cursoId' => $this->cursoId,
                    'gradeId' => $this->gradeId
                ]
            )
        );
    }

    /**
     * Display the specified TurmaDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $turmaDisciplina = $this->turmaDiscRepository ->findWithoutFail($id);

        if (empty($turmaDisciplina)) {
            app('flash')->error('TurmaDisciplina not found');

            return redirect(route('turmaDisciplinas.index'));
        }

        return view('turmaDisciplinas.show')
            ->with('turmaDisciplina', $turmaDisciplina)
            ->with('turmaId', $this->turmaId)
            ->with('semestreLetivoId', $this->semestreLetivoId)
            ->with('cursoId', $this->cursoId)
            ->with('gradeId', $this->gradeId)
            ->with('tituloUp', 'Matriz Curricular')
            ->with('subTitulo', 'Lista de Matriz Curricular');
    }

    /**
     * Show the form for editing the specified TurmaDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id, TurmaRepository $turmaRepository, MatrizCurricularDisciplinaRepository $matCurDisRepository)
    {
        $turmaDisciplina = $this->turmaDiscRepository ->findWithoutFail($id);

        if (empty($this->turmaId)) {
            app('flash')->error('Turma Não encontrada para adicionar Disciplinas');
            return redirect(route('turmaDisciplinas.index'));
        }

        $turma = $turmaRepository->find($this->turmaId);

        if (empty($turmaDisciplina)) {
            app('flash')->error('TurmaDisciplina not found');

            return redirect(route('turmaDisciplinas.index'));
        }
        // Busca todas as disciplinas daquela Matriz e daquele período selecionando anterior
        $disciplinaPeriodos = $matCurDisRepository->findWhere([
            'periodo_id' => $turma->periodo->id,
            'matriz_curricular_id' => $this->gradeId
        ]);

        return view('turmaDisciplinas.edit')
            ->with('turma', $turma)
            ->with('disciplinaPeriodos', $disciplinaPeriodos)
            ->with('turmaDisciplina', $turmaDisciplina)
            ->with('turmaId', $this->turmaId)
            ->with('semestreLetivoId', $this->semestreLetivoId)
            ->with('cursoId', $this->cursoId)
            ->with('gradeId', $this->gradeId)
            ->with('tituloUp', 'Matriz Curricular')
            ->with('subTitulo', 'Lista de Matriz Curricular');
    }

    /**
     * Update the specified TurmaDisciplina in storage.
     *
     * @param  int $id
     * @param UpdateTurmaDisciplinaRequest $request
     *
     * @return Response
     */
    public function update(
        $id,
        UpdateTurmaDisciplinaRequest $request,
        MatrizCurricularDisciplinaRepository $matrizCuDiRepository
    ) {
        $turmaDisciplina = $this->turmaDiscRepository ->findWithoutFail($id);

        $input = $request->all();

        if (empty($this->turmaId)) {
            app('flash')->error('Turma Não encontrada para adicionar Disciplinas');
            return redirect(route('turmaDisciplinas.index'));
        }

        if (empty($turmaDisciplina)) {
            app('flash')->error('TurmaDisciplina not found');

            return redirect(route('turmaDisciplinas.index'));
        }

        $matrizCD = $matrizCuDiRepository->find($input['matrriz_curricular_disciplina_id']);

        $input['nome_disciplina'] = $matrizCD->disciplina->nome;

        $input['turma_id'] = $this->turmaId;

        $this->turmaDiscRepository ->update($input, $id);

        app('flash')->success('TurmaDisciplina updated successfully.');

        return redirect(
            route(
                'turmaDisciplinas.index',
                [
                    'turmaId' => $this->turmaId,
                    'semestreLetivoId' => $this->semestreLetivoId,
                    'cursoId' => $this->cursoId,
                    'gradeId' => $this->gradeId
                ]
            )
        );
    }

    /**
     * Remove the specified TurmaDisciplina from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        $turmaDisciplina = $this->turmaDiscRepository ->findWithoutFail($id);

        if (empty($turmaDisciplina)) {
            app('flash')->error('TurmaDisciplina not found');

            return redirect(route('turmaDisciplinas.index'));
        }

        $this->turmaDiscRepository ->delete($id);

        app('flash')->success('TurmaDisciplina deleted successfully.');

        return redirect(
            route(
                'turmaDisciplinas.index',
                [
                    'turmaId' => $this->turmaId,
                    'semestreLetivoId' => $this->semestreLetivoId,
                    'cursoId' => $this->cursoId,
                    'gradeId' => $this->gradeId
                ]
            )
        );
    }
}
