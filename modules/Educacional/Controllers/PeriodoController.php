<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreatePeriodoRequest;
use Educacional\Requests\UpdatePeriodoRequest;
use Educacional\Repositories\PeriodoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class PeriodoController extends AppBaseController
{
    /** @var  PeriodoRepository */
    private $periodoRepository;

    public function __construct(PeriodoRepository $periodoRepo)
    {
        $this->periodoRepository = $periodoRepo;
    }

    /**
     * Display a listing of the Periodo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->periodoRepository->pushCriteria(new RequestCriteria($request));
        $periodos = $this->periodoRepository->all();

        return view('periodos.index')
            ->with('periodos', $periodos)
            ->with('tituloUp', 'Periodo')
            ->with('subTitulo', 'Lista de Períodos');
    }

    /**
     * Show the form for creating a new Periodo.
     *
     * @return Response
     */
    public function create()
    {
        return view('periodos.create')
            ->with('tituloUp', 'Periodo')
            ->with('subTitulo', 'Cadastrar novo Período');
    }

    /**
     * Store a newly created Periodo in storage.
     *
     * @param CreatePeriodoRequest $request
     *
     * @return Response
     */
    public function store(CreatePeriodoRequest $request)
    {
        $input = $request->all();

        $this->periodoRepository->create($input);

        app('flash')->success('Periodo saved successfully.');

        return redirect(route('periodos.index'));
    }

    /**
     * Display the specified Periodo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            app('flash')->error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        return view('periodos.show')
            ->with('periodo', $periodo)
            ->with('tituloUp', 'Período')
            ->with('subTitulo', 'Visualizar Período');
    }

    /**
     * Show the form for editing the specified Periodo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            app('flash')->error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        return view('periodos.edit')->with('periodo', $periodo);
    }

    /**
     * Update the specified Periodo in storage.
     *
     * @param  int              $id
     * @param UpdatePeriodoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePeriodoRequest $request)
    {
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            app('flash')->error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        $periodo = $this->periodoRepository->update($request->all(), $id);

        app('flash')->success('Periodo updated successfully.');

        return redirect(route('periodos.index'));
    }

    /**
     * Remove the specified Periodo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            app('flash')->error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        $this->periodoRepository->delete($id);

        app('flash')->success('Periodo deleted successfully.');

        return redirect(route('periodos.index'));
    }
}
