<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Requests\CreateColigadaRequest;
use Educacional\Requests\UpdateColigadaRequest;
use Educacional\Repositories\ColigadaRepository;
use Educacional\Repositories\EnderecoRepository;
use Educacional\Repositories\TipoCursoRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Routing\Route;
use Educacional\Models\TipoCurso;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class ColigadaController extends AppBaseController
{
    /** @var  ColigadaRepository */
    private $coligadaRepository;

    private $coligada;

    private $tipoCursos;

    private $titulo;

    public function __construct(ColigadaRepository $coligadaRepo)
    {
        $this->coligadaRepository = $coligadaRepo;

        $this->titulo = 'Coligada';

        // Consulta sera realiza assim que chamar os metodos especificados
        $this->beforeFilter('@findWithoutFail', ['only' => ['update', 'destroy', 'show']]);
        //$this->beforeFilter('@findAllTipoCurso', ['only' => ['edit', 'show']]);
    }

    // Carrega automático assim que executa o metodo construtor
    public function findWithoutFail(Route $route)
    {
        $this->coligada = $this->coligadaRepository->findWithoutFail($route->getParameter('coligadas'));
    }

    private function findAllTipoCurso()
    {
        $this->tipoCursos = TipoCurso::lists('descricao', 'id');
    }

    /**
     * Display a listing of the Coligada.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->coligadaRepository->pushCriteria(new RequestCriteria($request));
        $coligadas = $this->coligadaRepository->all();

        return view('coligadas.index')
            ->with('coligadas', $coligadas)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Lista de ' . $this->titulo);
    }

    /**
     * Show the form for creating a new Coligada.
     *
     * @return Response
     */
    public function create()
    {
        $this->findAllTipoCurso();

        return view('coligadas.create')
            ->with('endereco', app('models.endereco'))
            ->with('tipocurso', $this->tipoCursos)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Cadastro de ' . $this->titulo);
    }

    /**
     * Store a newly created Coligada in storage.
     *
     * @param CreateColigadaRequest $request
     *
     * @return Response
     */
    public function store(
        CreateColigadaRequest $request,
        EnderecoRepository $enderecoRepository,
        TipoCursoRepository $tipoCursoRepository
    )
    {

        $input = $request->all();

        $endereco = $this->enderecoCriarAtualizar(null, $request, $enderecoRepository);

        $input['endereco_id'] = $endereco->id;

        $coligada = $this->coligadaRepository->create($input);

        $this->coligadaRepository->salvarTipoCursoColigada($request->all(), $coligada, $tipoCursoRepository);

        app('flash')->success('Coligada salva com Sucesso.');

        return redirect(route('coligadas.index'));
    }

    /**
     * Display the specified Coligada.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show()
    {
        $this->findAllTipoCurso();

        if (empty($this->coligada)) {
            app('flash')->error('Coligada não encontrada');

            return redirect(route('coligadas.index'));
        }

        return view('coligadas.show')
            ->with('coligada', $this->coligada)
            ->with('endereco', $this->coligada->endereco)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Visualizar ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Coligada.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit(Route $route)
    {
        $this->findWithoutFail($route);

        $this->findAllTipoCurso();

        if (empty($this->coligada)) {
            app('flash')->error('Coligada não encontrada');

            return redirect(route('coligadas.index'));
        }

        $endereco = $this->coligada->endereco;

        if (empty($endereco)) {
            $endereco = app('models.endereco');
        }

        return view('coligadas.edit')
            ->with('coligada', $this->coligada)
            ->with('endereco', $endereco)
            ->with('tipocurso', $this->tipoCursos)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Editar ' . $this->titulo);
    }

    /**
     * Update the specified Coligada in storage.
     *
     * @param  int $id
     * @param UpdateColigadaRequest $request
     *
     * @return Response
     */
    public function update(
        $id,
        UpdateColigadaRequest $request,
        EnderecoRepository $enderecoRepository,
        TipoCursoRepository $tipoCursoRepository
    )
    {

        if (empty($this->coligada)) {
            app('flash')->error('Coligada não encontrada');

            return redirect(route('coligadas.index'));
        }

        $endereco = $this->enderecoCriarAtualizar($this->coligada->endereco, $request, $enderecoRepository);

        $request['endereco_id'] = $endereco->id;

        $coligada = $this->coligadaRepository->update($request->all(), $id);

        $this->coligadaRepository->salvarTipoCursoColigada($request->all(), $coligada, $tipoCursoRepository);

        app('flash')->success('Coligada atualizada com sucesso.');

        return redirect(route('coligadas.index'));
    }

    /**
     * @param Request $request
     * Adiciona o tipo de curso para a Coligada
     * @param TipoCursoRepository $tipoCursoRepository
     * @return mixed|void
     */
    public function adicionarTipoCurso(Request $request, TipoCursoRepository $tipoCursoRepository)
    {

        $tipoCurso = $tipoCursoRepository->findWithoutFail($request['tipoCursoId']);

        /* Salva já o tipo de curso no curso
         * $this->coligadaRepository
            ->findWithoutFail($request['idColigada'])
            ->tipoCursos()
            ->save($tipoCurso);
        */
        return $tipoCurso;
    }

    /**
     * Se não tiver o endereço cria um Novo.
     * senão atualiza
     */
    private function enderecoCriarAtualizar($endereco, $request, EnderecoRepository $enderecoRepository)
    {
        if (empty($endereco)) {
            return $enderecoRepository->create($request->all());
        }

        return $enderecoRepository->update($request->all(), $endereco->id);
    }

    /**
     * Remove the specified Coligada from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        if (empty($this->coligada)) {
            app('flash')->error("Coligada não encontrada.");
            return redirect(route('coligadas.index'));
        }

        $this->coligadaRepository->delete($id);

        app('flash')->success('Coligada deletada com Sucesso.');

        return redirect(route('coligadas.index'));
    }
}
