<?php

namespace Educacional\Controllers;


use DB;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Requests;
use EduAdmin\Repositories\UserRepository;
use Educacional\Models\AnoSemestre;
use Educacional\Models\Professor;
use Educacional\Models\TipoCurso;
use Educacional\Models\Turma;
use Educacional\Models\TurmaDisciplina;
use Educacional\Repositories\AnoSemestreRepository;
use Educacional\Repositories\ColigadaRepository;
use Educacional\Repositories\CursoRepository;
use Educacional\Repositories\EnderecoRepository;
use Educacional\Repositories\ProfessorRepository;
use Educacional\Repositories\TipoCursoRepository;
use Educacional\Repositories\TurmaDisciplinaRepository;
use Educacional\Repositories\TurmaRepository;
use Educacional\Requests\CreateProfessorRequest;
use Educacional\Requests\UpdateProfessorRequest;
use Flash;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use Nucleo\Models\Pessoa;
use Nucleo\Repositories\PessoaRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProfessorDisciplinaController extends AppBaseController
{
    /** @var  ProfessorRepository */
    private $professorRepository;

    /** @var  Titulo View */
    private $titulo;

    /** @var  UserRepository */
    private $userRepository;

    /** @var  PessoaRepository */
    private $pessoaRepository;

    /** @var  EnderecoRepository */
    private $enderecoRepository;

    /** @var  parametros */
    private $parametros;

    public function __construct(Request $request)
    {
        $tipoCursoId = $request['tipoCursoId'];
        $semestreLetivoId = $request['semestreLetivoId'];
        $cursoId = $request['cursoId'];

        $this->parametros = compact('tipoCursoId', 'semestreLetivoId', 'cursoId');

        $this->titulo = "Disciplina para Professor";
    }

    /**
     * Display a listing of the Professor.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, TurmaDisciplinaRepository $turmaDisciplinaRepository)
    {

        $turmaDisciplinas = TurmaDisciplina::from('educacional.turma_disciplina')
            ->join('educacional.turma as tu', function ($join) {
                $join->where('tu.ano_semestre_id', '=', $this->parametros['semestreLetivoId'])
                    ->where('tu.curso_id', '=', $this->parametros['cursoId'])
                    ->on('educacional.turma_disciplina.turma_id', '=', "tu.id");
            })
            ->distinct()
            ->join('educacional.professor_disciplina as prd', 'prd.turma_disciplina_id', '=', 'educacional.turma_disciplina.id')
            ->get(['educacional.turma_disciplina.*']);

        //->join('educacional.turma as tu', 'td.turma_id', '=', 'tu.id')
        //$turmaDisciplinaRepository->pushCriteria(new RequestCriteria($request));

        //Busca a Coligada que está cadastrada
        //$coligada = $turmaDisciplinaRepository->findWhere('');

        return view('professorDisciplina.index')
            ->with('tituloUp', $this->titulo)
            ->with('turmaDisciplinas', $turmaDisciplinas)
            ->with('parametros', $this->parametros)
            ->with('subTitulo', 'Lista de Professores com Disciplinas');
    }

    public function curso(ColigadaRepository $coligadaRepository)
    {
        //Busca a Coligada que está cadastrada
        $coligada = $coligadaRepository->first();


        return view('professorDisciplina.curso')
            ->with('coligada', $coligada)
            ->with('tituloUp', $this->titulo)
            ->with('subTitulo', 'Matrícula Calouro / Veterano');

    }

    /**
     * Show the form for creating a new Professor.
     *
     * @return Response
     */
    public function create(ProfessorRepository $professoreRepository, TurmaRepository $turmaRepository)
    {
        // Busca as turmas que contém Nesse Semestre Letivo Desse Curso
        $turmas = $turmaRepository->findWhere(['curso_id' => $this->parametros['cursoId'],
            'ano_semestre_id' => $this->parametros['semestreLetivoId']]);
        /*
                Turma::from("educacional.turma")
                    ->join('educacional.ano_semestre', function ($join) {
                        $join->on('educacional.turma.ano_semestre_id', '=', 'educacional.ano_semestre.id')
                            ->where('educacional.ano_semestre.atual', '=', true)
                            ->where('educacional.ano_semestre.id', '=', $this->parametros['semestreLetivoId']);
                    })
                    ->join('educacional.turma_disciplina', 'educacional.turma_disciplina.turma_id', '=', 'educacional.turma.id');
        dd($turma);

                    TurmaDisciplina::from('educacional.turma_disciplina')
                        ->distinct()
                        ->join('educacional.turma', 'educacional.turma_disciplina.turma_id', '=', 'educacional.turma.id')
                        ->join('educacional.ano_semestre', function ($join) {
                            $join->on('educacional.turma.ano_semestre_id', '=', 'educacional.ano_semestre.id')
                                ->where('educacional.ano_semestre.atual', '=', true)
                                ->where('educacional.ano_semestre.id', '=', $this->parametros['semestreLetivoId']);

                        })
                        ->join('educacional.matriz_curricular_disciplina', 'educacional.turma_disciplina.matrriz_curricular_disciplina_id', '=', 'educacional.matriz_curricular_disciplina.id')
                        ->join('educacional.matriz_curricular', function ($join) {
                            $join->on('educacional.matriz_curricular_disciplina.matriz_curricular_id', '=', 'educacional.matriz_curricular.id')
                                ->where('educacional.matriz_curricular.curso_id', '=', $this->parametros['cursoId']);
                        })
                        ->orderBy('educacional.turma.periodo_id')
                        ->get(
                            [
                                'educacional.turma_disciplina.id as turmaDisciplinaId',
                                'educacional.turma_disciplina.*',
                                'educacional.turma.*',
                                'educacional.matriz_curricular_disciplina.id as matrizDisciplinaId'
                            ]);
        */
        $professores = $professoreRepository->all();

        return view('professorDisciplina.create')
            ->with('tituloUp', $this->titulo)
            ->with('turmas', $turmas)
            ->with('professores', $professores)
            ->with('parametros', $this->parametros)
            ->with('subTitulo', 'Lista de Turmas e Disciplinas');
    }

    /**
     * Store a newly created Professor in storage.
     *
     * @param CreateProfessorRequest $request
     *
     * @return Response
     */
    public function store(Request $request )
    {
        $input = $request->all();

        //$turmasDisciplinas = $turmaRepository->findWhereIn('id',$input['turmasDisciplinas']);
        $turmasDisciplinas = TurmaDisciplina::findMany($input['turmasDisciplinas']);

        //$professor = $professorRepository->find($input['professor']);
        $professor = Professor::find($input['professor']);

        $professorDisciplina = $professor->TurmaDisciplina()->saveMany($turmasDisciplinas);

        if (empty($professorDisciplina)) {
            app('flash')->error('Erro ao Salva Disciplina para Professor.');

            return redirect(route('professor-disciplina-curso'));
        }

        app('flash')->success('Disciplina Atribuiada com Sucesso para Professor.');

        return redirect(route('professor-disciplina.index',$this->parametros));

    }

    /**
     * Display the specified Professor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não Encontrado.');

            return redirect(route('professores.index'));
        }

        return view('professores.show')
            ->with('pessoa', $professor->user->pessoa)
            ->with('endereco', $professor->user->pessoa->endereco)
            ->with('tituloUp', $this->titulo)
            ->with('professor', $professor)
            ->with('subTitulo', 'Visualizar Cadastro de ' . $this->titulo);
    }

    /**
     * Show the form for editing the specified Professor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não encontrado.');

            return redirect(route('professores.index'));
        }

        return view('professores.edit')
            ->with('pessoa', $professor->user->pessoa)
            ->with('endereco', $professor->user->pessoa->endereco)
            ->with('tituloUp', $this->titulo)
            ->with('professor', $professor)
            ->with('subTitulo', 'Editar Cadastro de ' . $this->titulo);

    }

    /**
     * Update the specified Professor in storage.
     *
     * @param  int $id
     * @param UpdateProfessorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfessorRequest $request)
    {

        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não encontrado.');

            return redirect(route('professores.index'));
        }

        $input = $request->all();

        $endereco = $this->enderecoRepository->atualizarCriar($input);
        $input['endereco_id'] = $endereco->id;

        $pessoa = $this->pessoaRepository->atualizarCriar($input);
        $input['pessoa_id'] = $pessoa->id;

        $user = $this->userRepository->atualizarCriar($input);
        $input['user_id'] = $user->id;


        $this->professorRepository->update($request->all(), $id);

        app('flash')->success('Professor atualizado com Sucesso.');

        return redirect(route('professores.index'));
    }

    /**
     * Remove the specified Professor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $professor = $this->professorRepository->findWithoutFail($id);

        if (empty($professor)) {
            app('flash')->error('Professor não Encontrado.');

            return redirect(route('professores.index'));
        }

        $this->professorRepository->delete($id);

        app('flash')->success('Professor deletado com Sucesso.');

        return redirect(route('professores.index'));
    }
}
