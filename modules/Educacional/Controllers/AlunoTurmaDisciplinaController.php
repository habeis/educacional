<?php

namespace Educacional\Controllers;

use App\Http\Requests;
use Educacional\Http\Requests\CreateAlunoTurmaDisciplinaRequest;
use Educacional\Http\Requests\UpdateAlunoTurmaDisciplinaRequest;
use Educacional\Repositories\AlunoTurmaDisciplinaRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AlunoTurmaDisciplinaController extends AppBaseController
{
    /** @var  AlunoTurmaDisciplinaRepository */
    private $alunoTurmaDisciplinaRepository;

    public function __construct(AlunoTurmaDisciplinaRepository $alunoTurmaDisciplinaRepo)
    {
        $this->alunoTurmaDisciplinaRepository = $alunoTurmaDisciplinaRepo;
    }

    /**
     * Display a listing of the AlunoTurmaDisciplina.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->alunoTurmaDisciplinaRepository->pushCriteria(new RequestCriteria($request));
        $alunoTurmaDisciplinas = $this->alunoTurmaDisciplinaRepository->paginate(10);

        return view('alunoTurmaDisciplinas.index')
            ->with('alunoTurmaDisciplinas', $alunoTurmaDisciplinas);
    }

    /**
     * Show the form for creating a new AlunoTurmaDisciplina.
     *
     * @return Response
     */
    public function create()
    {
        return view('alunoTurmaDisciplinas.create');
    }

    /**
     * Store a newly created AlunoTurmaDisciplina in storage.
     *
     * @param CreateAlunoTurmaDisciplinaRequest $request
     *
     * @return Response
     */
    public function store(CreateAlunoTurmaDisciplinaRequest $request)
    {
        $input = $request->all();

        $alunoTurmaDisciplina = $this->alunoTurmaDisciplinaRepository->create($input);

        Flash::success('AlunoTurmaDisciplina saved successfully.');

        return redirect(route('alunoTurmaDisciplinas.index'));
    }

    /**
     * Display the specified AlunoTurmaDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $alunoTurmaDisciplina = $this->alunoTurmaDisciplinaRepository->findWithoutFail($id);

        if (empty($alunoTurmaDisciplina)) {
            Flash::error('AlunoTurmaDisciplina not found');

            return redirect(route('alunoTurmaDisciplinas.index'));
        }

        return view('alunoTurmaDisciplinas.show')->with('alunoTurmaDisciplina', $alunoTurmaDisciplina);
    }

    /**
     * Show the form for editing the specified AlunoTurmaDisciplina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $alunoTurmaDisciplina = $this->alunoTurmaDisciplinaRepository->findWithoutFail($id);

        if (empty($alunoTurmaDisciplina)) {
            Flash::error('AlunoTurmaDisciplina not found');

            return redirect(route('alunoTurmaDisciplinas.index'));
        }

        return view('alunoTurmaDisciplinas.edit')->with('alunoTurmaDisciplina', $alunoTurmaDisciplina);
    }

    /**
     * Update the specified AlunoTurmaDisciplina in storage.
     *
     * @param  int              $id
     * @param UpdateAlunoTurmaDisciplinaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlunoTurmaDisciplinaRequest $request)
    {
        $alunoTurmaDisciplina = $this->alunoTurmaDisciplinaRepository->findWithoutFail($id);

        if (empty($alunoTurmaDisciplina)) {
            Flash::error('AlunoTurmaDisciplina not found');

            return redirect(route('alunoTurmaDisciplinas.index'));
        }

        $alunoTurmaDisciplina = $this->alunoTurmaDisciplinaRepository->update($request->all(), $id);

        Flash::success('AlunoTurmaDisciplina updated successfully.');

        return redirect(route('alunoTurmaDisciplinas.index'));
    }

    /**
     * Remove the specified AlunoTurmaDisciplina from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $alunoTurmaDisciplina = $this->alunoTurmaDisciplinaRepository->findWithoutFail($id);

        if (empty($alunoTurmaDisciplina)) {
            Flash::error('AlunoTurmaDisciplina not found');

            return redirect(route('alunoTurmaDisciplinas.index'));
        }

        $this->alunoTurmaDisciplinaRepository->delete($id);

        Flash::success('AlunoTurmaDisciplina deleted successfully.');

        return redirect(route('alunoTurmaDisciplinas.index'));
    }
}
