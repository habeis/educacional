<table class="table table-responsive">
    <thead>
        <th>Descricao</th>
        <th>Obs</th>
        <th>Ativo</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($modalidades as $modalidade)
        <tr>
            <td>{!! $modalidade->descricao !!}</td>
            <td>{!! $modalidade->obs !!}</td>
            <td>{!! $modalidade->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['modalidades.destroy', $modalidade->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('modalidades.show', [$modalidade->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('modalidades.edit', [$modalidade->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>