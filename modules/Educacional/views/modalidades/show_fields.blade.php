<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    {!! Form::text('id', $modalidade->id, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::text('descricao', $modalidade->descricao, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Obs Field -->
<div class="form-group">
    {!! Form::label('obs', 'Obs:') !!}
    {!! Form::text('obs', $modalidade->obs, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Ativo Field -->
<div class="form-group">
    {!! Form::label('ativo', 'Ativo:') !!}
    {!! Form::text('ativo', $modalidade->ativo, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('modalidades.index') !!}" class="btn btn-primary">Cancel</a>
</div>

