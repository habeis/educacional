@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($curso, ['route' => ['cursos.update', $curso->id], 'method' => 'patch']) !!}

        @include('cursos.fields')

        {!! Form::close() !!}
    </div>
@endsection