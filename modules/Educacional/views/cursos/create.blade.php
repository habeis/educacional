@extends('layouts.page')

@section('content-center')

    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'cursos.store']) !!}

        @include('cursos.fields')

        {!! Form::close() !!}
    </div>
@endsection