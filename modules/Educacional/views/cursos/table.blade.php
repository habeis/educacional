<table class="table table-responsive">
    <thead>
        <th>Nome</th>
        <th>Quantidade Periodo</th>
        <th>Horario Turno</th>
        <th>Ativo</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($cursos as $curso)
        <tr>
            <td>{!! $curso->nome !!}</td>
            <td>{!! $curso->quantidade_periodo !!}</td>
            <td>{!! $curso->horario_turno !!}</td>
            <td>{!! $curso->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['cursos.destroy', $curso->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a title="Visualizar Curso" href="{!! route('cursos.show', [$curso->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a title="Editar Curso" href="{!! route('cursos.edit', [$curso->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <a title="Listar Matríz Curricular" href="{!! route('matrizCurriculars.index',['cursoId' => $curso->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-list"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ["title" =>"Deletar Curso",'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>