@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('cursos.create') !!}">{{ trans("forms.button_create") }}</a>
    </a>
    <div class="clearfix"></div>

    @if($cursos->isEmpty())
        <div class="well text-center">Nenhum Curso encontrado.</div>
    @else
        @include('cursos.table')
    @endif
@endsection