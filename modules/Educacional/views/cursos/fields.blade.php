<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<!-- Quantidade Periodo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantidade_periodo', 'Quantidade Periodo:') !!}
    {!! Form::number('quantidade_periodo', null, ['class' => 'form-control']) !!}
</div>

<!-- Turno Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('turno_id', 'Turno:') !!}
    {!! Form::select('turno_id',$turnos, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}
</div>

<!-- Horario Turno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('horario_turno', 'Horario Turno:') !!}
    {!! Form::text('horario_turno', null, ['class' => 'form-control']) !!}
</div>

<!-- Coordenacao Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coordenacao_id', 'Coordenacao:') !!}
    {!! Form::select('coordenacao_id',$coordenacaos, null,['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}
</div>

<!-- Habilitacao Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('habilitacao_id', 'Habilitacao:') !!}
    {!! Form::select('habilitacao_id',$habilistacaos,null,['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}
</div>

<!-- Modalidade Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modalidade_id', 'Modalidade:') !!}
    {!! Form::select('modalidade_id', $modalidades, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}
</div>

<!-- Modalidade Tipo de Cursos Graduação-Ténico ou pós-Graduação Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coligada_tipo_curso_id', 'Tipo de Cursos:') !!}
    {!! Form::select('coligada_tipo_curso_id', array_pluck($coligadaTipoCurso,'descricao','id'), null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}
</div>

<!-- Inep Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inep_id', 'Código no Inep:') !!}
    {!! Form::number('inep_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Modo Curso Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modo_curso_id', 'Modo Curso:') !!}
    {!! Form::select('modo_curso_id',$modoCursos ,null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}</div>

<!-- Portaria Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('portaria_id', 'Portaria:') !!}
    {!! Form::select('portaria_id',$portarias, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}</div>
</div>

<!-- Obs Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('obs', 'Observação:') !!}
    {!! Form::textarea('obs', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('cursos.index') !!}" class="btn btn-primary">Cancelar</a>

</div>
