<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    <p>{!! Form::text('id', $curso->id, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{!! Form::text('nome', $curso->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Quantidade Periodo Field -->
<div class="form-group">
    {!! Form::label('quantidade_periodo', 'Quantidade Periodo:') !!}
    <p>{!! Form::text('quantidade_periodo', $curso->quantidade_periodo, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Turno Id Field -->
<div class="form-group">
    {!! Form::label('turno_id', 'Turno:') !!}
    <p>{!! Form::text('turno_id', $curso->turno->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Horario Turno Field -->
<div class="form-group">
    {!! Form::label('horario_turno', 'Horario Turno:') !!}
    <p>{!! Form::text('horario_turno', $curso->horario_turno, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Coordenacao Id Field -->
<div class="form-group">
    {!! Form::label('coordenacao_id', 'Coordenacao:') !!}
    <p>{!! Form::text('horario_turno',$curso->coordenacao->nome , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Habilitacao Id Field -->
<div class="form-group">
    {!! Form::label('habilitacao_id', 'Habilitacao:') !!}
    <p>{!! Form::text('habilitacao_id', $curso->habilitacao->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Modalidade Id Field -->
<div class="form-group">
    {!! Form::label('modalidade_id', 'Modalidade:') !!}
    <p>{!! Form::text('modalidade_id', $curso->modalidade->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div> </div>

<!-- Obs Field -->
<div class="form-group">
    {!! Form::label('obs', 'Observação:') !!}
    <p>{!! Form::text('obs', $curso->obs, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Modo Curso Id Field -->
<div class="form-group">
    {!! Form::label('modo_curso_id', 'Modo Curso:') !!}
    <p>{!! Form::text('modo_curso_id', $curso->modoCurso->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Portaria Id Field -->
<div class="form-group">
    {!! Form::label('portaria_id', 'Portaria:') !!}
    <p>{!! Form::text('portaria_id', $curso->portaria->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Ativo Field -->
<div class="form-group">
    {!! Form::label('ativo', 'Ativo:') !!}
    <p>{!! Form::checkbox('ativo',  $curso->ativo, ['class' => 'flat','disabled'=>'disabled']) !!}</p>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('cursos.index') !!}" class="btn btn-primary">Cancelar</a>
</div>
