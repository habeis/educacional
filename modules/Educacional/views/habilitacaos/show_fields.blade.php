<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    {!! Form::text('id', $habilitacao->id, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::text('descricao', $habilitacao->descricao, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('logradouro', $habilitacao->nome, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Decreto Lei Resolucao Field -->
<div class="form-group">
    {!! Form::label('decreto_lei_resolucao', 'Decreto Lei Resolucao:') !!}
    {!! Form::text('decreto_lei_resolucao', $habilitacao->decreto_lei_resolucao, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Ativo Field -->
<div class="form-group">
    {!! Form::label('ativo', 'Ativo:') !!}
    {!! Form::text('ativo', $habilitacao->ativo, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Submit Field -->
<div class="form-group">
    <a href="{!! route('habilitacaos.index') !!}" class="btn btn-primary">Cancel</a>
</div>