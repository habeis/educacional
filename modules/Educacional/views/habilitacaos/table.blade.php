<table class="table table-responsive">
    <thead>
        <th>Descricao</th>
        <th>Nome</th>
        <th>Decreto / Lei / Resolucao</th>
        <th>Ativo</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($habilitacaos as $habilitacao)
        <tr>
            <td>{!! $habilitacao->descricao !!}</td>
            <td>{!! $habilitacao->nome !!}</td>
            <td>{!! $habilitacao->decreto_lei_resolucao !!}</td>
            <td>{!! $habilitacao->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['habilitacaos.destroy', $habilitacao->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('habilitacaos.show', [$habilitacao->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('habilitacaos.edit', [$habilitacao->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Realmente deseja excluir?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>