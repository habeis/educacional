<table class="table table-responsive" id="alunoTurmaDisciplinas-table">
    <thead>
        <th>Aluno Turma Id</th>
        <th>Turma Disciplina Id</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Deleted At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($alunoTurmaDisciplinas as $alunoTurmaDisciplina)
        <tr>
            <td>{!! $alunoTurmaDisciplina->aluno_turma_id !!}</td>
            <td>{!! $alunoTurmaDisciplina->turma_disciplina_id !!}</td>
            <td>{!! $alunoTurmaDisciplina->created_at !!}</td>
            <td>{!! $alunoTurmaDisciplina->updated_at !!}</td>
            <td>{!! $alunoTurmaDisciplina->deleted_at !!}</td>
            <td>
                {!! Form::open(['route' => ['alunoTurmaDisciplinas.destroy', $alunoTurmaDisciplina->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('alunoTurmaDisciplinas.show', [$alunoTurmaDisciplina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('alunoTurmaDisciplinas.edit', [$alunoTurmaDisciplina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>