<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Aluno Turma Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aluno_turma_id', 'Aluno Turma Id:') !!}
    {!! Form::number('aluno_turma_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Turma Disciplina Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('turma_disciplina_id', 'Turma Disciplina Id:') !!}
    {!! Form::number('turma_disciplina_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::date('updated_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    {!! Form::date('deleted_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('alunoTurmaDisciplinas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
