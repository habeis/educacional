@extends('layouts.app')

@section('content')
    @include('alunoTurmaDisciplinas.show_fields')

    <div class="form-group">
           <a href="{!! route('alunoTurmaDisciplinas.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
