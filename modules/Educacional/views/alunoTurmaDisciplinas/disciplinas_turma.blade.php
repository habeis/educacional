<div class="form-group form-group col-sm-12 table-responsive">

    <div class="panel panel-info">
        <div class="panel-heading"><h3>Disciplinas da Turma</h3></div>
        <table class="table table-striped table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>Nome da Disciplina</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($disciplinaPeriodo as $disciplinaPeriodo)
                <tr>
                    <td>
                        {!! Form::checkbox('disciplinaPeriodos[]', $disciplinaPeriodo->turmaDisciplinaId,in_array($disciplinaPeriodo->turmaDisciplinaId,array_pluck($disciplinaPeriodo,'matrriz_curricular_disciplina_id'))) !!}
                    </td>
                    <td>
                        {!! Form::label('disciplinaPeriodos[]', $disciplinaPeriodo->nome_disciplina) !!}<br>
                    </td>
                    <td>
                        {!! Form::label('disciplinaPeriodos[]', $disciplinaPeriodo->turma->periodo->descricao) !!} -
                        {!! Form::label('disciplinaPeriodos[]', $disciplinaPeriodo->turma->nome) !!}
                        <br>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
