<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $alunoTurmaDisciplina->id !!}</p>
</div>

<!-- Aluno Turma Id Field -->
<div class="form-group">
    {!! Form::label('aluno_turma_id', 'Aluno Turma Id:') !!}
    <p>{!! $alunoTurmaDisciplina->aluno_turma_id !!}</p>
</div>

<!-- Turma Disciplina Id Field -->
<div class="form-group">
    {!! Form::label('turma_disciplina_id', 'Turma Disciplina Id:') !!}
    <p>{!! $alunoTurmaDisciplina->turma_disciplina_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $alunoTurmaDisciplina->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $alunoTurmaDisciplina->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $alunoTurmaDisciplina->deleted_at !!}</p>
</div>

