@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit AlunoTurmaDisciplina</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($alunoTurmaDisciplina, ['route' => ['alunoTurmaDisciplinas.update', $alunoTurmaDisciplina->id], 'method' => 'patch']) !!}

            @include('alunoTurmaDisciplinas.fields')

            {!! Form::close() !!}
        </div>
@endsection