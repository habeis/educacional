<div class="table-responsive col-lg-12">

    <div class="form-group col-sm-12">
        {!! Form::label('matricula', 'Matrícula:') !!}
        {!! Form::text('matricula',null, ['placeholder' => 'Digite a Matrícula do Aluno', 'required' => 'required','class' => 'form-control col-sm-12']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Avançar', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('aluno-historico.index') !!}" class="btn btn-default">Cancelar</a>
    </div>

</div>