<table class="table table-responsive" id="alunos-table">
    <thead>
    <th>#</th>
    <th title="Nome da Dsiciplina"> Nome Disciplina</th>
    <th title="Carga Horária"> CH</th>
    <th title="Período da Turma"> Período</th>
    <th title="Situação"> Situação</th>
    </thead>
    <tbody>
    @foreach ($alunoTurmas as $disciplina)
        <tr>
             <td>
                {!! Form::label('nome_disciplina', $disciplina->nome_disciplina) !!}<br>
            </td>
            <td>
                {!! Form::label('periodo_descricao', $disciplina->turma->periodo->descricao) !!} -
                {!! Form::label('nome_turma', $disciplina->turma->nome) !!}
                <br>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>