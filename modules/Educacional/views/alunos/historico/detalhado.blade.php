@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'aluno-historico-detalhado',  'method' => 'GET']) !!}

        @include('alunos.historico.fields_detalhado')

        @include('alunos.historico.table')

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('aluno-historico.index') !!}" class="btn btn-primary">Voltar</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection