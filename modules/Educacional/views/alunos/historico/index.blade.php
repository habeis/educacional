@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'aluno-historico-detalhado',  'method' => 'GET']) !!}

        @include('alunos.historico.fields')

        {!! Form::close() !!}
    </div>
@endsection