<div class="table-responsive col-lg-12">

    <div class="form-group col-sm-6 ">
        {!! Form::label('matricula', 'Matrícula:') !!}
        {!! Form::text('matricula',null, ['placeholder' => 'Matrícula do Aluno', 'required' => 'required','class' => 'form-control col-sm-6']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('nome', 'Nome:') !!}
        {!! Form::text('nome',null, ['placeholder' => 'Nome do Aluno', 'required' => 'required','class' => 'form-control col-sm-6']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('curso', 'Curso:') !!}
        {!! Form::text('curso',null, ['placeholder' => 'Nome do Curso', 'required' => 'required','class' => 'form-control col-sm-12']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('portaria', 'Portaria:') !!}
        {!! Form::text('portaria',null, ['placeholder' => 'Portaria do Curso', 'required' => 'required','class' => 'form-control col-sm-12']) !!}
    </div>


</div>