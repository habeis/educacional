<!-- Escola Conclusao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('escola_conclusao', 'Escola Conclusao::') !!}
    {!! Form::text('escola_conclusao', $aluno->escola_conclusao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Tipo Escola Conclusao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_escola_conclusao', 'Escola Conclusão:') !!}
    {!! Form::text('tipo_escola_conclusao', $aluno->tipo_escola_conclusao , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Ano Conclusao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ano_conclusao', 'Ano Conclusao:') !!}
    {!! Form::text('ano_conclusao', $aluno->ano_conclusao , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Cidade Conclusao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cidade_conclusao', 'Cidade Conclusao:') !!}
    {!! Form::text('cidade_conclusao', $aluno->cidade_conclusao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Ativo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ativo', 'Ativo:') !!}
    {!! Form::text('ativo', $aluno->ativo, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

