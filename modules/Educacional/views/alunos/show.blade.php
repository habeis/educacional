@extends('layouts.page')

@section('content-center')

    <a href="{!! route('alunos.index') !!}" class="btn btn-primary">Listar alunos</a>
    <div id="wizard" class="form_wizard wizard_horizontal">

        @include('alunos.step')
        <div class="stepContainer">
            <div id="step-1" class="wizard_content" style="display: block;">

            @include('pessoas.show_fields')
            </div>
            <div id="step-2" class="wizard_content" style="display: none;">
                @include('alunos.show_fields')
            </div>
            <div id="step-3" class="wizard_content" style="display: none;">

                @include('enderecos.show_fields')

            </div>
        </div>
    </div>

@endsection

@section('post-script')

    <script src="{{ asset('js/wizard/jquery.smartWizard.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard
            $('#wizard').smartWizard({
                includeFinishButton: false,
            });

            function onFinishCallback() {
                $('#wizard').smartWizard(
                        'showMessage', 'Finish Clicked');
                //alert('Finish Clicked');
            }
        });
        $(document).ready(function () {
            // Smart Wizard
            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });
        });
    </script>

@endsection
