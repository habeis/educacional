<table class="table table-responsive" id="alunos-table">
    <thead>
    <th>Usuario</th>
    <th>CPF</th>
    <th>Ativo</th>
    <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($alunos as $aluno)
        <tr>
            <td>{!! $aluno->user->pessoa->nome !!}</td>
            <td>{!! $aluno->user->pessoa->cpf !!}</td>
            <td>{!! $aluno->user->pessoa->email !!}</td>
            <td>{!! $aluno->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['alunos.destroy', $aluno->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('alunos.show', [$aluno->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('alunos.edit', [$aluno->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>