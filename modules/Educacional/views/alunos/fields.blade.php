<div class="table-responsive col-lg-12">
    <div class="panel-group h2">
        <span>Informaçao Escolar</span>
    </div>

    <!-- Escola Conclusao Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('escola_conclusao', 'Escola Conclusão:') !!}
        {!! Form::text('escola_conclusao', $aluno->escola_conclusao, ['class' => 'form-control','required'=>'required','data-parsley-id'=>'0924']) !!}
    </div>

    <!-- Tipo Escola Conclusao Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tipo_escola_conclusao', 'Tipo Escola Conclusão:') !!}
        {!! Form::select('tipo_escola_conclusao', trans("educacional.tipo_escola"), $aluno->tipo_escola_conclusao, ['placeholder' => 'Selecione o  Tipo de Escola', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
    </div>

    <!-- Ano Conclusao Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('ano_conclusao', 'Ano Conclusao:') !!}
        {!! Form::number('ano_conclusao', $aluno->ano_conclusao, ['class' => 'form-control']) !!}
    </div>

    <!-- Cidade Conclusao Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('cidade_conclusao', 'Cidade Conclusao:') !!}
        {!! Form::text('cidade_conclusao', $aluno->cidade_conclusao, ['class' => 'form-control']) !!}
    </div>

    <!-- Tipo Curso Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tipo_curso_id', 'Grau de Instrução:') !!}
        {!! Form::select('tipo_curso_id', trans("educacional.grau_instrucao"), $aluno->tipo_curso_id, ['placeholder' => 'Selecione o  Tipo de Escola', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
    </div>

</div>