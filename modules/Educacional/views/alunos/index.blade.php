@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('alunos.create') !!}">{{ trans("forms.button_create") }}
    </a>

    <div class="clearfix"></div>
    @if($alunos->isEmpty())
        <div class="well text-center">Não foi encontrada nenhum Aluno(s).</div>
    @else
        @include('alunos.table')
    @endif
@endsection