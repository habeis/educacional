@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <a href="{!! route('alunos.index') !!}" class="btn btn-primary">Listar alunos</a>

    <div id="wizard" class="form_wizard wizard_horizontal">
        @include('alunos.step')
        <div class="stepContainer">
            {!! Form::open(['route' => 'alunos.store']) !!}
            <div id="step-1" class="wizard_content" style="display: block;">
                @include('pessoas.fields')
            </div>
            <div id="step-2" class="wizard_content" style="display: none;">
                @include('alunos.fields')
            </div>
            <div id="step-3" class="wizard_content" style="display: none;">
                @include('enderecos.fields')
                        <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('post-script')

    <script src="{{ asset('js/wizard/jquery.smartWizard.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard
            $('#wizard').smartWizard({
                includeFinishButton: false,

            });

            function onFinishCallback() {
                $('#wizard').smartWizard(
                        'showMessage', 'Finish Clicked');
                //alert('Finish Clicked');
            }
        });

        $(document).ready(function () {
            // Smart Wizard
            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });

        });
    </script>


@endsection
