<table class="table table-responsive">
    <thead>
        <th>Descricao</th>
        <th>Ativo</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($portarias as $portaria)
        <tr>
            <td>{!! $portaria->descricao !!}</td>
            <td>{!! $portaria->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['portarias.destroy', $portaria->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('portarias.show', [$portaria->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('portarias.edit', [$portaria->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>