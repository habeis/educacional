@extends('layouts.page')

@section('content-center')
    <div class="form-group">
        <h2>{{ $anoSemestre->ano_semestre }} -{{$curso->nome }} </h2>
    </div>
    @include('turmas.show_fields')

@endsection
