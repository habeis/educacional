<table class="table table-responsive">
    <thead>
    <th>Nome</th>
    <th>Classe</th>
    <th>Período</th>
    <th>Máximo de Alunos</th>
    <th>Ano Semestre Id</th>
    <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($turmas as $turma)
        <tr>
            <td>{!! $turma->nome !!}</td>
            <td>{!! $turma->classe !!}</td>
            <td>{!! $turma->periodo->descricao!!}</td>
            <td>{!! $turma->maximo_aluno !!}</td>
            <td>{!! $turma->anoSemestre->ano_semestre !!}</td>
            <td>
                {!! Form::open(['route' => ['turmas.destroy', $turma->id, 'semestreLetivoId' => $anoSemestre,'cursoId'=>$turma->curso_id ,'gradeId'=> $gradeId ], 'method' => 'delete']) !!}

                <div class='btn-group'>
                    <a href="{!! route('turmas.show', [$turma->id,'semestreLetivoId' => $anoSemestre,'cursoId'=>$turma->curso_id ,'gradeId'=> $gradeId ]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="{!! route('turmas.edit', [$turma->id, 'semestreLetivoId' => $anoSemestre,'cursoId'=>$turma->curso_id ,'gradeId'=> $gradeId ]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i>
                    </a>
                    <a href="{!! route('turmaDisciplinas.index', ['turmaId'=>$turma->id , 'semestreLetivoId' => $anoSemestre,'cursoId'=>$turma->curso_id ,'gradeId'=> $gradeId ]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-th-list"></i>
                    </a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Você realmente deseja deletar?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>