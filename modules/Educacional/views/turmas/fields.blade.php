{!! Form::hidden('gradeId', $gradeId) !!}
        <!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('curso', 'Curso:') !!}
    {!! Form::text('curso', $curso->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
    {!! Form::hidden('curso_id', $curso->id) !!}
</div>

<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anosemestre', 'Semestre Levtivo:') !!}
    {!! Form::text('anosemestre', $anoSemestre->ano_semestre, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
    {!! Form::hidden('ano_semestre_id', $anoSemestre->id) !!}
</div>

<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('periodo_id', 'Periodos:') !!}
    {!! Form::select('periodo_id',array_pluck($periodos,'descricao','id'), null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}

</div>

<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control','placeholder'=>'NomeSemestreAno']) !!}
</div>

<!-- Classe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('classe', 'Classe:') !!}
    {!! Form::text('classe', null, ['title'=> 'Classe da turma (A,B,C...M)','class' => 'form-control','placeholder'=>'A,B,C...M','maxlength'=>'1']) !!}
</div>

<!-- Maximo Aluno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('maximo_aluno', 'Maximo Aluno:') !!}
    {!! Form::number('maximo_aluno', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('turmas.index',
        ['semestreLetivoId' => $anoSemestre->id, 'cursoId' => $curso->id, 'gradeId' => $gradeId ])
        !!}" class="btn btn-primary">
        Cancel
    </a>
</div>
