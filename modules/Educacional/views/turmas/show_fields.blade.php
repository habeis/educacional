<!-- Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id', 'Código:') !!}
    <p>{!! Form::text('id', $turma->id, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Nome Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{!! Form::text('nome', $turma->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Classe Field -->
<div class="form-group col-sm-12">
    {!! Form::label('classe', 'Classe:') !!}
    <p>{!! Form::text('classe', $turma->classe, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Periodo Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('periodo_id', 'Período:') !!}
    <p>{!! Form::text('periodo_id', $turma->periodo_id, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Maximo Aluno Field -->
<div class="form-group col-sm-12">
    {!! Form::label('maximo_aluno', 'Máximo Aluno:') !!}
    <p>{!! Form::text('maximo_aluno', $turma->maximo_aluno, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Ano Semestre Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('ano_semestre_id', 'Semestre Letivo:') !!}
    <p>{!! Form::text('ano_semestre_id', $turma->anoSemestre->ano_semestre, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('turmas.index',
    ['semestreLetivoId' => $anoSemestre->id, 'cursoId' => $curso->id, 'gradeId' => $gradeId ]) !!}"
       class="btn btn-primary">
        Cancelar
    </a>
</div>
