@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'turmas.index', 'method' => 'GET']) !!}

        <div class="form-group col-sm-12">
            {!! Form::label('semestreLetivoId', 'Semestre Letivo:') !!}
            {!! Form::select('semestreLetivoId',$anoSemestre, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('cursoId', 'Cursos:') !!}
            {!! Form::select('cursoId',$cursos, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('gradeId', 'Grade:') !!}
            {!! Form::select('gradeId',[], null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::submit('Avançar', ['class' => 'btn btn-success']) !!}
        </div>

        {!! Form::close() !!}
    </div>
    @endsection

            <!-- Busca a Grade do curso via javascript-->
@section('post-script')
    <script type="text/javascript">
        $("#cursoId").on("change", function (event) {

            var token = $("input[type=hidden][name=_token]").val();

            var cursoId = event.target.value;
            // Acessa uma determinada Rota passando o Id do curso e preenchendo o Select com as Grades
            $.get('/semestre-cursos-grade?curso_id=' + cursoId, function (data) {
                console.log(data);
                $("#gradeId").empty();
                $("#gradeId").append('<option value="">Seleciona a grade</option>');
                $.each(data, function (index, gradeObjt) {
                    $("#gradeId").append('<option value="' + gradeObjt.id + '"> Grade versão ' + gradeObjt.versao + ' - ' + gradeObjt['nome'] + ' </option>');
                });
            });
        });
    </script>
@endsection
