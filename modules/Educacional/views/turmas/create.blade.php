@extends('layouts.page')

@section('content-center')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'turmas.store']) !!}

        @include('turmas.fields')

        {!! Form::close() !!}
    </div>
@endsection