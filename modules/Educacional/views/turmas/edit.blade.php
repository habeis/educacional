@extends('layouts.page')

@section('content-center')

    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($turma, ['route' => ['turmas.update', $turma->id], 'method' => 'patch']) !!}

        @include('turmas.fields')

        {!! Form::close() !!}
    </div> '
@endsection