@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <div class="col-lg-6">
        <a class="btn btn-primary pull-left" style="margin-top: 25px " title="Selecionar Turma de um curso"
           href="{!! route('semestre-cursos') !!}">
            Curso(s)
        </a>
    </div>
    <div class="col-lg-6">
        <a class="btn btn-primary pull-right" style="margin-top: 25px"
           href="{!! route('turmas.create',
                        ['semestreLetivoId'=>$anoSemestre, 'cursoId'=>$cursoId,'gradeId'=>$gradeId]) !!}">
            {{trans("forms.button_create") }}
        </a>
    </div>

    <div class="clearfix"></div>

    @if($turmas->isEmpty())
        <div class="well text-center">Nenhuma Turma encontrada.</div>
    @else
        @include('turmas.table')
    @endif

@endsection