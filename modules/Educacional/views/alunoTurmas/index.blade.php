@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    @include('core-templates::common.errors')
    <div class="row">
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('aluno-historico.index') !!}" class="btn btn-primary">Histórico</a>
        </div>

        {!! Form::open(['id'=>'form1','route' => 'alunos-turma', 'method' => 'GET', 'data-parsley-validate','class'=>'form-horizontal form-label-left']) !!}

        <div class="form-group col-sm-12">
            {!! Html::decode(Form::label('tipoCursoId','Tipo de Curso <span class="required">*</span>')) !!}
            {!! Form::select('tipoCursoId',array_pluck($coligada->tipoCursos,'descricao','id'), null, ['placeholder' => 'Seleciona um Tipo de Curso', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('semestreLetivoId', 'Semestre Letivo:') !!}
            {!! Form::select('semestreLetivoId',[], null, ['placeholder' => 'Seleciona um Semestre Letivo','class' => 'form-control col-sm-6', 'disabled'=>'disabled', 'required' => 'required']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('cursoId', 'Cursos:') !!}
            {!! Form::select('cursoId',[], null, ['placeholder' => 'Seleciona um Curso','class' => 'form-control col-sm-6', 'disabled'=>'disabled', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#tab_content1" id="tab1" name="tab1" role="tab" data-toggle="tab" aria-expanded="true">Calouro</a>
                </li>
                <li role="presentation" class="">
                    <a href="#tab_content2" role="tab" id="tab2" name="tab2" data-toggle="tab"
                       aria-expanded="false">Veterano</a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                    <div class="form-group col-sm-12">
                        {!! Form::label('turmaId', 'Turma:') !!}
                        {!! Form::select('turmaId',[], null, ['placeholder' => 'Seleciona uma Turma','class' => 'form-control col-sm-6', 'required' => 'required']) !!}
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                    <div class="form-group col-sm-12">
                        {!! Form::label('matricula', 'Matrícula:') !!}
                        {!! Form::text('matricula',null,['placeholder' => 'Digite a Matrícula do Aluno', 'required' => 'required','class' => 'form-control col-sm-6', 'disabled'=>'disabled']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group col-sm-12">
            {!! Form::submit('Avançar', ['class' => 'btn btn-success']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

@section('post-script')
    <script type="text/javascript">
        // Consulta os Cursos do Tipo de Curso
        $("#tipoCursoId").on("change", function (event) {
            desativarSemestre();

            var tipoCursoId = event.target.value;
            // Só efetua a consulta se o id for maior que 0
            if (tipoCursoId > 0) {
                ativaSemestre();
                //Busca o curso e coloca no option todos os cursos
                $.get('/curso-tipo-curso?tipo_curso_id=' + tipoCursoId, function (data) {
                    $.each(data, function (index, cursoObjt) {
                        $("#cursoId").append('<option value="' + cursoObjt.id + '"> ' + cursoObjt.nome + ' </option>');
                    });
                });

                // Lista os semestre e coloca no option
                $.get('/ano-semestre-tipo-curso?tipo_curso_id=' + tipoCursoId, function (data) {
                    console.log(data);
                    $.each(data, function (index, cursoObjt) {
                        $("#semestreLetivoId").append('<option value="' + cursoObjt.id + '"> ' + cursoObjt.ano_semestre + '  </option>');
                    });
                });
            }
        });

        $("#semestreLetivoId").on("change", function (event) {

            desativarCurso();

            if (event.target.value > 0) {
                // Ativa o curso e seleciona o primeiro
                ativaCurso();
            }

        });
        $("#cursoId").on("change", function (event) {

            var token = $("input[type=hidden][name=_token]").val();

            var cursoId = event.target.value;

            var anoSemestreId = $("#semestreLetivoId option:selected").val();

            limparTurma();
            ativarTipoAluno();
            if (cursoId > 0) {

                ativaTurma();
                $.get('/turmas-semestre?curso_id=' + cursoId + '&semestreLetivo_id=' + anoSemestreId, function (data) {

                    $.each(data, function (index, turmaObjt) {
                        $("#turmaId").append('<option value="' + turmaObjt.turmaId + '"> Turma -' + turmaObjt.nomeTurma + ' - ' + turmaObjt.descricaoPeriodo + ' </option>');
                    });
                });
            }
        });

        $("#tab1").on("click", function (event) {
            desativaMatricula();
            ativaTurma();
        });

        $("#tab2").on("click", function (event) {
            ativarMatricula();
            desativarTurma();
        });

        function desativaMatricula() {
            $("#matricula").prop("disabled", true);
        }
        function ativarMatricula() {
            $("#matricula").prop("disabled", false);
        }

        function ativarTipoAluno() {
            $("#tipoAluno").prop("disabled", false);
        }
        function limparSemestre() {
            $("#semestreLetivoId").empty();
            $("#semestreLetivoId").append('<option value="">Seleciona um Semestre Letivo</option>');
            limparCurso();
        }
        function desativarSemestre() {
            $('#semestreLetivoId option:first').prop('selected', true);
            $("#semestreLetivoId").prop("disabled", true);
            desativarCurso();
            limparSemestre();
        }
        function ativaSemestre() {
            $('#semestreLetivoId  option:first').prop('selected', true);
            $("#semestreLetivoId").prop("disabled", false);
        }
        function ativaCurso() {
            $('#cursoId  option:first').prop('selected', true);
            $("#cursoId").prop("disabled", false);
            limparTurma();
        }
        function ativaTurma() {
            $('#turmaId  option:first').prop('selected', true);
            $("#turmaId").prop("disabled", false);
        }
        function desativarCurso() {
            $('#cursoId option:first').prop('selected', true);
            $("#cursoId").prop("disabled", true);
            limparTurma();
        }
        function desativarTurma() {
            $("#turmaId").prop("disabled", true);
        }
        function limparCurso() {
            $("#cursoId").empty();
            $("#cursoId").append('<option value="">Seleciona um Curso</option>');
            limparTurma();
        }
        function limparTurma() {
            $("#turmaId").empty();
            $("#turmaId").append('<option value="">Seleciona uma Turma</option>');
            desativarTurma();
        }

        $(document).ready(function () {
            $.listen('parsley:field:validate', function () {
                validateFront();
            });
            $('#form1.btn').on('click', function () {
                alert('oi');
                $('#form1').parsley().validate();
                validateFront();
            });
            var validateFront = function () {
                if (true === $('#demo-form2').parsley().isValid()) {
                    $('.bs-callout-info').removeClass('hidden');
                    $('.bs-callout-warning').addClass('hidden');
                } else {
                    $('.bs-callout-info').addClass('hidden');
                    $('.bs-callout-warning').removeClass('hidden');
                }
            };
        });
    </script>
@endsection
