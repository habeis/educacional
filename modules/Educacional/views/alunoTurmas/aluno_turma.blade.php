@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <div class="form-group col-sm-12">
        <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-check-square-o"></i>
                </div>
                <h2>{!! $turma->curso->nome.' - '.$turma->anoSemestre->ano_semestre !!}</h2>
                <div class="count"> {!! $turma->nome !!}</div>
                <h3>{!! $turma->periodo->numero_periodo.'º Período ' !!}</h3>

            </div>
        </div>
    </div>

    <div>
        <a href="{!! route('alunoTurmas.index') !!}" class="btn btn-primary pull-left">
            Calouro / Veterano
        </a>
        <a class="btn btn-primary pull-right" href="{!! route('aluno-cpf',$parametros) !!}">
            Matrícular Novo Aluno
        </a>
    </div>
    <div class="clearfix"></div>
    @if($alunoTurmas->isEmpty())

        <div class="well text-center">Nenhum aluno matrículado nessa Turma.</div>

    @else
        @include('alunoTurmas.table')
        @include('core-templates::common.paginate', ['records' => $alunoTurmas])
    @endif

@endsection