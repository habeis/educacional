<table class="table table-responsive" id="alunoTurmas-table">
    <thead>
        <th>Matricula</th>
        <th>Aluno</th>
        <th>email</th>
        <th>Status</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($alunoTurmas as $alunoTurma)
        <tr>
            <td>{!! $alunoTurma->matricula !!}</td>
            <td>{!! $alunoTurma->aluno->user->pessoa->nome !!}</td>
            <td>{!! $alunoTurma->aluno->user->pessoa->email !!}</td>
            <td>
                {!! Form::label('status_aluno', array_get(trans("educacional.status_aluno"),$alunoTurma->status_id)) !!}</td>
            <td>
                {!! Form::open(['route' => ['alunoTurmas.destroy', $alunoTurma->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('alunoTurmas.show', [$alunoTurma->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('alunoTurmas.edit', [$alunoTurma->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>