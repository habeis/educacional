@extends('layouts.app')

@section('content')
    @include('alunoTurmas.show_fields')

    <div class="form-group">
           <a href="{!! route('alunoTurmas.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
