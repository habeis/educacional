
<!-- Matricula Field -->
<div class="form-group col-sm-6">
    {!! Form::label('matricula', 'Matricula:') !!}
    {!! Form::number('matricula', $alunoTurma->matricula, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Classe  -->
<div class="form-group col-sm-6">
    {!! Form::label('ano_semestre', 'Semestre:') !!}
    {!! Form::text('ano_semestre', $turma->anoSemestre->ano_semestre, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
</div>

<!-- Matricula Field -->
<div class="form-group col-sm-6">
    {!! Form::label('curso', 'Curso:') !!}
    {!! Form::text('curso', $turma->curso->nome, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
</div>

<!-- Turma Id Field -->
<div class="form-group col-sm-6">

    {!! Form::label('turma_id', 'Turma:') !!}
    {!! Form::text('turma_id', $turma->nome, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>


<!-- Aluno Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('periodo', 'Período:') !!}
    {!! Form::text('periodo', $turma->periodo->descricao, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Forma Ingresso Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('forma_ingresso_id', 'Forma Ingresso:') !!}
    {!! Form::select('forma_ingresso_id', trans("educacional.forma_ingresso"), $alunoTurma->forma_ingresso_id, ['placeholder' => 'Selecione o Tipo de Curso', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
</div>

<!-- Data Ingresso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_ingresso', 'Data Ingresso:') !!}
    {!! Form::date('data_ingresso', $alunoTurma->data_ingresso, ['class' => 'form-control']) !!}
</div>

<!-- Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status_id', 'Status Aluno:') !!}
    {!! Form::select('status_id', trans("educacional.status_aluno"), $alunoTurma->status_id, ['placeholder' => 'Selecione o  Status do Aluno', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
</div>