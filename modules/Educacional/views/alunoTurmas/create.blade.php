@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    <a href="{!! route('alunos-turma', $parametros) !!}" class="btn btn-primary">Listar alunos</a>

    @include('core-templates::common.errors')

    <div id="wizard" class="form_wizard wizard_horizontal">
        @include('alunoTurmas.step')
        <div class="stepContainer">
            {!! Form::open(['route' => 'alunoTurmas.store']) !!}

            {!! Form::hidden('tipoCursoId',$tipoCursoId)  !!}
            {!! Form::hidden('semestreLetivoId',$semestreLetivoId)  !!}
            {!! Form::hidden('cursoId',$cursoId)  !!}
            {!! Form::hidden('turmaId',$turmaId)  !!}

            <div id="step-1" class="wizard_content" style="display: block;">

                {!! Form::hidden('cpf', $pessoa->cpf) !!}

                @include('pessoas.fields')
            </div>
            <div id="step-2" class="wizard_content" style="display: none;">

                @include('alunos.fields')
            </div>
            <div id="step-3" class="wizard_content" style="display: none;">

                {!! Form::hidden('endereco_id', $endereco->id) !!}
                @include('enderecos.fields')

            </div>
            <div id="step-4" class="wizard_content" style="display: none;">

                {!! Form::hidden('turma_id', $turma->id) !!}
                @include('alunoTurmas.fields')

                @include('alunoTurmaDisciplinas.disciplinas_turma')

                        <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
                    <a href="{!! route('alunos-turma',$parametros) !!}" class="btn btn-primary">Cancelar</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('post-script')

    <script src="{{ asset('js/wizard/jquery.smartWizard.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard
            $('#wizard').smartWizard({
                includeFinishButton: false,
            });

            function onFinishCallback() {
                $('#wizard').smartWizard(
                        'showMessage', 'Finish Clicked');
                //alert('Finish Clicked');
            }
        });

        $(document).ready(function () {
            // Smart Wizard
            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });

        });
    </script>

@endsection
