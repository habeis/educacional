@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit AlunoTurma</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($alunoTurma, ['route' => ['alunoTurmas.update', $alunoTurma->id], 'method' => 'patch']) !!}

            @include('alunoTurmas.fields')

            {!! Form::close() !!}
        </div>
@endsection