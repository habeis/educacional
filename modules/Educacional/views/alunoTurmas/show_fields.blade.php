<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $alunoTurma->id !!}</p>
</div>

<!-- Turma Id Field -->
<div class="form-group">
    {!! Form::label('turma_id', 'Turma Id:') !!}
    <p>{!! $alunoTurma->turma_id !!}</p>
</div>

<!-- Aluno Id Field -->
<div class="form-group">
    {!! Form::label('aluno_id', 'Aluno Id:') !!}
    <p>{!! $alunoTurma->aluno_id !!}</p>
</div>

<!-- Matricula Field -->
<div class="form-group">
    {!! Form::label('matricula', 'Matricula:') !!}
    <p>{!! $alunoTurma->matricula !!}</p>
</div>

<!-- Forma Ingresso Id Field -->
<div class="form-group">
    {!! Form::label('forma_ingresso_id', 'Forma Ingresso Id:') !!}
    <p>{!! $alunoTurma->forma_ingresso_id !!}</p>
</div>

<!-- Data Ingresso Field -->
<div class="form-group">
    {!! Form::label('data_ingresso', 'Data Ingresso:') !!}
    <p>{!! $alunoTurma->data_ingresso !!}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{!! $alunoTurma->status_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $alunoTurma->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $alunoTurma->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $alunoTurma->deleted_at !!}</p>
</div>

