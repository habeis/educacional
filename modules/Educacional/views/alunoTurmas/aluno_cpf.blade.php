@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    <a href="{!! route('alunos-turma', $parametros) !!}" class="btn btn-primary">Listar alunos</a>

    @include('core-templates::common.errors')

    <div class="clearfix"></div>
    <div class="row">

        {!! Form::open(['route' => 'alunoTurmas.create', 'method' => 'get','data-parsley-validate','class'=>'form-horizontal form-label-left']) !!}

        {!! Form::hidden('tipoCursoId',$tipoCursoId)  !!}
        {!! Form::hidden('semestreLetivoId',$semestreLetivoId)  !!}
        {!! Form::hidden('cursoId',$cursoId)  !!}
        {!! Form::hidden('turmaId',$turmaId)  !!}

        <div class="form-group col-md-6  ">
            {!! Form::label('cpf', 'CPF: ') !!}
            {!! Form::text('cpf', null, ['class'=> 'form-control', 'data-inputmask'=> "'mask': '999.999.999-99'", 'placeholder'=> 'CPF', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::submit('Avançar', ['class' => 'btn btn-success']) !!}
        </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('post-script')
    <!-- Mask Javascript -->
    <!-- Javascript -->
    <script src="{{ asset('js/input_mask/jquery.inputmask.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(":input").inputmask();
        });

    </script>
@endsection
