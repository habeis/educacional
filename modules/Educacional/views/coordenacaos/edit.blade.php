@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($coordenacao, ['route' => ['coordenacaos.update', $coordenacao->id], 'method' => 'patch']) !!}

        @include('coordenacaos.fields')

        {!! Form::close() !!}
    </div>

@endsection