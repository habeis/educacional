@extends('layouts.page')

@section('content-center')

    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'coordenacaos.store']) !!}

        @include('coordenacaos.fields')

        {!! Form::close() !!}
    </div>
@endsection