<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    {!! Form::text('id', $coordenacao->id , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', $coordenacao->nome , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Telefone Field -->
<div class="form-group">
    {!! Form::label('telefone', 'Telefone:') !!}
    {!! Form::text('telefone', $coordenacao->telefone , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Ramal Field -->
<div class="form-group">
    {!! Form::label('ramal', 'Ramal:') !!}
    {!! Form::text('ramal', $coordenacao->ramal , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('coordenacaos.index') !!}" class="btn btn-primary">Cancelar</a>
</div>