<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefone', 'Telefone:') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<!-- Ramal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ramal', 'Ramal:') !!}
    {!! Form::text('ramal', null, ['class' => 'form-control']) !!}
</div>

<!-- Coligada Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coligada_id', 'Coligada Id:') !!}
    {!! Form::number('coligada_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('coordenacaos.index') !!}" class="btn btn-primary">Cancelar</a>
</div>
