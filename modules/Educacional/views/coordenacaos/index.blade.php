@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('coordenacaos.create') !!}">{{ trans("forms.button_create") }}
    </a>
    <div class="clearfix"></div>

    @if($coordenacaos->isEmpty())
        <div class="well text-center">No Coordenacaos found.</div>
    @else
        @include('coordenacaos.table')
    @endif

@endsection