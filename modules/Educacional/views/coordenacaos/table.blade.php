<table class="table table-responsive">
    <thead>
        <th>Nome</th>
        <th>Telefone</th>
        <th>Ramal</th>
        <th>Coligada Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($coordenacaos as $coordenacao)
        <tr>
            <td>{!! $coordenacao->nome !!}</td>
            <td>{!! $coordenacao->telefone !!}</td>
            <td>{!! $coordenacao->ramal !!}</td>
            <td>{!! $coordenacao->coligada_id !!}</td>
            <td>
                {!! Form::open(['route' => ['coordenacaos.destroy', $coordenacao->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('coordenacaos.show', [$coordenacao->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('coordenacaos.edit', [$coordenacao->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>