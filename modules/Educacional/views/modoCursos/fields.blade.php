<!-- Descricao Field -->
<div class="form-group col-sm-12">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::text('descricao', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ativo', 'Ativo:') !!}
    {!!
        Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}

    {!! Form::check('ativo', true, isset($modoCurso->ativo) && $modoCurso->ativo == true ? 1 : 0) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('modoCursos.index') !!}" class="btn btn-primary">Cancel</a>
</div>
