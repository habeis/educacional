<table class="table table-responsive">
    <thead>
        <th>Descricao</th>
        <th>Ativo</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($modoCursos as $modoCurso)
        <tr>
            <td>{!! $modoCurso->descricao !!}</td>
            <td>{!! $modoCurso->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['modoCursos.destroy', $modoCurso->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('modoCursos.show', [$modoCurso->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('modoCursos.edit', [$modoCurso->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>