@extends('layouts.app')

@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Modo de Curso</h3>
                <small>Acadêmico -> Cadastro -> Modo de Curso</small>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="clearfix"></div>
        <div class="row">
            <div class="container col-md-5 col-sm-5 col-xs-5">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar Modo de Curso </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </li>
                            <li>
                                <a class="close-link">
                                    <i class="fa fa-close">
                                    </i>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @include('core-templates::common.errors')
                        <div class="row">
                            {!! Form::model($modoCurso, ['route' => ['modoCursos.update', $modoCurso->id], 'method' => 'patch']) !!}

                            @include('modoCursos.fields')

                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection