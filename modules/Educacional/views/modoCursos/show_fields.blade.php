<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    {!! Form::text('id', $modoCurso->id, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::text('descricao', $modoCurso->descricao, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Ativo Field -->
<div class="form-group">
    {!! Form::label('ativo', 'Ativo:') !!}
    {!! Form::text('ativo', $modoCurso->ativo, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('modoCursos.index') !!}" class="btn btn-primary">Cancel</a>
</div>

