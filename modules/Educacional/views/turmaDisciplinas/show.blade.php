@extends('layouts.app')

@section('content')
    @include('turmaDisciplinas.show_fields')

    <div class="form-group">
        <a href="{!! route('turmaDisciplinas.index',['turmaId' => $turmaId, 'semestreLetivoId' => $semestreLetivoId, 'cursoId' => $cursoId, 'gradeId' => $gradeId ]) !!}"
           class="btn btn-default">
            Voltar
        </a>
    </div>
@endsection
