@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    <div CLASS="col-lg-6  ">
        <a class="btn btn-primary pull-left" style="margin-top: 25px " title="Selecionar Turma de um curso"
           href="{!! route('semestre-cursos') !!}">
            Curso(s)
        </a>
        <a class="btn btn-primary pull-left" style="margin-top: 25px " title="Selecionar Turma de um curso"
           href="{!! route('turmas.index',
       [
           'turmaId'=> $turmaId,
           'semestreLetivoId'=> $semestreLetivoId,
           'cursoId'=> $cursoId,
           'gradeId'=> $gradeId
       ]
       ) !!}">
            Turma(s)
        </a>
    </div>
    <div class="col-lg-6">
        <a class="btn btn-primary pull-right" style="margin-top: 25px"
           href="{!! route(
       'turmaDisciplinas.create',
       [
           'turmaId'=> $turmaId,
           'semestreLetivoId'=> $semestreLetivoId,
           'cursoId'=> $cursoId,
           'gradeId'=> $gradeId
       ]
       ) !!}">
            {{ trans("forms.button_create") }}
        </a>
    </div>

    <div class="clearfix"></div>

    @if($turmaDisciplinas->isEmpty())
        <div class="well text-center">Nenhuma disciplina encontrada.</div>
    @else
        @include('turmaDisciplinas.table')
    @endif

@endsection