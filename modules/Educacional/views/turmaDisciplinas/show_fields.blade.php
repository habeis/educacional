<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $turmaDisciplina->id !!}</p>
</div>

<!-- Nome Disciplina Field -->
<div class="form-group">
    {!! Form::label('nome_disciplina', 'Nome Disciplina:') !!}
    <p>{!! $turmaDisciplina->nome_disciplina !!}</p>
</div>

<!-- Turma Id Field -->
<div class="form-group">
    {!! Form::label('turma_id', 'Turma Id:') !!}
    <p>{!! $turmaDisciplina->turma_id !!}</p>
</div>

<!-- Matrriz Curricular Disciplina Id Field -->
<div class="form-group">
    {!! Form::label('matrriz_curricular_disciplina_id', 'Matrriz Curricular Disciplina Id:') !!}
    <p>{!! $turmaDisciplina->matrriz_curricular_disciplina_id !!}</p>
</div>
