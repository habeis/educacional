<table class="table table-responsive" id="turmaDisciplinas-table">
    <thead>
    <th>Nome Disciplina</th>
    <th>Turma</th>
    <th>Carga horária</th>
    <th>Período</th>
    <th>Tipo Disciplina</th>
    <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($turmaDisciplinas as $turmaDisciplina)
        <tr>
            <td>{!! $turmaDisciplina->nome_disciplina !!}</td>
            <td>{!! $turmaDisciplina->turma->nome !!}</td>
            <td>{!! $turmaDisciplina->matrizCurricularDisciplina->carga_horaria !!}</td>
            <td>{!! $turmaDisciplina->matrizCurricularDisciplina->periodo->descricao !!}</td>
            <td>{!! $turmaDisciplina->matrizCurricularDisciplina->tipoDisciplina->descricao !!}</td>
            <td>
                {!! Form::open(['route' =>
                [
                    'turmaDisciplinas.destroy',
                    $turmaDisciplina->id,
                    'turmaId'=> $turmaId,
                    'cursoId'=> $cursoId,
                    'gradeId'=>$gradeId,
                    'semestreLetivoId' =>$semestreLetivoId
                ], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('turmaDisciplinas.show', [$turmaDisciplina->id,
                                                                'turmaId'=> $turmaId,
                                                                'cursoId'=> $cursoId,
                                                                'gradeId'=>$gradeId,
                                                                'semestreLetivoId' => $semestreLetivoId ]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>