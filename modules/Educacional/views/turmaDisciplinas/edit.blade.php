@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::model($turmaDisciplina, ['route' => ['turmaDisciplinas.update', $turmaDisciplina->id], 'method' => 'patch']) !!}

        @include('turmaDisciplinas.fields')

        {!! Form::close() !!}
    </div>
@endsection