{!! Form::hidden('turmaId',$turmaId) !!}
{!! Form::hidden('cursoId',$cursoId) !!}
{!! Form::hidden('gradeId',$gradeId) !!}
{!! Form::hidden('semestreLetivoId',$semestreLetivoId) !!}

        <!-- Curso que irá cadastrar -->
<div class="form-group col-sm-12">
    {!! Form::label('curso', 'Matrriz Curricular Disciplina Id:') !!}
    {!! Form::text('Curso', $turma->curso->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Turma Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('turma_nome', 'Turma:') !!}
    {!! Form::text('turma_nome',$turma->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Período Field -->
<div class="form-group col-sm-6">
    {!! Form::label('periodo', 'Período:') !!}
    {!! Form::text('periodo',$turma->periodo->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<div class="form-group col-lg-3">
    @foreach ($disciplinaPeriodos  as $disciplinaPeriodo)
        {!! Form::checkbox('disciplinaPeriodos[]', $disciplinaPeriodo->id,in_array($disciplinaPeriodo->id,array_pluck($selecDisciplinaPeriodo,'matrriz_curricular_disciplina_id'))) !!}
        {!! Form::label('disciplinaPeriodos[]', $disciplinaPeriodo->nome_disciplina) !!}<br>
    @endforeach

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('turmaDisciplinas.index',
    [
           'turmaId'=> $turmaId,
           'semestreLetivoId'=> $semestreLetivoId,
           'cursoId'=> $cursoId,
           'gradeId'=> $gradeId
       ]
   ) !!}" class="btn btn-primary">
        Cancel
    </a>
</div>
