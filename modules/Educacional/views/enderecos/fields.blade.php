<div class="table-responsive col-lg-12">
    <div class="panel-group h2">
        <span>Endereço</span>
    </div>

    <!-- Logradouro Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('logradouro', 'Logradouro:') !!}
        {!! Form::text('logradouro', $endereco->logradouro, ['class' => 'form-control']) !!}
    </div>

    <!-- Numero Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('numero', 'Número:') !!}
        {!! Form::number('numero', $endereco->numero, ['class' => 'form-control']) !!}
    </div>

    <!-- Cep Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('cep', 'Cep:') !!}
        {!! Form::text('cep', $endereco->cep, ['class' => 'form-control']) !!}
    </div>

    <!-- Bairro Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('bairro', 'Bairro:') !!}
        {!! Form::text('bairro', $endereco->bairro, ['class' => 'form-control']) !!}
    </div>

    <!-- Cidade Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('cidade', 'Cidade:') !!}
        {!! Form::text('cidade', $endereco->cidade, ['class' => 'form-control']) !!}
    </div>

    <!-- Uf Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('uf', 'UF:') !!}
        {!! Form::text('uf', $endereco->uf, ['class' => 'form-control']) !!}
    </div>

    <!-- Complemento Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('complemento', 'Complemento:') !!}
        {!! Form::text('complemento', $endereco->complemento, ['class' => 'form-control']) !!}
    </div>
</div>
