<!-- Logradouro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logradouro', 'Logradouro:') !!}
    {!! Form::text('logradouro', $endereco->logradouro, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Numero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero', 'Numero:') !!}
    {!! Form::number('numero', $endereco->numero, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Cep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cep', 'Cep:') !!}
    {!! Form::text('cep', $endereco->cep, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Bairro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bairro', 'Bairro:') !!}
    {!! Form::text('bairro', $endereco->bairro, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Cidade Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cidade', 'Cidade:') !!}
    {!! Form::text('cidade', $endereco->cidade, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Uf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uf', 'UF:') !!}
    {!! Form::text('uf', $endereco->uf, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>

<!-- Complemento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('complemento', 'Complemento:') !!}
    {!! Form::text('complemento', $endereco->complemento, ['class' => 'form-control','disabled'=>'disabled']) !!}
</div>
    