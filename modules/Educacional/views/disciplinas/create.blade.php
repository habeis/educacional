@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'disciplinas.store']) !!}

        @include('disciplinas.fields')

        {!! Form::close() !!}
    </div>
@endsection