<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    <p>{!! Form::text('id', $disciplina->id , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{!! Form::text('nome', $disciplina->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Nome Abreviado Field -->
<div class="form-group">
    {!! Form::label('nome_abreviado', 'Nome Abreviado:') !!}
    <p>{!! Form::text('nome_abreviado', $disciplina->nome_abreviado, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Submit Field -->
<div class="form-group">
    <a href="{!! route('disciplinas.index') !!}" class="btn btn-primary">Cancelar</a>
</div>
