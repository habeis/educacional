<table class="table table-responsive">
    <thead>
        <th>Código</th>
        <th>Nome</th>
        <th>Nome Abreviado</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($disciplinas as $disciplina)
        <tr>
            <td>{!! $disciplina->id !!}</td>
            <td>{!! $disciplina->nome !!}</td>
            <td>{!! $disciplina->nome_abreviado !!}</td>
            <td>
                {!! Form::open(['route' => ['disciplinas.destroy', $disciplina->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('disciplinas.show', [$disciplina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('disciplinas.edit', [$disciplina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>