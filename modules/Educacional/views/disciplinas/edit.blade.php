@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::model($disciplina, ['route' => ['disciplinas.update', $disciplina->id], 'method' => 'patch']) !!}

        @include('disciplinas.fields')

        {!! Form::close() !!}
    </div>
@endsection