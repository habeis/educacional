<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<!-- Nome Abreviado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome_abreviado', 'Nome Abreviado:') !!}
    {!! Form::text('nome_abreviado', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('disciplinas.index') !!}" class="btn btn-primary">Cancelar</a>
</div>
