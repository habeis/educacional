@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('disciplinas.create') !!}">{{ trans("forms.button_create") }}</a>
    </a>

    <div class="clearfix"></div>

    @if($disciplinas->isEmpty())
        <div class="well text-center">No Disciplinas found.</div>
    @else
        @include('disciplinas.table')
    @endif
@endsection