<table class="table table-striped responsive-utilities jambo_table bulk_action">
    <thead>
        <tr class="headings">
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th class="column-title">Nome</th>
            <th class="column-title">Cnpj</th>
            <th class="column-title">Razao Social</th>
            <th class="column-title">Telefone</th> 
            <th class="column-title no-link last" colspan="3">
                <span class="nobr">Ação</span>
            </th>
            <th class="bulk-actions" colspan="6">
              <a class="antoo" style="color:#fff; font-weight:500;">Quantidade Coligadas ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
            </th>
        </th>
    </thead>
    <tbody>
    @foreach($coligadas as $coligada)
        <tr class="even pointer">
            <td class="a-center ">
                <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" ">{!! $coligada->nome !!}</td>
            <td class=" ">{!! $coligada->cnpj !!}</td>
            <td class=" ">{!! $coligada->razao_social !!}</td>
            <td class=" ">{!! $coligada->telefone !!}</td> 

            <td>
                {!! Form::open(['route' => ['coligadas.destroy', $coligada->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('coligadas.show', [$coligada->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('coligadas.edit', [$coligada->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Você deseja realmente deletar?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
