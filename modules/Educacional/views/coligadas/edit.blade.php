@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($coligada, ['route' => ['coligadas.update', $coligada->id], 'method' => 'patch']) !!}

        @include('coligadas.fields')

        {!! Form::close() !!}
    </div>

@endsection
