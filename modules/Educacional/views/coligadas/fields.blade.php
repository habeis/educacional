<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
</div>

<!-- Cnpj Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cnpj', 'Cnpj:') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
</div>

<!-- Razao Social Field -->
<div class="form-group col-sm-6">
    {!! Form::label('razao_social', 'Razao Social:') !!}
    {!! Form::text('razao_social', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
</div>

<!-- Telefone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefone', 'Telefone:') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
</div>

@include('enderecos.fields')

<div class="form-group col-sm-12">
    <div class="panel panel-default">

        <div class="panel-heading">Tipo de Curso</div>

        {!! Form::hidden('id', null,['id' => 'id']) !!}
        <div class="panel">

            <div class="form-group col-sm-12 ">
                {!! Form::label('tipocurso', 'Tipo de Curso:') !!}

                {!! Form::select('tipocurso',$tipocurso,null,['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}
            </div>

            <div class="form-group col-sm-12 ">
                {!! Form::button($title="Adicionar",$atributos = ['id' => 'registro', 'class' => 'btn btn-primary'], $secure = null ) !!}
            </div>

            <table class="table" id="tableTipoCurso">
                <thead>
                <th>Código</th>
                <th>Descrição</th>
                <th>#</th>
                </thead>
                <tbody>
                @if(!empty($coligada))
                    @foreach($coligada->tipoCursos as $p)
                        <tr>
                            <td class="row" headers="Código">
                                {!! Form::hidden('tipoCursos[]', $p->id) !!}
                                {!!  $p->id !!}
                            </td>
                            <td class="row" headers="Descrição">
                                {!! $p->descricao !!}
                            </td>
                            <td>
                                <button class="btn btn-danger btn-xs" onclick="RemoveTableRow(this,{{ $p->id}})"
                                        type="button">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
    </div>

</div>

<!-- Obs Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('obs', 'Obs:') !!}
    {!! Form::textarea('obs', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('coligadas.index') !!}" class="btn btn-primary">Cancel</a>
</div>

@section('post-script')
    <script type="text/javascript">
        $("#registro").click(function (event) {
            var select = $("#tipocurso option:selected").val();
            var coligadaId = $("#id").val();
            var token = $("input[type=hidden][name=_token]").val();
            var route = 'http://localhost:8000/post-tipoCurso';
            var newRow = $("<tr>");
            var cols = "";

            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {tipoCursoId: select, idColigada: coligadaId},

                success: function (data) {

                    cols += '<td>' + data['id'] + '</td>';
                    cols += '<td>' + data['descricao'] + '</td>';
                    cols += '<td><button class="btn btn-danger btn-xs" onclick="RemoveTableRow(this)" type="button"><i class="glyphicon glyphicon-trash"></i></button><td>';
                    cols += '<td><input name="tipoCursos[]" value="' + data['id'] + '" type="hidden"/><td>';
                    newRow.append(cols);

                    $("#tableTipoCurso").append(newRow);
                }
            });
        });

        RemoveTableRow = function (handler, id) {

            var tr = $(handler).closest('tr');
            tr.fadeOut(400, function () {
                tr.remove();
            });

            return false;
        };
    </script>

@endsection
