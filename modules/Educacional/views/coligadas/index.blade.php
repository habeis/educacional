@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('coligadas.create') !!}">{{ trans("forms.button_create") }}
    </a>
    <div class="clearfix"></div>

    @if($coligadas->isEmpty())
        <div class="well text-center">Não foi encontrada nenhuma Coligada(s).</div>
    @else
        @include('coligadas.table')
    @endif

@endsection