@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'coligadas.store']) !!}

        @include('coligadas.fields')

        {!! Form::close() !!}
    </div>
@endsection