 <!-- Nome Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('nome', 'Nome:') !!}
        {!! Form::text('nome', $coligada->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!} 
    </div>

    <!-- Cnpj Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('cnpj', 'Cnpj:') !!}
        {!! Form::text('cnpj', $coligada->cnpj, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
    </div>

    <!-- Razao Social Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('razao_social', 'Razao Social:') !!}
        {!! Form::text('razao_social', $coligada->razao_social, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
    </div>

    <!-- Telefone Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('telefone', 'Telefone:') !!}
        {!! Form::text('telefone', $coligada->telefone, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
    </div>

    @include('enderecos.show_fields')

    <!-- Obs Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('obs', 'Obs:') !!}
        {!! Form::textarea('obs', $coligada->obs, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
    </div>
      
<!-- Submit Field -->
<div class="form-group col-sm-12">
     <a href="{!! route('coligadas.index') !!}" class="btn btn-primary">Cancel</a>
</div>
