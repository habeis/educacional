@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit TipoDisciplina</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($tipoDisciplina, ['route' => ['tipoDisciplinas.update', $tipoDisciplina->id], 'method' => 'patch']) !!}

            @include('tipoDisciplinas.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection