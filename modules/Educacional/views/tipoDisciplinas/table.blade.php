<table class="table table-responsive">
    <thead>
        <th>Descricao</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($tipoDisciplinas as $tipoDisciplina)
        <tr>
            <td>{!! $tipoDisciplina->descricao !!}</td>
            <td>
                {!! Form::open(['route' => ['tipoDisciplinas.destroy', $tipoDisciplina->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tipoDisciplinas.show', [$tipoDisciplina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tipoDisciplinas.edit', [$tipoDisciplina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>