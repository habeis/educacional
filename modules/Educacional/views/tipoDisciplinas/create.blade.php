@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'tipoDisciplinas.store']) !!}

        @include('tipoDisciplinas.fields')

        {!! Form::close() !!}
    </div>

@endsection