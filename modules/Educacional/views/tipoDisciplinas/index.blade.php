@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('tipoDisciplinas.create') !!}">
        {{ trans("forms.button_create") }}
    </a>

    <div class="clearfix"></div>

    @if($tipoDisciplinas->isEmpty())
        <div class="well text-center">Nenhum Tipo de disciplinas encontrado.</div>
        @else
        @include('tipoDisciplinas.table')
        @endif

        </div>
@endsection