@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::model($periodo, ['route' => ['periodos.update', $periodo->id], 'method' => 'patch']) !!}

        @include('periodos.fields')

        {!! Form::close() !!}
    </div>
@endsection