@extends('layouts.page')

@section('content-center')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'periodos.store']) !!}

        @include('periodos.fields')

        {!! Form::close() !!}
    </div>
@endsection