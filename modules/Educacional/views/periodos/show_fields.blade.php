<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $periodo->id !!}</p>
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    <p>{!! $periodo->descricao !!}</p>
</div>

<!-- Numero Periodo Field -->
<div class="form-group">
    {!! Form::label('numero_periodo', 'Numero Periodo:') !!}
    <p>{!! $periodo->numero_periodo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $periodo->created_at !!}</p>
</div>

<!-- Modified At Field -->
<div class="form-group">
    {!! Form::label('modified_at', 'Modified At:') !!}
    <p>{!! $periodo->modified_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $periodo->deleted_at !!}</p>
</div>

