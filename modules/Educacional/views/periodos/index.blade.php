@extends('layouts.page')

@section('content-center')
    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('periodos.create') !!}">{{ trans("forms.button_create") }}</a>
    </a>

    <div class="clearfix"></div>

    @if($periodos->isEmpty())
        <div class="well text-center">Nenhum Periodo encontrado.</div>
    @else
        @include('periodos.table')
    @endif

@endsection