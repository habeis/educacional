<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    {!! Form::text('id', $anoSemestre->id, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Ano Semestre Field -->
<div class="form-group">
    {!! Form::label('ano_semestre', 'Ano Semestre:') !!}
    {!! Form::text('ano_semestre', $anoSemestre->ano_semestre, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Data Inicio Field -->
<div class="form-group">
    {!! Form::label('data_inicio', 'Data Inicio:') !!}
    {!! Form::text('data_inicio', $anoSemestre->data_inicio, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Data Fim Field -->
<div class="form-group">
    {!! Form::label('data_fim', 'Data Fim:') !!}
    {!! Form::text('data_fim', $anoSemestre->data_fim, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Atual Field -->
<div class="form-group">
    {!! Form::label('atual', 'Atual:') !!}
    {!! Form::text('atual', $anoSemestre->atual, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Coligada Tipo Curso Id Field -->
<div class="form-group">
    {!! Form::label('coligada_tipo_curso_id', 'Coligada Tipo Curso Id:') !!}
    {!! Form::text('coligada_tipo_curso_id', $anoSemestre->coligada_tipo_curso_id, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Obs Field -->
<div class="form-group">
    {!! Form::label('obs', 'Obs:') !!}
    {!! Form::text('obs', $anoSemestre->obs, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('anoSemestres.index') !!}" class="btn btn-primary">Cancel</a>
</div>