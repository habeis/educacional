<!-- Ano Semestre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ano_semestre', 'Ano Semestre:') !!}
    {!! Form::text('ano_semestre', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_inicio', 'Data Inicio:') !!}
    {!! Form::date('data_inicio', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Fim Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_fim', 'Data Fim:') !!}
    {!! Form::date('data_fim', null, ['class' => 'form-control']) !!}
</div>
<!-- Modalidade Tipo de Cursos Graduação-Ténico ou pós-Graduação Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coligada_tipo_curso_id', 'Tipo de Cursos:') !!}
    {!! Form::select('coligada_tipo_curso_id', array_pluck($coligadaTipoCurso,'descricao','id'), null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-12']) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('atual', 'Atual:') !!}
    {!!
        Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}
    {!! Form::check('atual', true, isset($anoSemestre->atual) && $anoSemestre->atual == true ? 1 : 0) !!}
</div>

<!-- Obs Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('obs', 'Obs:') !!}
    {!! Form::textarea('obs', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('anoSemestres.index') !!}" class="btn btn-primary">Cancel</a>
</div>
