<table class="table table-responsive">
    <thead>
        <th>Ano Semestre</th>
        <th>Data Inicio</th>
        <th>Data Fim</th>
        <th>Atual</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($anoSemestres as $anoSemestre)
        <tr>
            <td>{!! $anoSemestre->ano_semestre !!}</td>
            <td>{!! $anoSemestre->data_inicio !!}</td>
            <td>{!! $anoSemestre->data_fim !!}</td>
            <td>{!! $anoSemestre->atual !!}</td>
            <td>
                {!! Form::open(['route' => ['anoSemestres.destroy', $anoSemestre->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('anoSemestres.show', [$anoSemestre->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('anoSemestres.edit', [$anoSemestre->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>