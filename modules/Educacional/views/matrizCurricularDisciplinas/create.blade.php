@extends('layouts.page')

@section('content-center')

    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'matrizCurricularDisciplinas.store']) !!}

        @include('matrizCurricularDisciplinas.fields')
        <h2>{!! Form::hidden('cursoId', $curso->id) !!} </h2>
        {!! Form::close() !!}
    </div>
@endsection