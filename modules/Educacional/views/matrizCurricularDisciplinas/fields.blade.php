{!! Form::hidden('matriz_curricular_id', $matrizCurricular->id) !!}

<!-- Matriz Curricular Curso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('curso', 'Nome Curso:') !!}
    {!! Form::text('curso', $matrizCurricular->curso->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Matriz Curricular Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('versao', 'Matriz Curricular:') !!}
    {!! Form::number('versao', $matrizCurricular->versao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Disciplina Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('disciplina_id', 'Disciplina:') !!}
    {!! Form::select('disciplina_id',$disciplina, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}
</div>
<!-- Codigo Disciplina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo_disciplina', 'Código da Disciplina:') !!}
    {!! Form::text('codigo_disciplina', null, ['class' => 'form-control']) !!}
</div>

<!-- Periodo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('periodo_id', 'Periodo:') !!}
    {!! Form::select('periodo_id',$periodos, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}
</div>

<!-- Disciplina Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('credito_valor_id', 'Valor do credito:') !!}
    {!! Form::select('credito_valor_id',$creditoValor, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}
</div>

<!-- Credito Field -->
<div class="form-group col-sm-6">
    {!! Form::label('credito', 'Número de créditos') !!}
    {!! Form::number('credito', null, ['class' => 'form-control']) !!}
</div>

<!-- Carga Horaria Field -->
<div class="form-group col-sm-6">
    {!! Form::label('carga_horaria', 'Carga Horaria:') !!}
    {!! Form::number('carga_horaria', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Disciplina Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_disciplina_id', 'Tipo Disciplina:') !!}
    {!! Form::select('tipo_disciplina_id',$tipoDisciplinas, null, ['placeholder' => 'Seleciona','class' => 'form-control col-sm-6']) !!}
</div>

<!-- Pre Requisito Field -->
<div class="form-group col-sm-6">

    {!! Form::label('pre_requisito', 'Pré-Requisito:') !!}
    {!!
        Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}

    {!! Form::check('pre_requisito', true, isset($matrizCurricular->atual) && $matrizCurricular->atual == true ? 1 : 0) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('matrizCurricularDisciplinas.index',['matriz_curricular'=>$matrizCurricular->id, 'cursoId' => $curso->id]) !!}" class="btn btn-primary">Cancel</a>
</div>
