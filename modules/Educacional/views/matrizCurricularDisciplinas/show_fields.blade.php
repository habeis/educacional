<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    <p>{!! Form::text('id', $matrizCurricularDisciplina->id, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Matriz Curricular Id Field -->
<div class="form-group">
    {!! Form::label('versao', 'Versão da Matriz Curricular:') !!}
    <p>{!! Form::text('versao', $matrizCurricularDisciplina->matrizCurricular->versao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Disciplina Id Field -->
<div class="form-group">
    {!! Form::label('disciplina', 'Disciplina:') !!}
    <p>{!! Form::text('disciplina', $matrizCurricularDisciplina->disciplina->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Codigo Disciplina Field -->
<div class="form-group">
    {!! Form::label('codigo_disciplina', 'Codigo Disciplina:') !!}
    <p>{!! Form::text('codigo_disciplina', $matrizCurricularDisciplina->codigo_disciplina , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Nome Disciplina Field -->
<div class="form-group">
    {!! Form::label('nome_disciplina', 'Nome Disciplina:') !!}
    <p>{!! Form::text('nome_disciplina', $matrizCurricularDisciplina->nome_disciplina , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Periodo Id Field -->
<div class="form-group">
    {!! Form::label('periodo_id', 'Periodo:') !!}
    <p>{!! Form::text('periodo_id', $matrizCurricularDisciplina->periodo->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Credito Field -->
<div class="form-group">
    {!! Form::label('credito', 'Credito:') !!}
    <p>{!! Form::text('credito', $matrizCurricularDisciplina->credito , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Credito Valor Id Field -->
<div class="form-group">
    {!! Form::label('credito_valor_id', 'Credito Valor:') !!}
    <p>{!! Form::text('credito_valor_id', $matrizCurricularDisciplina->creditoValor->valor, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Pre Requisito Field -->
<div class="form-group">
    {!! Form::label('pre_requisito', 'Pre Requisito:') !!}
    <p>{!! Form::text('pre_requisito', $matrizCurricularDisciplina->pre_requisito, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Carga Horaria Field -->
<div class="form-group">
    {!! Form::label('carga_horaria', 'Carga Horaria:') !!}
    <p>{!! Form::text('pre_requisito', $matrizCurricularDisciplina->carga_horaria, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Tipo Disciplina Id Field -->
<div class="form-group">
    {!! Form::label('tipo_disciplina_id', 'Tipo Disciplina:') !!}
    <p>{!! Form::text('pre_requisito', $matrizCurricularDisciplina->tipoDisciplina->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Submit Field -->
<div class="form-group">
    <a href="{!! route('matrizCurricularDisciplinas.index',['matriz_curricular'=>$matrizCurricular, 'cursoId' => $curso->id]) !!}" class="btn btn-primary">Cancelar</a>
</div>
