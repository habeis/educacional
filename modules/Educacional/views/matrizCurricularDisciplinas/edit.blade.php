@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($matrizCurricularDisciplina, ['route' => ['matrizCurricularDisciplinas.update', $matrizCurricularDisciplina->id, 'matriz_curricular' => $matrizCurricular->id, 'cursoId' => $curso->id], 'method' => 'patch']) !!}

        @include('matrizCurricularDisciplinas.fields')

        {!! Form::close() !!}
    </div>
@endsection