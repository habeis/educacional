@extends('layouts.page')

@section('content-center')
    @include('flash::message')


    <div class="form-group col-sm-12">
        <a class="btn btn-primary pull-right" style="margin-top: 25px"
           href="{!! route('matrizCurricularDisciplinas.create',['matriz_curricular'=>$matrizCurricular->id, 'cursoId' => $curso->id]) !!}">{{ trans("forms.button_create") }}
        </a>
        <a class="btn btn-primary pull-right" style="margin-top: 25px " title="Listar de Matrizes Curriculares "
           href="{!! route('matrizCurriculars.index', ['cursoId' => $curso->id]) !!}">
            Listar Matrizes
        </a>
        <a class="btn btn-primary pull-right" style="margin-top: 25px " title="Listar de Matrizes Curriculares "
           href="{!! route('cursos.index') !!}">
            Cursos
        </a>
        <h2>{!! Form::label('curso_nome', 'Curso:') !!} {{ $curso->nome }}</h2>
        <h2>{!! Form::label('versao', 'Versão:') !!} {{$matrizCurricular->versao}} </h2>
    </div>

    <div class="clearfix"></div>

    @if($matrizCurricularDisciplinas->isEmpty())
        <div class="well text-center">Nenhuma Disciplina Encontrada.</div>
    @else
        @include('matrizCurricularDisciplinas.table')
    @endif

@endsection