
<table class="table table-responsive">
    <thead>
        <th>Disciplina</th>
        <th>Periodo</th>
        <th>Crédito</th>
        <th>Valor Crédito</th>
        <th>Carga Horaria</th>
        <th>Tipo Disciplina</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($matrizCurricularDisciplinas as $matrizCurricularDisciplina)
        <tr>
            <td>{!! $matrizCurricularDisciplina->disciplina->nome!!}</td>
            <td>{!! $matrizCurricularDisciplina->periodo_id !!}</td>
            <td>{!! $matrizCurricularDisciplina->credito !!}</td>
            <td>{!! $matrizCurricularDisciplina->creditoValor->valor!!}</td>
            <td>{!! $matrizCurricularDisciplina->carga_horaria !!}</td>
            <td>{!! $matrizCurricularDisciplina->tipoDisciplina->descricao !!}</td>
            <td>
                {!! Form::open(['route' => ['matrizCurricularDisciplinas.destroy', $matrizCurricularDisciplina->id,'matriz_curricular' => $matrizCurricularDisciplina->matriz_curricular_id, 'cursoId' => $curso->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('matrizCurricularDisciplinas.show', [$matrizCurricularDisciplina->id,'matriz_curricular'=>$matrizCurricular->id, 'cursoId' => $curso->id]) !!}" title="Visualizar Disciplina" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('matrizCurricularDisciplinas.edit', [$matrizCurricularDisciplina->id,'matriz_curricular'=>$matrizCurricular->id, 'cursoId' => $curso->id]) !!}" title="Editar Disciplina" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Deseja realmente remover?')","title"=>"Remover Disciplina"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>