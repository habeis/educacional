<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::text('id', $turno->id, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::text('descricao', $turno->descricao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}
</div>

<!-- Ativo Field -->
<div class="form-group">
    {!! Form::label('ativo', 'Ativo:') !!}
    {!! Form::checkbox('ativo','value', $turno->ativo, ['class' => 'flat','disabled'=>'disabled']) !!}
 </div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('turnos.index') !!}" class="btn btn-primary">Cancel</a>
</div>
