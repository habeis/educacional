@extends('layouts.app')

@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Turno</h3>
                <small>Acadêmico -> Cadastro -> Turno</small>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="container col-md-5 col-sm-5 col-xs-5">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>
                            Visualizar Turno
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </li>
                            <li>
                                <a class="close-link" href="{!! route('home') !!}">
                                    <i class="fa fa-close"/></i>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @include('turnos.show_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
