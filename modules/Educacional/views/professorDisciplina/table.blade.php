<table class="table table-responsive table-hover table-striped" id="professores-table">
    <thead>
    <th>Professor</th>
    <th>Disciplina</th>
    <th>Turma</th>
    <th>Peŕiodo</th>
    <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($turmaDisciplinas as $turmaDisciplina)
        @foreach($turmaDisciplina->professorDisciplina as $professorDisciplina)
            <tr>
                <td>{!! $professorDisciplina->user->pessoa->nome !!}</td>
                <td>{!! $turmaDisciplina->nome_disciplina !!}</td>
                <td>{!! $turmaDisciplina->turma->nome !!}</td>
                <td>{!! $turmaDisciplina->turma->periodo_id !!}°</td>

            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
