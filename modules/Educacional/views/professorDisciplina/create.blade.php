@extends('layouts.page')

@section('content-center')

    @include('flash::message')
    @include('core-templates::common.errors')
    <div class="row">

        {!! Form::open(['route' => 'professor-disciplina.store']) !!}

        {!! Form::hidden('tipoCursoId',$parametros['tipoCursoId'])  !!}
        {!! Form::hidden('semestreLetivoId',$parametros['semestreLetivoId'])  !!}
        {!! Form::hidden('cursoId',$parametros['cursoId'])  !!}


        <div class="form-group col-sm-12">

            {!! Form::label('professor', 'Professor') !!}
            <select name="professor" class="form-control col-sm-6" required="required">
                <option selected="selected" value="">Seleciona um Professor</option>
                @foreach ($professores as $professor)
                    <option value="{!! $professor->id !!}">{!!$professor->user->pessoa->nome !!}</option>
                @endforeach
            </select>
        </div>

        @foreach ($turmas  as $turma)
            <div class="form-group col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Nome da Turma: {!! Form::label('turma', $turma->nome) !!}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table  table-responsive table-hover table-striped">
                            <thead>
                            <th width="20px">#</th>
                            <th colspan="3">Disciplina</th>
                            </thead>
                            <tbody>
                            @foreach ($turma->turmaDisciplina as $turmaDisciplina)

                                <tr class="">
                                    <td>
                                        {!! Form::checkbox('turmasDisciplinas[]', $turmaDisciplina->id,null, ['class' => 'icheckbox_flat-green flat']) !!}
                                    </td>
                                    <td>
                                        {!! Form::label('turma_discipolina_nome', $turmaDisciplina->nome_disciplina) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="form-group col-sm-12">
            <div class="form-group col-sm-12">
                {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
