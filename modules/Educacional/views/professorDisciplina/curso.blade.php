@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    @include('core-templates::common.errors')
    <div class="row">

        {!! Form::open(['id'=>'form1','route' => 'professor-disciplina.index', 'method' => 'GET', 'data-parsley-validate','class'=>'form-horizontal form-label-left']) !!}

        <div class="form-group col-sm-12">
            {!! Html::decode(Form::label('tipoCursoId','Tipo de Curso <span class="required">*</span>')) !!}
            {!! Form::select('tipoCursoId',array_pluck($coligada->tipoCursos,'descricao','id'), null, ['placeholder' => 'Seleciona um Tipo de Curso', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('semestreLetivoId', 'Semestre Letivo:') !!}
            {!! Form::select('semestreLetivoId',[], null, ['placeholder' => 'Seleciona um Semestre Letivo','class' => 'form-control col-sm-6', 'disabled'=>'disabled', 'required' => 'required']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('cursoId', 'Curso:') !!}
            {!! Form::select('cursoId',[], null, ['placeholder' => 'Seleciona um Curso','class' => 'form-control col-sm-6', 'disabled'=>'disabled', 'required' => 'required']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::submit('Avançar', ['class' => 'btn btn-success']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection

@section('post-script')
    <script type="text/javascript">
        // Consulta os Cursos do Tipo de Curso
        $("#tipoCursoId").on("change", function (event) {
            desativarSemestre();

            var tipoCursoId = event.target.value;
            // Só efetua a consulta se o id for maior que 0
            if (tipoCursoId > 0) {
                ativaSemestre();
                //Busca o curso e coloca no option todos os cursos
                $.get('/curso-tipo-curso?tipo_curso_id=' + tipoCursoId, function (data) {
                    $.each(data, function (index, cursoObjt) {
                        $("#cursoId").append('<option value="' + cursoObjt.id + '"> ' + cursoObjt.nome + ' </option>');
                    });
                });

                // Lista os semestre e coloca no option
                $.get('/ano-semestre-tipo-curso?tipo_curso_id=' + tipoCursoId, function (data) {
                    console.log(data);
                    $.each(data, function (index, cursoObjt) {
                        $("#semestreLetivoId").append('<option value="' + cursoObjt.id + '"> ' + cursoObjt.ano_semestre + '  </option>');
                    });
                });
            }
        });

        $("#semestreLetivoId").on("change", function (event) {

            desativarCurso();

            if (event.target.value > 0) {
                // Ativa o curso e seleciona o primeiro
                ativaCurso();
            }

        });
        $("#cursoId").on("change", function (event) {

            var token = $("input[type=hidden][name=_token]").val();

            var cursoId = event.target.value;

            var anoSemestreId = $("#semestreLetivoId option:selected").val();
            ativarTipoAluno();
        });

        function ativarTipoAluno() {
            $("#tipoAluno").prop("disabled", false);
        }
        function limparSemestre() {
            $("#semestreLetivoId").empty();
            $("#semestreLetivoId").append('<option value="">Seleciona um Semestre Letivo</option>');
            limparCurso();
        }
        function desativarSemestre() {
            $('#semestreLetivoId option:first').prop('selected', true);
            $("#semestreLetivoId").prop("disabled", true);
            desativarCurso();
            limparSemestre();
        }
        function ativaSemestre() {
            $('#semestreLetivoId  option:first').prop('selected', true);
            $("#semestreLetivoId").prop("disabled", false);
        }
        function ativaCurso() {
            $('#cursoId  option:first').prop('selected', true);
            $("#cursoId").prop("disabled", false);
        }
        function desativarCurso() {
            $('#cursoId option:first').prop('selected', true);
            $("#cursoId").prop("disabled", true);
        }
        function limparCurso() {
            $("#cursoId").empty();
            $("#cursoId").append('<option value="">Seleciona um Curso</option>');
        }
        $(document).ready(function () {
            $.listen('parsley:field:validate', function () {
                validateFront();
            });
            $('#form1.btn').on('click', function () {
                $('#form1').parsley().validate();
                validateFront();
            });
            var validateFront = function () {
                if (true === $('#demo-form2').parsley().isValid()) {
                    $('.bs-callout-info').removeClass('hidden');
                    $('.bs-callout-warning').addClass('hidden');
                } else {
                    $('.bs-callout-info').addClass('hidden');
                    $('.bs-callout-warning').removeClass('hidden');
                }
            };
        });
    </script>
@endsection
