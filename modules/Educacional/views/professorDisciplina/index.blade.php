@extends('layouts.page')

@section('content-center')

    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('professor-disciplina.create',$parametros)!!}">{{ trans("forms.button_create") }}
    </a>

    <div class="clearfix"></div>

    @include('core-templates::common.errors')

    @include('flash::message')

    <div class="clearfix"></div>

    @include('professorDisciplina.table')


@endsection
