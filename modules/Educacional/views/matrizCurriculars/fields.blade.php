<!-- Versao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('versao', 'Versao:') !!}
    {!! Form::number('versao', null, ['class' => 'form-control']) !!}
</div>

<!-- Hora Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hora_total', 'Hora Total:') !!}
    {!! Form::number('hora_total', null, ['class' => 'form-control']) !!}
</div>

<!-- Horas Complementar Field -->
<div class="form-group col-sm-6">
    {!! Form::label('horas_complementar', 'Horas Complementar:') !!}
    {!! Form::number('horas_complementar', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('atual', 'Atual:') !!}
    {!!
        Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}

    {!! Form::check('atual', true, isset($matrizCurricular->atual) && $matrizCurricular->atual == true ? 1 : 0) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('matrizCurriculars.index',['cursoId' => $curso->id]) !!}" class="btn btn-primary">Cancelar</a>
</div>
