@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('matrizCurriculars.create',['cursoId' => $curso->id]) !!}">
        {{ trans("forms.button_create") }}
    </a>
    <a class="btn btn-primary pull-right" style="margin-top: 25px " title="Listar de Matrizes Curriculares "
       href="{!! route('cursos.index') !!}">
        Cursos
    </a>
    <div class="clearfix"></div>

    @if($matrizCurriculars->isEmpty())
        <div class="well text-center">No MatrizCurriculars found.</div>
    @else
        @include('matrizCurriculars.table')
    @endif
@endsection