@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'matrizCurriculars.store']) !!}

        <div class="form-group col-sm-12">

            <h2>{!! Form::hidden('cursoId', $curso->id) !!} </h2>
            <h2>{!! Form::hidden('curso_id', $curso->id) !!} </h2>
            <h2>{!! Form::label('curso_nome', 'Curso:') !!} {{ $curso->nome }}</h2>
        </div>

        @include('matrizCurriculars.fields')

        {!! Form::close() !!}
    </div>
@endsection