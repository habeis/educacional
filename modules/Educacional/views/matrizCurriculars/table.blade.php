<table class="table table-responsive">
    <thead>
        <th>Curso</th>
        <th>Versao</th>
        <th>Hora Total</th>
        <th>Atual</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($matrizCurriculars as $matrizCurricular)
        <tr>
            <td>{!! $matrizCurricular->curso->nome!!}</td>
            <td>{!! $matrizCurricular->versao !!}</td>
            <td>{!! $matrizCurricular->hora_total !!}</td>
            <td>{!! $matrizCurricular->atual !!}</td>
            <td>
                {!! Form::open(['route' => ['matrizCurriculars.destroy', $matrizCurricular->id,'cursoId' => $curso->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('matrizCurriculars.show', [$matrizCurricular->id, 'cursoId' => $curso->id]) !!}" title="Visualizar Curso" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('matrizCurriculars.edit', [$matrizCurricular->id,'cursoId' => $curso->id]) !!}" title="Editar Curso" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('matrizCurricularDisciplinas.index', ['matriz_curricular'=>$matrizCurricular->id, 'cursoId' => $curso->id]) !!}" title="Listar Disciplinas" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-th"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')","title"=> "Remover Matriz Curricular"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>