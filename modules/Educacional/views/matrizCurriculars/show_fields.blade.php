<!-- Versao Field -->
<div class="form-group">
    {!! Form::label('versao', 'Versao:') !!}
    <p>{!! Form::text('versao', $matrizCurricular->versao, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Hora Total Field -->
<div class="form-group">
    {!! Form::label('hora_total', 'Hora Total:') !!}
    <p>{!! Form::text('hora_total', $matrizCurricular->hora_total, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Horas Complementar Field -->
<div class="form-group">
    {!! Form::label('horas_complementar', 'Horas Complementar:') !!}
    <p>{!! Form::text('horas_complementar', $matrizCurricular->horas_complementar , ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Curso Id Field -->
<div class="form-group">
    {!! Form::label('curso_id', 'Curso:') !!}
    <p>{!! Form::text('curso_id', $matrizCurricular->curso->nome, ['class' => 'form-control col-md-7 col-xs-12','disabled'=>'disabled']) !!}</p>
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
        {!! Form::label('atual', 'Atual:') !!}
        {!!
            Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
                return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
            })
        !!}

        {!! Form::check('atual', true, isset($matrizCurricular->atual) && $matrizCurricular->atual == true ? 1 : 0) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('matrizCurriculars.index', ['cursoId' => $curso->id]) !!}" class="btn btn-primary">Cancelar</a>
</div>
