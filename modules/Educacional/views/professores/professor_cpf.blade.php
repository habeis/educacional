@extends('layouts.page')

@section('content-center')

    @include('flash::message')

    <a href="{!! route('professores.index') !!}" class="btn btn-primary">Listar de Professor</a>

    @include('core-templates::common.errors')

    <div class="clearfix"></div>
    <div class="row">

        {!! Form::open(['route' => 'professores.create', 'method' => 'get','data-parsley-validate','class'=>'form-horizontal form-label-left']) !!}

        <div class="form-group col-md-6  ">
            {!! Form::label('cpf', 'CPF: ') !!}
            {!! Form::text('cpf', null, ['class'=> 'form-control', 'data-inputmask'=> "'mask': '999.999.999-99'", 'placeholder'=> 'CPF', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::submit('Avançar', ['class' => 'btn btn-success']) !!}
        </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('post-script')
    <!-- Mask Javascript -->
    <!-- Javascript -->
    <script src="{{ asset('js/input_mask/jquery.inputmask.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(":input").inputmask();
        });

    </script>
@endsection
