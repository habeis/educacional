<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $professor->id !!}</p>
</div>

<!-- Graduacao Field -->
<div class="form-group">
    {!! Form::label('graduacao', 'Graduacao:') !!}
    <p>{!! $professor->graduacao !!}</p>
</div>

<!-- Ativo Field -->
<div class="form-group">
    {!! Form::label('ativo', 'Ativo:') !!}
    <p>{!! $professor->ativo !!}</p>
</div>

<!-- Substituto Field -->
<div class="form-group">
    {!! Form::label('substituto', 'Substituto:') !!}
    <p>{!! $professor->substituto !!}</p>
</div>

<!-- Visitante Field -->
<div class="form-group">
    {!! Form::label('visitante', 'Visitante:') !!}
    <p>{!! $professor->visitante !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $professor->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $professor->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $professor->deleted_at !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $professor->user_id !!}</p>
</div>

