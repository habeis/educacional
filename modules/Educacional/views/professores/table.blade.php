<table class="table table-responsive" id="professores-table">
    <thead>
        <th>Nome</th>
        <th>email</th>
        <th>Ativo</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($professores as $professor)
        <tr>
            <td>{!! $professor->user->pessoa->nome !!}</td>
            <td>{!! $professor->user->pessoa->email !!}</td>
            <td>{!! $professor->ativo !!}</td>
            <td>
                {!! Form::open(['route' => ['professores.destroy', $professor->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('professores.show', [$professor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('professores.edit', [$professor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
