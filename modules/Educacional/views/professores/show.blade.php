@extends('layouts.page')

@section('content-center')

    <div class="form-group">
        <a href="{!! route('professores.index') !!}" class="btn btn-default">Listar Professores</a>
    </div>
    <div id="wizard" class="form_wizard wizard_horizontal">
        @include('professores.step')
        <div class="stepContainer">

            <div id="step-1" class="wizard_content" style="display: block;">

                {!! Form::hidden('cpf', $pessoa->cpf) !!}
                @include('pessoas.fields')
            </div>
            <div id="step-2" class="wizard_content" style="display: none;">
                @include('professores.fields')
            </div>
            <div id="step-3" class="wizard_content" style="display: none;">

                {!! Form::hidden('endereco_id', $endereco->id) !!}
                @include('enderecos.fields')

            </div>

        </div>
    </div>
@endsection

@section('post-script')

    <script src="{{ asset('js/wizard/jquery.smartWizard.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard
            $('#wizard').smartWizard({
                includeFinishButton: false,

            });

            function onFinishCallback() {
                $('#wizard').smartWizard(
                        'showMessage', 'Finish Clicked');
                //alert('Finish Clicked');
            }
        });

        $(document).ready(function () {
            // Smart Wizard
            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });

        });
    </script>


@endsection
