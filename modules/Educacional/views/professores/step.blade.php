<ul class="wizard_steps anchor">
    <li>
        <a href="#step-1" class="selected" isdone="1" rel="1">
            <span class="step_no">1</span>
            <span class="step_descr">
                Step 1<br>
                <small>Dados Pessoais</small>
            </span>
        </a>
    </li>
    <li>
        <a href="#step-2" class="disabled" isdone="0" rel="2">
            <span class="step_no">2</span>
            <span class="step_descr">
                Step 2<br>
                <small>Informaçao Escolar</small>
            </span>
        </a>
    </li>
    <li>
        <a href="#step-3" class="disabled" isdone="0" rel="3">
            <span class="step_no">3</span>
            <span class="step_descr">
                Step 3<br>
                <small>Endereço</small>
            </span>
        </a>
    </li>

</ul>