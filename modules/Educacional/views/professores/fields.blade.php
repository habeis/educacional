<!-- Substituto Field -->
<div class="form-group col-sm-2">
    {!! Form::label('substituto', 'Substituto:') !!}
    {!!
        Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}

    {!! Form::check('substituto', true, 0,['class' => 'form-control']) !!}

</div>

<!-- Visitante Field -->
<div class="form-group col-sm-2">
    {!! Form::label('visitante', 'Visitante:') !!}
    {!!
        Form::macro("check", function($name, $value = 1, $checked = null, $options = array()) {
            return '<input name="' . $name . '" value="0" type="hidden">' . Form::checkbox($name, $value, $checked, $options);
        })
    !!}

    {!! Form::check('visitante', true, 0,['class' => 'form-control']) !!}

</div>


<!-- Graduacao Field -->
<div class="form-group col-sm-12">
    {!! Form::label('graduacao', 'Grau de instrução:') !!}
    {!! Form::select('graduacao', trans("educacional.grau_instrucao"), null, ['placeholder' => 'Selecione a Graduação', 'class' => 'form-control col-sm-6', 'required' => 'required']) !!}
</div>
