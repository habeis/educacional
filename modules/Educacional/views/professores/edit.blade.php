@extends('layouts.page')

@section('content-center')
    @include('flash::message')

    @include('core-templates::common.errors')

    @include('core-templates::common.errors')

    <a href="{!! route('professores.index') !!}" class="btn btn-primary">Listar Professores</a>

    <div id="wizard" class="form_wizard wizard_horizontal">
        @include('professores.step')
        {!! Form::model($professor, ['route' => ['professores.update', $professor->id], 'method' => 'patch']) !!}
        <div class="stepContainer">

            <div id="step-1" class="wizard_content" style="display: block;">

                {!! Form::hidden('cpf', $pessoa->cpf) !!}
                @include('pessoas.fields')
            </div>
            <div id="step-2" class="wizard_content" style="display: none;">
                @include('professores.fields')
            </div>
            <div id="step-3" class="wizard_content" style="display: none;">

                {!! Form::hidden('endereco_id', $endereco->id) !!}
                @include('enderecos.fields')
                        <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('post-script')

    <script src="{{ asset('js/wizard/jquery.smartWizard.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard
            $('#wizard').smartWizard({
                includeFinishButton: false,

            });

            function onFinishCallback() {
                $('#wizard').smartWizard(
                        'showMessage', 'Finish Clicked');
                //alert('Finish Clicked');
            }
        });

        $(document).ready(function () {
            // Smart Wizard
            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });

        });
    </script>


@endsection
