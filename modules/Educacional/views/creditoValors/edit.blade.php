@extends('layouts.page')

@section('content-center')
    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::model($creditoValor, ['route' => ['creditoValors.update', $creditoValor->id], 'method' => 'patch']) !!}

        @include('creditoValors.fields')

        {!! Form::close() !!}
    </div>
@endsection