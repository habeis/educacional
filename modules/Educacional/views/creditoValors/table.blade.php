<table class="table table-responsive">
    <thead>
        <th>Valor</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($creditoValors as $creditoValor)
        <tr>
            <td>{!! $creditoValor->valor !!}</td>
            <td>
                {!! Form::open(['route' => ['creditoValors.destroy', $creditoValor->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('creditoValors.show', [$creditoValor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('creditoValors.edit', [$creditoValor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>