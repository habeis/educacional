@extends('layouts.page')

@section('content-center')
    @include('flash::message')
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('creditoValors.create') !!}">{{ trans("forms.button_create") }}</a>
    </a>

    <div class="clearfix"></div>

    @if($creditoValors->isEmpty())
        <div class="well text-center">Nenhum valor de Credito encontrado.</div>
    @else
        @include('creditoValors.table')
    @endif
@endsection