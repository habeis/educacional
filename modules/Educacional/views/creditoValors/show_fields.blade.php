<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Código:') !!}
    <p>{!! $creditoValor->id !!}</p>
</div>

<!-- Valor Field -->
<div class="form-group">
    {!! Form::label('valor', 'Valor:') !!}
    <p>{!! Form::text('valor',  $creditoValor->valor, ['class' => 'flat','disabled'=>'disabled']) !!}</p>
</div>


<!-- Submit Field -->
<div class="form-group ">
    <a href="{!! route('creditoValors.index') !!}" class="btn btn-primary">Cancelar</a>
</div>


