@extends('layouts.page')

@section('content-center')

    @include('core-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'creditoValors.store']) !!}

        @include('creditoValors.fields')

        {!! Form::close() !!}
    </div>
@endsection