<?php
// Aluno Historico
Breadcrumbs::register('aluno-historico.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Histórico', route('aluno-historico.index'));
});

Breadcrumbs::register('aluno-historico-detalhado', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Histórico Detalhado', route('aluno-historico-detalhado'));
});

// Coligada
Breadcrumbs::register('coligadas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar Coligadas', route('coligadas.index'));
});

Breadcrumbs::register('coligadas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Coligada', route('coligadas.create'));
});

Breadcrumbs::register('coligadas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Coligada', route('coligadas.edit'));
});

Breadcrumbs::register('coligadas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes da coligada', route('coligadas.show'));
});

// Curso
Breadcrumbs::register('cursos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Cursos', route('cursos.index'));
});

Breadcrumbs::register('cursos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Curso', route('cursos.create'));
});

Breadcrumbs::register('cursos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Curso', route('cursos.edit'));
});

Breadcrumbs::register('cursos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do curso', route('cursos.show'));
});

// Habilitação
Breadcrumbs::register('habilitacaos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Habilitacões', route('habilitacaos.index'));
});

Breadcrumbs::register('habilitacaos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Habilitação', route('habilitacaos.create'));
});

Breadcrumbs::register('habilitacaos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Habilitação', route('habilitacaos.edit'));
});

Breadcrumbs::register('habilitacaos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Habilitação', route('habilitacaos.show'));
});

// Matriz Curricular
Breadcrumbs::register('matrizCurriculars.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Matriz Curriculares', route('matrizCurriculars.index'));
});

Breadcrumbs::register('matrizCurriculars.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Matriz Curricular', route('matrizCurriculars.create'));
});

Breadcrumbs::register('matrizCurriculars.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Matriz Curricular', route('matrizCurriculars.edit'));
});

Breadcrumbs::register('matrizCurriculars.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Matriz Curricular', route('matrizCurriculars.show'));
});

// Matriz Curricular Disciplina
Breadcrumbs::register('matrizCurricularDisciplinas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar disciplina da Matriz Curriculares', route('matrizCurricularDisciplinas.index'));
});

Breadcrumbs::register('matrizCurricularDisciplinas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar disciplina da Matriz Curricular', route('matrizCurricularDisciplinas.create'));
});

Breadcrumbs::register('matrizCurricularDisciplinas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar disciplina da Matriz Curricular', route('matrizCurricularDisciplinas.edit'));
});

Breadcrumbs::register('matrizCurricularDisciplinas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes disciplina da Matriz Curricular', route('matrizCurricularDisciplinas.show'));
});

// Turma
Breadcrumbs::register('turmas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Turmas', route('turmas.index'));
});

Breadcrumbs::register('turmas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Turma', route('turmas.create'));
});

Breadcrumbs::register('turmas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Turmas', route('turmas.edit'));
});

Breadcrumbs::register('turmas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes da Turmas', route('turmas.show'));
});

Breadcrumbs::register('semestre-cursos', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Turmas', route('semestre-cursos'));
});

// Turma Disciplina
Breadcrumbs::register('turmaDisciplinas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Turmas', route('turmaDisciplinas.index'));
});

Breadcrumbs::register('turmaDisciplinas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Turma', route('turmaDisciplinas.create'));
});

Breadcrumbs::register('turmaDisciplinas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Turmas', route('turmaDisciplinas.edit'));
});

Breadcrumbs::register('turmaDisciplinas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes da Turmas', route('turmaDisciplinas.show'));
});
// Disciplina
Breadcrumbs::register('disciplinas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de disciplinas', route('disciplinas.index'));
});

Breadcrumbs::register('disciplinas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar disciplinas', route('disciplinas.create'));
});

Breadcrumbs::register('disciplinas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar disciplinas', route('disciplinas.edit'));
});

Breadcrumbs::register('disciplinas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes da disciplinas', route('disciplinas.show'));
});

// Modo Curso
Breadcrumbs::register('modoCursos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Modos de Cursos', route('modoCursos.index'));
});

Breadcrumbs::register('modoCursos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Modos de Curso', route('modoCursos.create'));
});

Breadcrumbs::register('modoCursos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Modos de Curso', route('modoCursos.edit'));
});

Breadcrumbs::register('modoCursos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Modos de curso', route('modoCursos.show'));
});

// Periodos
Breadcrumbs::register('periodos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Periodos', route('periodos.index'));
});

Breadcrumbs::register('periodos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Períodos', route('periodos.create'));
});

Breadcrumbs::register('periodos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Períodos', route('periodos.edit'));
});

Breadcrumbs::register('periodos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Períodos', route('periodos.show'));
});

// Tipo de Curso
Breadcrumbs::register('tipoCursos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Tipo de Curso', route('tipoCursos.index'));
});

Breadcrumbs::register('tipoCursos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Tipo de Curso', route('tipoCursos.create'));
});

Breadcrumbs::register('tipoCursos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Tipo de Curso', route('tipoCursos.edit'));
});

Breadcrumbs::register('tipoCursos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Tipo de Curso', route('tipoCursos.show'));
});

// Coordenação
Breadcrumbs::register('coordenacaos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Coordenações', route('coordenacaos.index'));
});

Breadcrumbs::register('coordenacaos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Coordenações', route('coordenacaos.create'));
});

Breadcrumbs::register('coordenacaos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Coordenações', route('coordenacaos.edit'));
});

Breadcrumbs::register('coordenacaos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Coordenações', route('coordenacaos.show'));
});
// Portarias
Breadcrumbs::register('portarias.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de portarias', route('portarias.index'));
});

Breadcrumbs::register('coordenacaos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Portaria', route('portarias.create'));
});

Breadcrumbs::register('portarias.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Portaria', route('portarias.edit'));
});

Breadcrumbs::register('portarias.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Portaria', route('portarias.show'));
});
// Modalidades
Breadcrumbs::register('modalidades.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de modalidades', route('modalidades.index'));
});

Breadcrumbs::register('modalidades.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Modalidade', route('modalidades.create'));
});

Breadcrumbs::register('modalidades.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Kodalidade', route('modalidades.edit'));
});

Breadcrumbs::register('modalidades.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes da Modalidades', route('modalidades.show'));
});
// Turnos
Breadcrumbs::register('turnos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Turnos', route('turnos.index'));
});

Breadcrumbs::register('turnos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Turno', route('turnos.create'));
});

Breadcrumbs::register('turnos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Turno', route('turnos.edit'));
});

Breadcrumbs::register('turnos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Turno', route('turnos.show'));
});

// Aluno
Breadcrumbs::register('alunos.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Alunos', route('alunos.index'));
});

Breadcrumbs::register('alunos.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Criar Aluno', route('alunos.create'));
});

Breadcrumbs::register('alunos.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Aluno', route('alunos.edit'));
});

Breadcrumbs::register('alunos.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Aluno   ', route('alunos.show'));
});

// Aluno Turma Disciplina
Breadcrumbs::register('alunoTurmaDisciplinas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Disciplinas Matrículado', route('alunoTurmaDisciplinas.index'));
});

Breadcrumbs::register('alunoTurmaDisciplinas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Matrícular Aluno na Disciplina', route('alunoTurmaDisciplinas.create'));
});

Breadcrumbs::register('alunoTurmaDisciplinas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Matrícular Aluno na Disciplina', route('alunoTurmaDisciplinas.edit'));
});

Breadcrumbs::register('alunoTurmaDisciplinas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes de Disciplinas matrículado', route('alunoTurmaDisciplinas.show'));
});

// Aluno Turma

Breadcrumbs::register('alunoTurmas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Matrícula Calouro / Veterano', route('alunoTurmas.index'));
});

Breadcrumbs::register('alunoTurmas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Matrícular Aluno na Turma', route('alunoTurmas.create'));
});

Breadcrumbs::register('alunoTurmas.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Aluno na Turma', route('alunoTurmas.edit'));
});

Breadcrumbs::register('alunoTurmas.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Aluno na Turma', route('alunoTurmas.show'));
});

Breadcrumbs::register('alunos-turma', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Matrícula de Calouro', route('alunos-turma'));
});

Breadcrumbs::register('aluno-cpf', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Matrícula Calouro / Veterano', route('aluno-cpf'));
});

// anoSemestres
Breadcrumbs::register('anoSemestres.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar alunos da Turma', route('anoSemestres.index'));
});

Breadcrumbs::register('anoSemestres.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Matrícular Aluno na Turma', route('anoSemestres.create'));
});

Breadcrumbs::register('anoSemestres.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Aluno na Turma', route('anoSemestres.edit'));
});

Breadcrumbs::register('anoSemestres.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Aluno na Turma', route('anoSemestres.show'));
});

// Professores
Breadcrumbs::register('professores.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Listar de Professor', route('professores.index'));
});

Breadcrumbs::register('professores.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cadastrar Novo Professor', route('professores.create'));
});

Breadcrumbs::register('professores.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Editar Professor', route('professores.edit'));
});

Breadcrumbs::register('professores.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Detalhes do Professor', route('professores.show'));
});

Breadcrumbs::register('professor-cpf', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cadastro de Professor', route('professor-cpf'));
});

Breadcrumbs::register('professor-disciplina.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cadastro de Professor', route('professor-disciplina.index'));
});
Breadcrumbs::register('professor-disciplina.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cadastro de Disciplina para Professor', route('professor-disciplina.create'));
});

Breadcrumbs::register('professor-disciplina-curso', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cadastro de Professor', route('professor-disciplina-curso'));
});
