<?php

namespace Educacional\Repositories;

use Educacional\Models\Professor;
use InfyOm\Generator\Common\BaseRepository;

class ProfessorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Professor::class;
    }
}
