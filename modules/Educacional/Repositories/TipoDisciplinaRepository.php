<?php

namespace Educacional\Repositories;

use Educacional\Models\TipoDisciplina;
use InfyOm\Generator\Common\BaseRepository;

class TipoDisciplinaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoDisciplina::class;
    }
}
