<?php

namespace Educacional\Repositories;

use Educacional\Models\MatrizCurricular;
use InfyOm\Generator\Common\BaseRepository;

class MatrizCurricularRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MatrizCurricular::class;
    }
}
