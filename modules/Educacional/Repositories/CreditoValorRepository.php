<?php

namespace Educacional\Repositories;

use Educacional\Models\CreditoValor;
use InfyOm\Generator\Common\BaseRepository;

class CreditoValorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CreditoValor::class;
    }
}
