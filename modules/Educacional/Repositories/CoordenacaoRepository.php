<?php

namespace Educacional\Repositories;

use Educacional\Models\Coordenacao;
use InfyOm\Generator\Common\BaseRepository;

class CoordenacaoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Coordenacao::class;
    }
}
