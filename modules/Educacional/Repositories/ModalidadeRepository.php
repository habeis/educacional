<?php

namespace Educacional\Repositories;

use Educacional\Models\Modalidade;
use InfyOm\Generator\Common\BaseRepository;

class ModalidadeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Modalidade::class;
    }
}
