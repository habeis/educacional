<?php

namespace Educacional\Repositories;

use Educacional\Models\AlunoTurma;
use InfyOm\Generator\Common\BaseRepository;

class AlunoTurmaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AlunoTurma::class;
    }

    //$disciplinaRetirar->get($j)->matrizDisciplinaId
    public function retirarDisciplinaArra($turmasDisciplinas, $disciplinaRetirar)
    {

        for ($i = 0; $i < count($turmasDisciplinas); $i++) {
            for ($j = 0; $j < count($disciplinaRetirar); $j++) {

                if ($turmasDisciplinas[$i]['matrizDisciplinaId'] == $disciplinaRetirar->get($j)->matrizDisciplinaId) {

                    array_pull($turmasDisciplinas[$i], $turmasDisciplinas[$i]);
                }

            }
        }

    }

    // Tirar as disciplinas que o mesmo já cursou da variável $turmasDisciplinas
    public function consultaDisciplinasCursadaId($matricula){
        
        return AlunoTurma::from('educacional.aluno_turma')
            ->join('educacional.aluno_turma_disciplina as atd', 'educacional.aluno_turma.id', '=', 'atd.aluno_turma_id')
            ->join('educacional.turma_disciplina as td', 'td.id', '=', 'atd.turma_disciplina_id')
            ->join('educacional.matriz_curricular_disciplina as mtd', 'mtd.id', '=', 'td.matrriz_curricular_disciplina_id')
            ->where('matricula', '=', $matricula)
            ->get(['mtd.id as matrizDisciplinaId']);
    }
    
    // Tirar as disciplinas que o mesmo já cursou da variável $turmasDisciplinas
    public function consultaDisciplinasCursada($matricula){
        
        return AlunoTurma::from('educacional.aluno_turma')
            ->join('educacional.aluno_turma_disciplina as atd', 'educacional.aluno_turma.id', '=', 'atd.aluno_turma_id')
            ->join('educacional.turma_disciplina as td', 'td.id', '=', 'atd.turma_disciplina_id')
            ->join('educacional.matriz_curricular_disciplina as mtd', 'mtd.id', '=', 'td.matrriz_curricular_disciplina_id')
            ->where('matricula', '=', $matricula)
            ->get(['educacional.aluno_turma.*', 'mtd.*']);
    }
}
