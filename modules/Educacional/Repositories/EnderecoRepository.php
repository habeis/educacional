<?php

namespace Educacional\Repositories;

use Educacional\Models\Endereco;
use InfyOm\Generator\Common\BaseRepository;

class EnderecoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Endereco::class;
    }

    public function atualizarCriar($input)
    {
        // Salva ou atualiza o Endereço
        if (!empty($input['endereco_id'])) {

            $endereco = $this->update($input, $input['endereco_id']);
        } else {
            $endereco = $this->create($input);
        }
        return $endereco;
    }
}
