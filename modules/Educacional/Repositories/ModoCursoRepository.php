<?php

namespace Educacional\Repositories;

use Educacional\Models\ModoCurso;
use InfyOm\Generator\Common\BaseRepository;

class ModoCursoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ModoCurso::class;
    }
}
