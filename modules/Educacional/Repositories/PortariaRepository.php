<?php

namespace Educacional\Repositories;

use Educacional\Models\Portaria;
use InfyOm\Generator\Common\BaseRepository;

class PortariaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Portaria::class;
    }
}
