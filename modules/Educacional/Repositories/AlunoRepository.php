<?php

namespace Educacional\Repositories;

use EduAdmin\Repositories\UserRepository;
use Educacional\Models\Aluno;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Common\BaseRepository;
use Laracasts\Flash\Flash;
use Nucleo\Repositories\PessoaRepository;

class AlunoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Aluno::class;
    }

    public function salvarAtualizarAluno(
        $input,
        PessoaRepository $pessoaRepository,
        EnderecoRepository $enderecoRepository,
        UserRepository $userRepository)
    {
        //Busca a pessoa se o mesmo conter ela já no banco de dados
        $pessoa = $pessoaRepository->findWhere(['cpf' => $input['cpf']])->first();

        // Salva ou atualiza o Endereço
        if (!empty($input['endereco_id'])) {

            $endereco = $enderecoRepository->update($input, $input['endereco_id']);
        } else {
            $endereco = $enderecoRepository->create($input);
        }
        $input['endereco_id'] = $endereco->id;

        if (!empty($pessoa)) {
            // Se conter no banco de dados atualizar se não conter Cria um novo.
            $pessoa = $pessoaRepository->update($input, $pessoa->id);
        } else {
            $pessoa = $pessoaRepository->create($input);
        }

        $input['pessoa_id'] = $pessoa->id;

        // Pesquisa para ver se essa pessoa já contém User no banco
        $user = $userRepository->findWhere(['pessoa_id' => $pessoa->id])->first();


        //Se contém o usuário então apenas atualiza
        if (isset($user)) {

            $user = $userRepository->update($input, $user->id);

        } else {

            // O sistema não pode permitir uma pessoa cadastra o mesmo email mais que 1 vez
            $user = $userRepository->findWhere(['email' => $input['email']])->first();

            if (isset($user)) {

                // o email pega o cpf até o usuário atualizar pois já contem no banco de dados esse email
                $input['email'] = $input['cpf'];
                Flash::error('Email precisa ser atualizado pois já contém cadastrado no sistema.');
            }

            $input['name'] = $input['cpf'];
            $input['password'] = Hash::make($input['pessoa_id']);

            $user = $userRepository->create($input);
        }

        $input['user_id'] = $user->id;

        return $this->create($input);
    }
}
