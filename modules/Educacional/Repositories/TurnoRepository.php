<?php


namespace Educacional\Repositories;

use Educacional\Models\Turno;
use InfyOm\Generator\Common\BaseRepository;

class TurnoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
         **/
    public function model()
    {
        return Turno::class;
    }
}
