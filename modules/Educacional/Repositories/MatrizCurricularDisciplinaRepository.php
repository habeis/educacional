<?php

namespace Educacional\Repositories;

use Educacional\Models\MatrizCurricularDisciplina;
use InfyOm\Generator\Common\BaseRepository;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class MatrizCurricularDisciplinaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    public function findMatrizCurricular($id)
    {
        return $this->findWhere(['matriz_curricular_id' => $id]);

    }

    public function disciplinasPeriodoIdMatrizId($periodoId, $matrizCurricularId)
    {
        // Busca todas as disciplinas daquela Matriz e daquele período selecionando anterior
        $disciplinaMaCu = $this->findWhere(['periodo_id' => $periodoId, 'matriz_curricular_id' => $matrizCurricularId]);
        return $disciplinaMaCu;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MatrizCurricularDisciplina::class;
    }
}
