<?php

namespace Educacional\Repositories;

use Educacional\Models\TurmaDisciplina;
use InfyOm\Generator\Common\BaseRepository;

class TurmaDisciplinaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Adiciona as disicplinas na turma
     * @param $matrizDisciplinas
     * @param $input
     */
    public function createDisciplinaTurma($matrizDisciplinas, $input)
    {
        // Salva todas disciplina da turma de um determinado período
        foreach ($matrizDisciplinas as $disciplinas) {
            $input['matrriz_curricular_disciplina_id'] = $disciplinas->id;
            $input['nome_disciplina'] = $disciplinas->disciplina->nome;

            $this->create($input);
        }
    }

    private  $semestreLetivoId;

    private  $cursoId;

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TurmaDisciplina::class;
    }

    // Consulta todas disciplinas das turmas de um determinado Curso e do semestre atual e retira todas que o aluno já cursou
    public function disciplinasCursar($semestreLetivoId, $curso_id, $disciplinaRetirar)
    {
        $this->semestreLetivoId = $semestreLetivoId;
        
        $this->cursoId = $curso_id;

        return TurmaDisciplina::from('educacional.turma_disciplina')
            ->distinct()
            ->join('educacional.turma', 'educacional.turma_disciplina.turma_id', '=', 'educacional.turma.id')
            ->join('educacional.ano_semestre', function ($join) {
                $join->on('educacional.turma.ano_semestre_id', '=', 'educacional.ano_semestre.id')
                    ->where('educacional.ano_semestre.atual', '=', true)
                    ->where('educacional.ano_semestre.id', '=', $this->semestreLetivoId);

            })
            ->join('educacional.matriz_curricular_disciplina', 'educacional.turma_disciplina.matrriz_curricular_disciplina_id', '=', 'educacional.matriz_curricular_disciplina.id')
            ->join('educacional.matriz_curricular', function ($join) {
                $join->on('educacional.matriz_curricular_disciplina.matriz_curricular_id', '=', 'educacional.matriz_curricular.id')
                    ->where('educacional.matriz_curricular.curso_id', '=', $this->cursoId);
            })
            ->whereNotIn('educacional.matriz_curricular_disciplina.id',
                array_pluck($disciplinaRetirar->all(), 'matrizDisciplinaId'))
            ->orderBy('educacional.turma.periodo_id')
            ->get(
                [
                    'educacional.turma_disciplina.id as turmaDisciplinaId',
                    'educacional.turma_disciplina.*',
                    'educacional.turma.*',
                    'educacional.matriz_curricular_disciplina.id as matrizDisciplinaId'
                ]);

    }
}
