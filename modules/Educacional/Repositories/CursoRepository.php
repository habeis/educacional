<?php

namespace Educacional\Repositories;

use Educacional\Models\Curso;
use InfyOm\Generator\Common\BaseRepository;

class CursoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Curso::class;
    }

    /**
     * Ataliza a Matrícula do Curso
     * @param $curso
     */
    public function atualizaMatriculaCurso($curso)
    {
        $curso2 = \DB::table('educacional.curso')
            ->where('id', $curso->id)
            ->update(['matricula' => $curso->matricula]);

        return $curso2;
    }

    /**
     * Busca o Curso de uma Turma
     * @param $turmaId
     */
    public function buscarTurmaCurso($turmaId)
    {
        $curso = \DB::table('educacional.curso')
            ->leftJoin('educacional.turma', 'educacional.curso.id', '=', 'educacional.turma.curso_id')
            ->where('educacional.turma.id', '=', $turmaId)
            ->get(['educacional.curso.*']);
        
        return $curso;
    }
}
