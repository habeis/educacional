<?php

namespace Educacional\Repositories;

use Educacional\Models\AlunoTurmaDisciplina;
use InfyOm\Generator\Common\BaseRepository;

class AlunoTurmaDisciplinaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AlunoTurmaDisciplina::class;
    }

    public function salvarDisciplinasAluno($arrayDisciplinas, $alunoTurmaId)
    {
        // percorre pegando todas as discioplinas contidas na lista que foram seleconadas
        foreach ($arrayDisciplinas as $value) {

            $input['aluno_turma_id'] = $alunoTurmaId;
            $input['turma_disciplina_id'] = $value;
            
            $this->create($input);
        }
    }
}
