<?php

namespace Educacional\Repositories;

use Educacional\Models\Disciplina;
use InfyOm\Generator\Common\BaseRepository;

class DisciplinaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Disciplina::class;
    }
}
