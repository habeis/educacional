<?php

namespace Educacional\Repositories;

use Educacional\Models\Coligada;
use InfyOm\Generator\Common\BaseRepository;

class ColigadaRepository extends BaseRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Coligada::class;
    }

    // Salva os tipos de cursos das Coligadas
    public function salvarTipoCursoColigada($inputs, $coligada, $tipoCursoRepository)
    {
        // Percorre a lista de Tipo de Cursos que foram Adiconados na Tabela
        foreach ($inputs['tipoCursos'] as $tipoCursoId) {
            if (empty($this->findWithoutFail($coligada->id)->tipoCursos()->find($tipoCursoId))) {
                // Pesquisa o Tipo de Curso
                $tipoCurso = $tipoCursoRepository->findWithoutFail($tipoCursoId);
                // Salva um tipo de curso com a coligada na tabela Coligada_tipo_curso
                $this->findWithoutFail($coligada->id)->tipoCursos()->save($tipoCurso,['ativo' => true]);
            }
        }
    }
    public function listarTipoCursosColigada(){
        //$this->first()->
    }

}
