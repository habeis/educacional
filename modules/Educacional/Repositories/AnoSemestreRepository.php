<?php

namespace Educacional\Repositories;

use DB;
use Educacional\Models\AnoSemestre;
use InfyOm\Generator\Common\BaseRepository;

class AnoSemestreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AnoSemestre::class;
    }

    // SQL Nativa
    public function findAllTipoCursoId($tipoCursoId){

        return DB::select('select sm.* from 
                  educacional.ano_semestre as sm , 
                  educacional.tipo_curso as tc,
                  educacional.coligada_tipo_curso as ctc
                  where 
                      tc.id = ? and  
                      ctc.tipo_curso_id = tc.id and
                      sm.coligada_tipo_curso_id = cc.id ',[$tipoCursoId]);
    }
}
