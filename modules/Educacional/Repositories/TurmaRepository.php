<?php

namespace Educacional\Repositories;

use Educacional\Models\Turma;
use InfyOm\Generator\Common\BaseRepository;

class TurmaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Turma::class;
    }

    
}
