<?php

namespace Educacional\Repositories;

use Educacional\Models\Habilitacao;
use InfyOm\Generator\Common\BaseRepository;

class HabilitacaoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Habilitacao::class;
    }
}
