<?php

namespace Educacional\Repositories;

use Educacional\Models\Periodo;
use InfyOm\Generator\Common\BaseRepository;
use Symfony\Component\Finder\Comparator\NumberComparator;

class PeriodoRepository extends BaseRepository
{

    private $gradeId;
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Periodo::class;
    }

    /**
     * Consulta no qual traz os
     * periodos daquele curso
     * que contém disciplinas cadastradas.
     */
    public function periodoMatrizCurridularId($gradeId)
    {

        $this->gradeId = $gradeId;

        $periodoCurso = \DB::table('educacional.periodo')
            ->join('educacional.matriz_curricular_disciplina', function ($join) {
                    $join->on('educacional.periodo.id', '=', 'periodo_id')
                        ->where('matriz_curricular_id', '=', $this->gradeId);
            })
            ->select('periodo.descricao', 'periodo.id')
            ->get();

        return $periodoCurso;
    }
}
