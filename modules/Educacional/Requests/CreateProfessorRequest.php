<?php

namespace Educacional\Requests;

use App\Http\Requests\Request;
use Educacional\Models\Professor;

class CreateProfessorRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Professor::$rules;
    }
}
