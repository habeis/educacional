<?php

namespace Educacional\Requests;

use App\Http\Requests\Request;
use Educacional\Models\Curso;

class UpdateCursoRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Curso::$rules;
    }
}
