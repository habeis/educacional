<?php

namespace Educacional\Http\Requests;

use Educacional\Http\Requests\Request;
use App\Models\AlunoTurmaDisciplina;

class UpdateAlunoTurmaDisciplinaRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return AlunoTurmaDisciplina::$rules;
    }
}
