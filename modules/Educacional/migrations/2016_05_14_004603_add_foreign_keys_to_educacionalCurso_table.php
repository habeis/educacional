<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalCursoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.curso', function(Blueprint $table)
		{
			$table->foreign('coligada_tipo_curso_id', 'foreign_key_coligada_tipo_curso_id')->references('id')->on('educacional.coligada_tipo_curso')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('coordenacao_id', 'foreign_key_coordenacao_id')->references('id')->on('educacional.coordenacao')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('habilitacao_id', 'foreign_key_habilitacao')->references('id')->on('educacional.habilitacao')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('modalidade_id', 'foreign_key_modalidade')->references('id')->on('educacional.modalidade')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('modo_curso_id', 'foreign_key_modo_curso_id')->references('id')->on('educacional.modo_curso')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('portaria_id', 'foreign_key_portaria_id')->references('id')->on('educacional.portaria')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('turno_id', 'foreign_key_turno_id')->references('id')->on('educacional.turno')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.curso', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_coligada_tipo_curso_id');
			$table->dropForeign('foreign_key_coordenacao_id');
			$table->dropForeign('foreign_key_habilitacao');
			$table->dropForeign('foreign_key_modalidade');
			$table->dropForeign('foreign_key_modo_curso_id');
			$table->dropForeign('foreign_key_portaria_id');
			$table->dropForeign('foreign_key_turno_id');
		});
	}

}
