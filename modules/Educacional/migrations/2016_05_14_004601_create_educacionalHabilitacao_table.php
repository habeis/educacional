<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalHabilitacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.habilitacao', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('descricao', 100)->nullable();
			$table->string('nome', 100);
			$table->text('decreto_lei_resolucao')->nullable();
			$table->boolean('ativo')->default(true);
			
			$table->timestamps();
			$table->softDeletes();
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.habilitacao');
	}

}
