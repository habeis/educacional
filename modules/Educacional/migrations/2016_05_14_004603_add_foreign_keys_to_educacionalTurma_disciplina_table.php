<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalTurmaDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.turma_disciplina', function(Blueprint $table)
		{
			$table->foreign('matrriz_curricular_disciplina_id', 'foreign_key_matriz_curricular_disci__pk')->references('id')->on('educacional.matriz_curricular_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('turma_id', 'foreign_key_turma_id')->references('id')->on('educacional.turma')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.turma_disciplina', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_matriz_curricular_disci__pk');
			$table->dropForeign('foreign_key_turma_id');
		});
	}

}
