<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nucleo.role_user', function (Blueprint $table) {

            $table->integer('id', true)
                ->unique();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nucleo.role_user', function(Blueprint $table)
        {
            $table->dropColumn('id');
        });
    }
}
