<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalProfessorDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.professor_disciplina', function(Blueprint $table)
		{
			$table->integer('id', true);
			
			$table->integer('professor_id');
			$table->integer('turma_disciplina_id')->index('idx_professor_disciplina');

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.professor_disciplina');
	}

}
