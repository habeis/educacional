<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalAlunoTurmaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.aluno_turma', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('turma_id')->index('idx_aluno_turma');
			$table->integer('aluno_id')->index('idx_aluno_turma_0');
			$table->integer('matricula');
			$table->integer('forma_ingresso_id');
			$table->date('data_ingresso');
			$table->integer('status_id');

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.aluno_turma');
	}

}
