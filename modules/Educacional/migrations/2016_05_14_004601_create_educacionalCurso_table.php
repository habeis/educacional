<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalCursoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.curso', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 100);
			$table->integer('quantidade_periodo');
			$table->string('horario_turno', 30)->nullable();
			$table->integer('inep_id')->nullable();
			$table->text('obs')->nullable();
			$table->boolean('ativo')->default(true);

			$table->timestamps();
			$table->softDeletes();

			$table->integer('turno_id');
			$table->integer('coordenacao_id');
			$table->integer('habilitacao_id');
			$table->integer('coligada_tipo_curso_id');
			$table->integer('modalidade_id');
			$table->integer('modo_curso_id');
			$table->integer('portaria_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.curso');
	}

}
