<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalHorarioModeloHorarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.horario_modelo_horario', function(Blueprint $table)
		{
			$table->integer('id', true);

			$table->timestamps();
			$table->softDeletes();

			$table->integer('horario_id')->index('idx_horario_modelo_horario_0');
			$table->integer('modleo_horario_id')->index('idx_horario_modelo_horario');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.horario_modelo_horario');
	}

}
