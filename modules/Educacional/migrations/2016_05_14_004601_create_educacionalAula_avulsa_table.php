<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalAulaAvulsaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.aula_avulsa', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('data')->nullable();
			
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('turma_disciplina_id')->index('idx_aula_avulsa');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.aula_avulsa');
	}

}
