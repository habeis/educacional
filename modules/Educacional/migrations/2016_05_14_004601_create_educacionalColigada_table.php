<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalColigadaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educacional.coligada', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nome');
            $table->string('cnpj', 20);
            $table->string('razao_social')->nullable();
            $table->string('telefone', 14)->nullable();
            $table->text('obs')->nullable();

            $table->timestamps();
            $table->softDeletes();
            
            $table->integer('endereco_id');
            
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('educacional.coligada');
    }

}