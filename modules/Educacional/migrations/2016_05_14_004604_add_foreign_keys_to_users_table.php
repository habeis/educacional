<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nucleo.users', function (Blueprint $table) {
            $table->integer('pessoa_id')->index('idx_pessoa_user');

            $table->foreign('pessoa_id', 'fk_pessoa_id')
                ->references('id')
                ->on('nucleo.pessoa')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nucleo.users', function (Blueprint $table) {
            $table->dropForeign('fk_pessoa_id');
            $table->dropColumn('pessoa_id');
        });
    }

}
