<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalAnoSemestreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.ano_semestre', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('ano_semestre', 8);
			$table->date('data_inicio');
			$table->date('data_fim');
			$table->text('obs')->nullable();
			$table->boolean('atual');
			$table->integer('coligada_tipo_curso_id');

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.ano_semestre');
	}

}
