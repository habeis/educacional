<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalProfessorDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.professor_disciplina', function(Blueprint $table)
		{
			$table->foreign('turma_disciplina_id', 'fk_professor_disciplina')->references('id')->on('educacional.turma_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('professor_id', 'foreign_key_professor_fk')->references('id')->on('educacional.professor')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.professor_disciplina', function(Blueprint $table)
		{
			$table->dropForeign('fk_professor_disciplina');
			$table->dropForeign('foreign_key_professor_fk');
		});
	}

}
