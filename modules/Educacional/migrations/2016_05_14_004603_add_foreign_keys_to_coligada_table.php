<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToColigadaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.coligada', function(Blueprint $table)
		{
			$table->foreign('endereco_id', 'foreign_key_coligada_endereco_fk')->references('id')->on('nucleo.endereco')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.coligada', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_coligada_endereco_fk');
		});
	}

}
