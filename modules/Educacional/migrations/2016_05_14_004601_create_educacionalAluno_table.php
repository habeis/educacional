<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalAlunoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.aluno', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tipo_curso_id');
			$table->string('escola_conclusao', 100);
			$table->integer('tipo_escola_conclusao');
			$table->integer('ano_conclusao');
			$table->string('cidade_conclusao', 100);
			$table->boolean('ativo')->default(true);

			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('user_id')->index('idx_aluno');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.aluno');
	}

}
