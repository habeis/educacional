<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalColigadaTipoCursoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.coligada_tipo_curso', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('coligada_id');
			$table->integer('tipo_curso_id');
			$table->boolean('ativo');

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.coligada_tipo_curso');
	}

}
