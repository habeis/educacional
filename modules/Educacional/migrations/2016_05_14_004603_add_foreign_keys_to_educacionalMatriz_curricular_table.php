<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalMatrizCurricularTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.matriz_curricular', function(Blueprint $table)
		{
			$table->foreign('curso_id', 'foreign_key_curso_id')->references('id')->on('educacional.curso')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.matriz_curricular', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_curso_id');
		});
	}

}
