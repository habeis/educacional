<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalMatrizCurricularTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.matriz_curricular', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('versao');
			$table->integer('hora_total');
			$table->integer('horas_complementar')->nullable();
			$table->boolean('atual')->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->integer('curso_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.matriz_curricular');
	}

}
