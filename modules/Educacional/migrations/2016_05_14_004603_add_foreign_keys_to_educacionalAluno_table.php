<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalAlunoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.aluno', function(Blueprint $table)
		{
			$table->foreign('user_id', 'fk_aluno_user')
				->references('id')
				->on('nucleo.users')
				->onUpdate('RESTRICT')
				->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.aluno', function(Blueprint $table)
		{
			$table->dropForeign('fk_aluno_user');
		});
	}

}
