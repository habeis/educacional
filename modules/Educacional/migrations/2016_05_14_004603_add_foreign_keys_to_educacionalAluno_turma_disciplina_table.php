<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalAlunoTurmaDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.aluno_turma_disciplina', function(Blueprint $table)
		{
			$table->foreign('turma_disciplina_id', 'fk_aluno_turma_disciplina')->references('id')->on('educacional.turma_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('aluno_turma_id', 'foreign_key_aluno_turma_fk')->references('id')->on('educacional.aluno_turma')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.aluno_turma_disciplina', function(Blueprint $table)
		{
			$table->dropForeign('fk_aluno_turma_disciplina');
			$table->dropForeign('foreign_key_aluno_turma_fk');
		});
	}

}
