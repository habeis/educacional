<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnderecoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nucleo.endereco', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('logradouro');
			$table->integer('numero')->nullable();
			$table->string('cep', 15)->nullable();
			$table->string('bairro', 100);
			$table->string('cidade', 100);
			$table->string('uf', 2);
			$table->string('complemento', 100)->nullable();
			
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nucleo.endereco');
	}

}
