<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalAnoSemestreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.ano_semestre', function(Blueprint $table)
		{
			$table->foreign('coligada_tipo_curso_id', 'foreign_key_coligada_tipo_curso_id')->references('id')->on('educacional.coligada_tipo_curso')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.ano_semestre', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_coligada_tipo_curso_id');
		});
	}

}
