<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalAulaAvulsaHorarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.aula_avulsa_horario', function(Blueprint $table)
		{
			$table->integer('id', true);

			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('horario_id')->index('idx_aula_avulsa_horario_0');
			$table->integer('aula_avulsa_id')->index('idx_aula_avulsa_horario');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.aula_avulsa_horario');
	}

}
