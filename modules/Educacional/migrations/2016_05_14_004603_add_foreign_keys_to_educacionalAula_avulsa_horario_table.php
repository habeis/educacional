<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalAulaAvulsaHorarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.aula_avulsa_horario', function(Blueprint $table)
		{
			$table->foreign('horario_id', 'fk_aula_avulsa_horario_horario')->references('id')->on('educacional.horario')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('aula_avulsa_id', 'foreign_key_aula_avulsa_id')->references('id')->on('educacional.aula_avulsa')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.aula_avulsa_horario', function(Blueprint $table)
		{
			$table->dropForeign('fk_aula_avulsa_horario_horario');
			$table->dropForeign('foreign_key_aula_avulsa_id');
		});
	}

}
