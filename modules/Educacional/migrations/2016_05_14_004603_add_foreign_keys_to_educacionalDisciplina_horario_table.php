<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalDisciplinaHorarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.disciplina_horario', function(Blueprint $table)
		{
			$table->foreign('turma_disciplina_id', 'fk_disciplina_horario')->references('id')->on('educacional.turma_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('horario_id', 'fk_disciplina_horario_horario')->references('id')->on('educacional.horario')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.disciplina_horario', function(Blueprint $table)
		{
			$table->dropForeign('fk_disciplina_horario');
			$table->dropForeign('fk_disciplina_horario_horario');
		});
	}

}
