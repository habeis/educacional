<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMatriculaCursoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('educacional.curso', function (Blueprint $table) {
            /*
             *  Esse campo de matrícula pega como base para ser gerado as matŕicula dos alunos
             */
            $table->integer('matricula')->nullable();;
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('educacional.curso', function (Blueprint $table) {
            $table->dropColumn('matricula');
        });
    }

}
