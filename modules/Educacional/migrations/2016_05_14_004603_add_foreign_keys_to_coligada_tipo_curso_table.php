<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToColigadaTipoCursoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.coligada_tipo_curso', function(Blueprint $table)
		{
			$table->foreign('coligada_id', 'foreign_key_coligada_id')->references('id')->on('educacional.coligada')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('tipo_curso_id', 'foreign_key_tipo_curso_id')->references('id')->on('educacional.tipo_curso')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.coligada_tipo_curso', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_coligada_id');
			$table->dropForeign('foreign_key_tipo_curso_id');
		});
	}

}
