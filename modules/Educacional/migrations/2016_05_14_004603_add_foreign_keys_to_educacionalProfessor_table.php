<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalProfessorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.professor', function(Blueprint $table)
		{
			$table->foreign('user_id', 'fk_professor_user')->references('id')->on('nucleo.users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.professor', function(Blueprint $table)
		{
			$table->dropForeign('fk_professor_user');
		});
	}

}
