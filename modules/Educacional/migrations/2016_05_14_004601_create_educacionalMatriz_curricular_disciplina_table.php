<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalMatrizCurricularDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.matriz_curricular_disciplina', function(Blueprint $table)
		{
			$table->integer('id', true);
				
			$table->string('codigo_disciplina', 30);
			$table->string('nome_disciplina', 100);
			$table->integer('credito');
			$table->boolean('pre_requisito');
			$table->integer('carga_horaria');
			
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('periodo_id');
			$table->integer('credito_valor_id')->index('idx_matriz_curricular_disciplina_0');
			$table->integer('tipo_disciplina_id')->index('idx_matriz_curricular_disciplina');
			$table->integer('matriz_curricular_id');
			$table->integer('disciplina_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.matriz_curricular_disciplina');
	}

}
