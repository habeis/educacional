<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalDisciplinaDependenciaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.disciplina_dependencia', function(Blueprint $table)
		{
			$table->foreign('matriz_curricular_disciplina_id', 'foreign_key_matriz_curricular_disciplina_id')->references('id')->on('educacional.matriz_curricular_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.disciplina_dependencia', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_matriz_curricular_disciplina_id');
		});
	}

}
