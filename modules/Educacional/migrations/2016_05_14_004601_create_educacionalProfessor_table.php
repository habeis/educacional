<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalProfessorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.professor', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('graduacao');
			$table->boolean('ativo')->default(true);
			$table->boolean('substituto')->nullable();
			$table->boolean('visitante')->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->integer('user_id')->index('idx_professor');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.professor');
	}

}
