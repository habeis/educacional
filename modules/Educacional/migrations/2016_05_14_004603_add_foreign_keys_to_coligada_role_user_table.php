<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToColigadaRoleUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nucleo.coligada_role_user', function (Blueprint $table) {
            $table->foreign('coligada_id', 'foreign_key_coligada_id_user_coligada')
                ->references('id')
                ->on('educacional.coligada')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('role_user_id', 'foreign_key_role_user_fk')
                ->references('id')
                ->on('nucleo.role_user')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nucleo.coligada_role_user', function (Blueprint $table) {
            $table->dropForeign('foreign_key_coligada_id_user_coligada');
            $table->dropForeign('foreign_key_role_user_fk');
        });
    }

}
