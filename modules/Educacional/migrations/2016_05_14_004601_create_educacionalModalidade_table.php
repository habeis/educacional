<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalModalidadeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.modalidade', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('descricao', 100);
			$table->text('obs')->nullable();
			$table->boolean('ativo')->default(true);

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.modalidade');
	}

}
