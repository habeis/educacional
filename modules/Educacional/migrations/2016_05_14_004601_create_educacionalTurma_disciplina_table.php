<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalTurmaDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.turma_disciplina', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome_disciplina', 100);

			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('turma_id')->index('idx_turma_disciplina');
			$table->integer('matrriz_curricular_disciplina_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.turma_disciplina');
	}

}
