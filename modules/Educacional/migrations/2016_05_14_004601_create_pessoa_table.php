<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePessoaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nucleo.pessoa', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome');
			$table->string('cpf', 15);
			$table->string('rg', 10);
			$table->string('sexo', 30);
			$table->string('raca', 30);
			$table->date('data_nascimento');
			$table->string('estado_civil', 30);
			$table->string('naturalidade', 50);
			$table->string('nacionalidade', 50);
			$table->string('pai_nome', 100)->nullable();
			$table->string('pai_cpf', 15)->nullable();
			$table->string('mae_nome', 100)->nullable();
			$table->string('mae_cpf', 15)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('telefone_celular', 141)->nullable();
			$table->string('telefone_residencial', 15)->nullable();
			$table->string('telefone_comercial', 15)->nullable();
			$table->string('ramal', 4)->nullable();
			$table->string('fax', 15)->nullable();
			$table->date('data_emissao_rf')->nullable();
			$table->boolean('ativo')->default(true);

			$table->timestamps();
			$table->softDeletes();

			$table->integer('endereco_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nucleo.pessoa');
	}

}
