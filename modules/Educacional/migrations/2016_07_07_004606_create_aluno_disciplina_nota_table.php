<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlunoDisciplinaNotaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 *  Notas das disciplina que o aluno está cursando.
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.aluno_disciplina_nota', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->double('nota',2);

			$table->integer('modelo_nota_id');
			$table->integer('aluno_turma_disciplina_id');

			$table->foreign('modelo_nota_id', 'foreign_key_modelo_nota')->references('id')->on('nucleo.modelo_nota')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('aluno_turma_disciplina_id', 'foreign_key_aluno_turma_disciplina')->references('id')->on('educacional.aluno_turma_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.aluno_disciplina_nota');
	}

}
