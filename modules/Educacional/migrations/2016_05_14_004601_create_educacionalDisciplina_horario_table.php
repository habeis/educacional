<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalDisciplinaHorarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.disciplina_horario', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('turma_disciplina_id')->index('idx_disciplina_horario_0');
			$table->integer('horario_id')->index('idx_disciplina_horario');

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.disciplina_horario');
	}

}
