<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalAlunoTurmaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.aluno_turma', function(Blueprint $table)
		{
			$table->foreign('aluno_id', 'fk_aluno_turma_aluno')->references('id')->on('educacional.aluno')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('turma_id', 'foreign_key_turma_fk')->references('id')->on('educacional.turma')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.aluno_turma', function(Blueprint $table)
		{
			$table->dropForeign('fk_aluno_turma_aluno');
			$table->dropForeign('foreign_key_turma_fk');
		});
	}

}
