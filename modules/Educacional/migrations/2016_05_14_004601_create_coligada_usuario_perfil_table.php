<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColigadaUsuarioPerfilTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nucleo.coligada_role_user', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('role_user_id');
			$table->integer('coligada_id');
			
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nucleo.coligada_role_user');
	}

}
