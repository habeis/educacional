<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalHorarioModeloHorarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.horario_modelo_horario', function(Blueprint $table)
		{
			$table->foreign('modleo_horario_id', 'fk_horario_modelo_horario')->references('id')->on('educacional.modelo_horario')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('horario_id', 'foreign_key_horario_fk')->references('id')->on('educacional.horario')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.horario_modelo_horario', function(Blueprint $table)
		{
			$table->dropForeign('fk_horario_modelo_horario');
			$table->dropForeign('foreign_key_horario_fk');
		});
	}

}
