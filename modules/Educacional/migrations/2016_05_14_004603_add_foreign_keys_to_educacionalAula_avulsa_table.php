<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalAulaAvulsaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.aula_avulsa', function(Blueprint $table)
		{
			$table->foreign('turma_disciplina_id', 'fk_aula_avulsa')->references('id')->on('educacional.turma_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('turma_disciplina_id', 'foreign_key_turma_disciplina_fk')->references('id')->on('educacional.turma_disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.aula_avulsa', function(Blueprint $table)
		{
			$table->dropForeign('fk_aula_avulsa');
			$table->dropForeign('foreign_key_turma_disciplina_fk');
		});
	}

}
