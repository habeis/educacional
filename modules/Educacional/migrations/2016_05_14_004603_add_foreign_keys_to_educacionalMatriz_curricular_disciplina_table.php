<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalMatrizCurricularDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.matriz_curricular_disciplina', function(Blueprint $table)
		{
			$table->foreign('credito_valor_id', 'fk_credito_valor_fk')->references('id')->on('educacional.credito_valor')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('disciplina_id', 'foreign_key_disciplina')->references('id')->on('educacional.disciplina')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('matriz_curricular_id', 'foreign_key_matriz_curricular')->references('id')->on('educacional.matriz_curricular')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('periodo_id', 'foregn_key_periodo_id')->references('id')->on('educacional.periodo')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.matriz_curricular_disciplina', function(Blueprint $table)
		{
			$table->dropForeign('fk_credito_valor_fk');
			$table->dropForeign('foreign_key_disciplina');
			$table->dropForeign('foreign_key_matriz_curricular');
			$table->dropForeign('foregn_key_periodo_id');
		});
	}

}
