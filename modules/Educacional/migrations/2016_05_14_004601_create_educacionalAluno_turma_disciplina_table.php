<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducacionalAlunoTurmaDisciplinaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educacional.aluno_turma_disciplina', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('aluno_turma_id')->index('idx_aluno_turma_disciplina');
			$table->integer('turma_disciplina_id')->index('idx_aluno_turma_disciplina_0');

			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educacional.aluno_turma_disciplina');
	}

}
