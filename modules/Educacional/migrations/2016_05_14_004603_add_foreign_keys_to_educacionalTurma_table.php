<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducacionalTurmaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('educacional.turma', function(Blueprint $table)
		{
			$table->foreign('ano_semestre_id', 'foreign_key_semestre_pk')->references('id')->on('educacional.ano_semestre')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('curso_id', 'foreign_key_curso_id')->references('id')->on('educacional.curso')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('periodo_id', 'foreign_key_periodo_fk')->references('id')->on('educacional.periodo')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('educacional.turma', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_semestre_pk');
			$table->dropForeign('foreign_key_curso_id');
			$table->dropForeign('foreign_key_periodo_fk');
		});
	}

}
