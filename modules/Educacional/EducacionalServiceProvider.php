<?php

namespace Educacional;

use Educacional\Models\Aluno;
use Educacional\Models\Endereco;
use Illuminate\Support\ServiceProvider;
use Educacional\Models\MatrizCurricular;
use Nucleo\Models\Pessoa;

class EducacionalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishMigrations();
    }

    public function register()
    {
        $this->app->bind('educ.model.matriz_curricular', MatrizCurricular::class);
        $this->app->bind('models.endereco', Endereco::class);
        $this->app->bind('nucleo.pessoa', Pessoa::class);
        $this->app->bind('educacional.aluno', Aluno::class);
        $this->app->bind('educacional.alunoTurma', Aluno::class);
    }

    public function publishMigrations()
    {
        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
