<?php

namespace Educacional\Models;

use App\Models\AttributesMutator;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Educacional\Models\Curso;

/**
 * @SWG\Definition(
 *      definition="MatrizCurricular",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="versao",
 *          description="versao",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="hora_total",
 *          description="hora_total",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="horas_complementar",
 *          description="horas_complementar",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="atual",
 *          description="atual",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="excluido",
 *          description="excluido",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="curso_id",
 *          description="curso_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class MatrizCurricular extends Model
{
    use SoftDeletes;
    use AttributesMutator;

    public $table = 'educacional.matriz_curricular';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'versao',
        'hora_total',
        'horas_complementar',
        'atual',
        'curso_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'versao' => 'integer',
        'hora_total' => 'integer',
        'horas_complementar' => 'integer',
        'atual' => 'boolean',
        'curso_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function setCursoIdAttribute($cursoId)
    {
        $this->attributes['curso_id'] = $this->nullIfBlank($cursoId);
    }

    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }
}
