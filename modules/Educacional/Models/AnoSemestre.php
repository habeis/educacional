<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="AnoSemestre",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ano_semestre",
 *          description="ano_semestre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="data_inicio",
 *          description="data_inicio",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="data_fim",
 *          description="data_fim",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="obs",
 *          description="obs",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="atual",
 *          description="atual",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="coligada_tipo_curso_id",
 *          description="coligada_tipo_curso_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class AnoSemestre extends Model
{
    use SoftDeletes;

    public $table = 'educacional.ano_semestre';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'ano_semestre',
        'data_inicio',
        'data_fim',
        'obs',
        'atual'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ano_semestre' => 'string',
        'data_inicio' => 'date',
        'data_fim' => 'date',
        'obs' => 'string',
        'atual' => 'boolean',
        'coligada_tipo_curso_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    
}
