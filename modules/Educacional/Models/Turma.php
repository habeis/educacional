<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Turma",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nome",
 *          description="nome",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="classe",
 *          description="classe",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="periodo_id",
 *          description="periodo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="maximo_aluno",
 *          description="maximo_aluno",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ano_semestre_id",
 *          description="ano_semestre_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Turma extends Model
{
    use SoftDeletes;

    public $table = 'educacional.turma';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome',
        'classe',
        'periodo_id',
        'maximo_aluno',
        'ano_semestre_id',
        'curso_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'classe' => 'string',
        'periodo_id' => 'integer',
        'maximo_aluno' => 'integer',
        'ano_semestre_id' => 'integer',
        'curso_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function periodo()
    {
        return $this->belongsTo('Educacional\Models\Periodo');
    }

    public function curso()
    {
        return $this->belongsTo('Educacional\Models\Curso');
    }

    public function anoSemestre()
    {
        return $this->belongsTo('Educacional\Models\AnoSemestre');
    }
    public function turmaDisciplina()
    {
        return $this->hasMany('Educacional\Models\TurmaDisciplina');
    }
}
