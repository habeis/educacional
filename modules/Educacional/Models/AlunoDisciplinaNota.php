<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
     * @SWG\Definition(
     *      definition="AlunoDisciplinaNota",
     *      required={},
     *      @SWG\Property(
     *          property="id",
     *          description="id",
     *          type="integer",
     *          format="int32"
     *      ),
 *      @SWG\Property(
 *          property="modelo_nota_id",
 *          description="modelo_nota_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="aluno_turma_disciplina_id",
 *          description="aluno_turma_disciplina_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class AlunoDisciplinaNota extends Model
{
    use SoftDeletes;

    public $table = 'educacional.aluno_disciplina_nota';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'aluno_disciplina_id',
        'modelo_nota_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'aluno_disciplina_id' => 'integer',
        'modelo_nota_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
