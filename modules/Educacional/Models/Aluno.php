<?php

namespace Educacional\Models;

use App\Models\Auth\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Aluno",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tipo_curso_id",
 *          description="tipo_curso_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="escola_conclusao",
 *          description="escola_conclusao",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tipo_escola_conclusao",
 *          description="tipo_escola_conclusao",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ano_conclusao",
 *          description="ano_conclusao",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cidade_conclusao",
 *          description="cidade_conclusao",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ativo",
 *          description="ativo",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="excluido",
 *          description="excluido",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Aluno extends Model
{
    use SoftDeletes;

    public $table = 'educacional.aluno';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tipo_curso_id',
        'escola_conclusao',
        'tipo_escola_conclusao',
        'ano_conclusao',
        'cidade_conclusao',
        'ativo',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tipo_curso_id' => 'integer',
        'escola_conclusao' => 'string',
        'tipo_escola_conclusao' => 'integer',
        'ano_conclusao' => 'integer',
        'cidade_conclusao' => 'string',
        'ativo' => 'boolean',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
