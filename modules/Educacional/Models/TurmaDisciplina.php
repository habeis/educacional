<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="TurmaDisciplina",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nome_disciplina",
 *          description="nome_disciplina",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="turma_id",
 *          description="turma_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="matrriz_curricular_disciplina_id",
 *          description="matrriz_curricular_disciplina_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class TurmaDisciplina extends Model
{
    use SoftDeletes;

    public $table = 'educacional.turma_disciplina';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome_disciplina',
        'turma_id',
        'matrriz_curricular_disciplina_id',
        'created',
        'modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome_disciplina' => 'string',
        'turma_id' => 'integer',
        'matrriz_curricular_disciplina_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function turma()
    {
        return $this->belongsTo(Turma::class);
    }

    public function matrizCurricularDisciplina()
    {
        return $this->belongsTo(MatrizCurricularDisciplina::class, 'matrriz_curricular_disciplina_id', 'id');
    }
    public function professorDisciplina(){
        return $this->belongsToMany(Professor::class,'educacional.professor_disciplina','turma_disciplina_id','professor_id')->withTimestamps();
    }
}
