<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="AlunoTurmaDisciplina",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="aluno_turma_id",
 *          description="aluno_turma_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="turma_disciplina_id",
 *          description="turma_disciplina_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class AlunoTurmaDisciplina extends Model
{
    use SoftDeletes;

    public $table = 'educacional.aluno_turma_disciplina';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'aluno_turma_id',
        'turma_disciplina_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'aluno_turma_id' => 'integer',
        'turma_disciplina_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
