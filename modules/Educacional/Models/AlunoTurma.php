<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="AlunoTurma",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="turma_id",
 *          description="turma_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="aluno_id",
 *          description="aluno_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="matricula",
 *          description="matricula",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="forma_ingresso_id",
 *          description="forma_ingresso_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="data_ingresso",
 *          description="data_ingresso",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="status_id",
 *          description="status_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class AlunoTurma extends Model
{
    use SoftDeletes;

    public $table = 'educacional.aluno_turma';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'turma_id',
        'aluno_id',
        'matricula',
        'forma_ingresso_id',
        'data_ingresso',
        'status_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'turma_id' => 'integer',
        'aluno_id' => 'integer',
        'matricula' => 'integer',
        'forma_ingresso_id' => 'integer',
        'data_ingresso' => 'date',
        'status_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function aluno()
    {
        return $this->belongsTo(Aluno::class);
    }

    public function turma()
    {
        return $this->belongsTo(Turma::class);
    }
}
