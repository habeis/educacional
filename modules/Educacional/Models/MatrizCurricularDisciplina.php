<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="MatrizCurricularDisciplina",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="matriz_curricular_id",
 *          description="matriz_curricular_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="disciplina_id",
 *          description="disciplina_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="codigo_disciplina",
 *          description="codigo_disciplina",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nome_disciplina",
 *          description="nome_disciplina",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="periodo_id",
 *          description="periodo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="credito",
 *          description="credito",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="credito_valor_id",
 *          description="credito_valor_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pre_requisito",
 *          description="pre_requisito",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="carga_horaria",
 *          description="carga_horaria",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tipo_disciplina_id",
 *          description="tipo_disciplina_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class MatrizCurricularDisciplina extends Model
{
    use SoftDeletes;

    public $table = 'educacional.matriz_curricular_disciplina';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    
    protected $dates = ['deleted_at'];


    public $fillable = [
        'matriz_curricular_id',
        'disciplina_id',
        'codigo_disciplina',
        'nome_disciplina',
        'periodo_id',
        'credito',
        'credito_valor_id',
        'pre_requisito',
        'carga_horaria',
        'tipo_disciplina_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'matriz_curricular_id' => 'integer',
        'disciplina_id' => 'integer',
        'codigo_disciplina' => 'string',
        'nome_disciplina' => 'string',
        'periodo_id' => 'integer',
        'credito' => 'integer',
        'credito_valor_id' => 'integer',
        'pre_requisito' => 'boolean',
        'carga_horaria' => 'integer',
        'tipo_disciplina_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function disciplina()
    {
        return $this->belongsTo('Educacional\Models\Disciplina');
    }

    public function creditoValor()
    {
        return $this->belongsTo('Educacional\Models\CreditoValor');
    }

    public function matrizCurricular()
    {
        return $this->belongsTo('Educacional\Models\MatrizCurricular');
    }

    public function tipoDisciplina()
    {
        return $this->belongsTo('Educacional\Models\TipoDisciplina');
    }

    public function periodo()
    {
        return $this->belongsTo('Educacional\Models\Periodo');
    }
}
