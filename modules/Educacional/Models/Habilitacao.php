<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Habilitacao",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="descricao",
 *          description="descricao",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nome",
 *          description="nome",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="decreto_lei_resolucao",
 *          description="decreto_lei_resolucao",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ativo",
 *          description="ativo",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="excluido",
 *          description="excluido",
 *          type="boolean"
 *      )
 * )
 */
class Habilitacao extends Model
{
    use SoftDeletes;

    public $table = 'educacional.habilitacao';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'descricao',
        'nome',
        'decreto_lei_resolucao',
        'ativo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'descricao' => 'string',
        'nome' => 'string',
        'decreto_lei_resolucao' => 'string',
        'ativo' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
