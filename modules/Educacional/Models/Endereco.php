<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nucleo\Models\Pessoa;

/**
 * @SWG\Definition(
 *      definition="Endereco",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="logradouro",
 *          description="logradouro",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="numero",
 *          description="numero",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cep",
 *          description="cep",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="bairro",
 *          description="bairro",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cidade",
 *          description="cidade",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="uf",
 *          description="uf",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="complemento",
 *          description="complemento",
 *          type="string"
 *      )
 * )
 */
class Endereco extends Model
{
    use SoftDeletes;

    public $table = 'endereco';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'logradouro',
        'numero',
        'cep',
        'bairro',
        'cidade',
        'uf',
        'complemento',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'logradouro' => 'string',
        'numero' => 'integer',
        'cep' => 'string',
        'bairro' => 'string',
        'cidade' => 'string',
        'uf' => 'string',
        'complemento' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'uf' => 'required|max:2',
        'cep' => 'required|max:15',
        'bairro' => 'required|max:100',
        'complemento' => 'required|max:100',
        'logradouro' => 'required|max:255'
    ];

    public function pessoa()
    {
        return $this->hasMany(Pessoa::class);
    }
}
