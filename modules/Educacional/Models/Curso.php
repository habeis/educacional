<?php

namespace Educacional\Models;

use Educacional\Models\MatrizCurricular;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Educacional\Models\Coordenacao;
use Educacional\Models\Modalidade;
use Educacional\Models\Portaria;
use Educacional\Models\Turno;

/**
 * @SWG\Definition(
 *      definition="Curso",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nome",
 *          description="nome",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="quantidade_periodo",
 *          description="quantidade_periodo",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="turno_id",
 *          description="turno_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="horario_turno",
 *          description="horario_turno",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coordenacao_id",
 *          description="coordenacao_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="habilitacao_id",
 *          description="habilitacao_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="coligada_tipo_curso_id",
 *          description="coligada_tipo_curso_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="modalidade_id",
 *          description="modalidade_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="inep_id",
 *          description="inep_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="obs",
 *          description="obs",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ativo",
 *          description="ativo",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="modo_curso_id",
 *          description="modo_curso_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="portaria_id",
 *          description="portaria_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Curso extends Model
{
    use SoftDeletes;

    public $table = 'educacional.curso';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome',
        'quantidade_periodo',
        'turno_id',
        'horario_turno',
        'coordenacao_id',
        'habilitacao_id',
        'coligada_tipo_curso_id',
        'modalidade_id',
        'inep_id',
        'obs',
        'ativo',
        'modo_curso_id',
        'portaria_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'quantidade_periodo' => 'integer',
        'turno_id' => 'integer',
        'horario_turno' => 'string',
        'coordenacao_id' => 'integer',
        'habilitacao_id' => 'integer',
        'coligada_tipo_curso_id' => 'integer',
        'modalidade_id' => 'integer',
        'inep_id' => 'integer',
        'obs' => 'string',
        'ativo' => 'boolean',
        'modo_curso_id' => 'integer',
        'portaria_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        "habilitacao_id" => 'required|required_with:field_b',
        "modo_curso_id" => 'required',
        "modalidade_id" => 'required',
        "coordenacao_id" => 'required',
        "turno_id" => 'required',
        "nome" => 'required'
    ];

    public function modalidade()
    {
        return $this->belongsTo(Modalidade::class);
    }

    public function turno()
    {
        return $this->belongsTo(Turno::class);
    }

    public function habilitacao()
    {
        return $this->belongsTo(Habilitacao::class);
    }

    public function modoCurso()
    {
        return $this->belongsTo(ModoCurso::class);
    }

    public function coordenacao()
    {
        return $this->belongsTo(Coordenacao::class);
    }

    public function portaria()
    {
        return $this->belongsTo(Portaria::class);
    }

    public function matrizCurricular()
    {
        return $this->hasMany(MatrizCurricular::class, "curso_id", "id");
    }
}
