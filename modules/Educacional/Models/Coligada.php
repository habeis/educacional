<?php

namespace Educacional\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Coligada",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nome",
 *          description="nome",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cnpj",
 *          description="cnpj",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="razao_social",
 *          description="razao_social",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefone",
 *          description="telefone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="obs",
 *          description="obs",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="endereco_id",
 *          description="endereco_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Coligada extends Model
{
    
    use SoftDeletes;

    public $table = 'educacional.coligada';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome',
        'cnpj',
        'razao_social',
        'telefone',
        'obs',
        'endereco_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'cnpj' => 'string',
        'razao_social' => 'string',
        'telefone' => 'string',
        'obs' => 'string',
        'endereco_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required|max:255'
    ];

    public function endereco()
    {
        return $this->belongsTo(Endereco::class);
    }

    public function tipoCursos()
    {
        return $this->belongsToMany(TipoCurso::class, "educacional.coligada_tipo_curso")->withTimestamps();
    }
}
