<?php

namespace Educacional\Models;

use App\Models\Auth\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Professor",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="graduacao",
 *          description="graduacao",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ativo",
 *          description="ativo",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="substituto",
 *          description="substituto",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="visitante",
 *          description="visitante",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Professor extends Model
{
    use SoftDeletes;

    public $table = 'educacional.professor';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'graduacao',
        'ativo',
        'substituto',
        'visitante',
        'deleted_at',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'graduacao' => 'integer',
        'ativo' => 'boolean',
        'substituto' => 'boolean',
        'visitante' => 'boolean',
        'deleted_at' => 'datetime',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function TurmaDisciplina(){
        return $this->belongsToMany(TurmaDisciplina::class,'educacional.professor_disciplina','professor_id','turma_disciplina_id')->withTimestamps();
    }
}
