<?php

use Illuminate\Database\Seeder;
use Educacional\Models\TipoDisciplina;

class TipoDisciplinaSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.tipo_disciplina")->delete();
        foreach ($this->getTipoDisciplina() as $tipoDisciplina) {
            TipoDisciplina::create($tipoDisciplina);
        }
    }

    public function getTipoDisciplina()
    {
        return [
            [
                'descricao' => 'Núcleo Orientado'
            ],
            [
                'descricao' => 'Obrigatória'
            ],
            [
                'descricao' => 'Optativa'
            ]
        ];
    }
}
