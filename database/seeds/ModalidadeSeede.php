<?php

use Illuminate\Database\Seeder;
use \Educacional\Models\Modalidade;

class ModalidadeSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.modalidade")->delete();
        foreach ($this->getModalidade() as $modalidade) {
            Modalidade::create($modalidade);
        }
    }


    private function getModalidade()
    {
        return [
            [
                'descricao' => 'BACHARELADO',
                'ativo' => true

            ],
            [
                'descricao' => 'LICENCIATURA',
                'ativo' => true

            ],
            [
                'descricao' => 'TECNOLOGO',
                'ativo' => true

            ],
            [
                'descricao' => 'ENSINO MEDIO',
                'ativo' => true

            ],
        ];
    }
}
