<?php

use Illuminate\Database\Seeder;
use Educacional\Models\Portaria;

class PortariaSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.portaria")->delete();
        foreach ($this->getPortaria() as $portaria) {
            Portaria::create($portaria);
        }
    }


    private function getPortaria()
    {
        return [
            [
                'descricao' => 'Portaria 2015 0092',
                'ativo' => true
            ],
            [
                'descricao' => 'Portaria 2014 0092',
                'ativo' => true
            ],
            [
                'descricao' => 'Portaria 2013 0092',
                'ativo' => true
            ],
            [
                'descricao' => 'Portaria 20121 0092',
                'ativo' => true
            ],
            [
                'descricao' => 'Portaria 20178 0092',
                'ativo' => true
            ]
        ];
    }
}
