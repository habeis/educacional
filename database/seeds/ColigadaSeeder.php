<?php

use Illuminate\Database\Seeder;
use Educacional\Models\Coligada;
use Educacional\Models\TipoCurso;
class ColigadaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.coligada")->delete();
        foreach ($this->getColigadas() as $coligada) {
            $coligada = Coligada::create($coligada);

            $tipoCurso = TipoCurso::first();

            $coligada->tipoCursos()->save($tipoCurso, ['ativo' => true]);
        }
    }

    private function getColigadas()
    {
        return [
            [
                'nome' => 'Faculdade ITOP',
                'cnpj' => '090.090.0001/22',
                'razao_social' => 'SM Barbosa',
                'telefone' => '3212.0290',
                'endereco_id' => '1',
            ]
        ];
    }
    private function getTipoCurso($coligadaId)
    {

        return [
            [
                'tipo_curso_id' => '1',
                'coligada_id' => $coligadaId,
                'ativo' => true,
            ]
        ];
    }
}
