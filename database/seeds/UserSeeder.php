<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->delete();

        foreach ($this->getUser() as $user) {
            User::create($user);
        }
    }

    private function getUser()
    {
        return [
            [
                'name'      => 'Administrador',
                'email'     => 'admin@gmail.com',
                'password'  => '123456',
            ]
        ];
    }
}
