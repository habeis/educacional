<?php

use Illuminate\Database\Seeder;
use \Educacional\Models\Turno;

class TurnoSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.turno")->delete();
        foreach ($this->getTurno() as $turno) {
            Turno::create($turno);
        }
    }

    private function getTurno()
    {
        return [
            [
                'descricao' => 'Matutino',
                'ativo' => true

            ],
            [
                'descricao' => 'Vespertino',
                'ativo' => true

            ],
            [
                'descricao' => 'Noturno',
                'ativo' => true

            ],
            [
                'descricao' => 'Integral',
                'ativo' => true

            ],
        ];
    }
}
