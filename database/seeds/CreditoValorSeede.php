<?php

use Illuminate\Database\Seeder;
use \Educacional\Models\CreditoValor;

class CreditoValorSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.credito_valor")->delete();
        foreach ($this->getCreditoValor() as $creditoValor) {
            CreditoValor::create($creditoValor);
        }
    }


    private function getCreditoValor()
    {
        return [
            [
                'valor' => '18.00'
            ],
            [
                'valor' => '22.00'
            ],
            [
                'valor' => '25.00'
            ],
            [
                'valor' => '26.00'
            ],
            [
                'valor' => '32.00'
            ]
        ];
    }
}
