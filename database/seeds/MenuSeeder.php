<?php

use Illuminate\Database\Seeder;
use Menu\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \DB::statement('ALTER TABLE '  . $table .  ' DISABLE TRIGGER ALL;');
//        \DB::table($table)->delete();
        \DB::table("nucleo.menu")->delete();
//        \DB::statement('ALTER TABLE '  . $table . ' ENABLE TRIGGER ALL;');

        foreach ($this->getMenu() as $menu) {
            Menu::create($menu);
        }
    }

    private function getMenu()
    {
        return [
            [
                'title'     => 'Administrativo',
                'icon_name' => 'fa fa-desktop',
                'menu_order' => 1,
                'is_active' => true,
                'is_root'   => true
            ],
            [
                'title'     => 'Usuários',
                'route'     => 'admin.users.index',
                'menu_order' => 2,
                'parent_id' => 1,
                'is_active' => true,
                'is_root'   => false
            ],
            [
                'title'     => 'Perfis',
                'route'     => 'admin.roles.index',
                'menu_order' => 3,
                'parent_id' => 1,
                'is_active' => true,
                'is_root'   => false
            ],
            [
                'title'     => 'Privilégios',
                'route'     => 'admin.permissions.index',
                'menu_order' => 4,
                'parent_id' => 1,
                'is_active' => true,
                'is_root'   => false
            ],
            [
                'title'     => 'Menu',
                'route'     => 'admin.menus.index',
                'menu_order' => 1,
                'parent_id' => 1,
                'is_active' => true,
                'is_root'   => false
            ],
            [
                'title'     => 'Auditoria',
                'route'     => 'admin.logs.index',
                'menu_order' => 5,
                'parent_id' => 1,
                'is_active' => true,
                'is_root'   => false
            ],
            [
                'title'     => 'Cursos',
                'route'     => 'cursos.index',
                'menu_order' => 6,
                'parent_id' => 1,
                'is_active' => true,
                'is_root'   => false
            ]
        ];
    }
}
