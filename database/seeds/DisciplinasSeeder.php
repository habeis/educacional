<?php

use Illuminate\Database\Seeder;
use Educacional\Models\Disciplina;

class DisciplinasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.disciplina")->delete();
        foreach ($this->getDisciplinas() as $disciplina) {
            Disciplina::create($disciplina);
        }
    }


    private function getDisciplinas()
    {
        return [
            [
                'nome' => 'Metodologia Cientifica',
                'nome_abreviado' => 'Metodologia Cientifica'
            ],
            [
                'nome' => 'Algortmo',
                'nome_abreviado' => 'Algortmo'
            ],
            [
                'nome' => 'Lógica de Programação',
                'nome_abreviado' => 'L. Programação'
            ],
            [
                'nome' => 'Matematica 1',
                'nome_abreviado' => 'Matematica 1'
            ],
            [
                'nome' => 'Matematica 2',
                'nome_abreviado' => 'Matematica 2'
            ]
        ];
    }
}
