<?php

use Illuminate\Database\Seeder;
use Educacional\Models\Coordenacao;

class CoordenacaoSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.coordenacao")->delete();
        foreach ($this->getCoordenacao() as $coordenacao) {
            Coordenacao::create($coordenacao);
        }
    }


    private function getCoordenacao()
    {
        return [
            [
                'nome' => 'Coordenação de Administração',
                'telefone' => '3312-2209',
                'ramal' => '',
                'coligada_id' => '1',
            ],
            [
                'nome' => 'Coordenação de Ciências da Computação',
                'telefone' => '3312-2209',
                'ramal' => '',
                'coligada_id' => '1',
            ],
            [
                'nome' => 'Coordenação de Ciências Contábeis',
                'telefone' => '3312-2209',
                'ramal' => '',
                'coligada_id' => '1',
            ],
            [
                'nome' => 'Coordenação de Pedagogia',
                'telefone' => '3312-2209',
                'ramal' => '',
                'coligada_id' => '1',
            ],
            [
                'nome' => 'Coordenação de Letras',
                'telefone' => '3312-2209',
                'ramal' => '',
                'coligada_id' => '1',
            ]
        ];
    }
}
