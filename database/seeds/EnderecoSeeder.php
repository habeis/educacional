<?php

use Illuminate\Database\Seeder;
use Educacional\Models\Endereco;
class EnderecoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("nucleo.endereco")->delete();
        foreach ($this->getEnderecos() as $endereco) {
            Endereco::create($endereco);
        }
    }

    private function getEnderecos()
    {
        return [
            [
                'logradouro' => '120 sul',
                'numero' => '22',
                'cep' => '77000-090',
                'bairro' => 'centro',
                'cidade' => 'palmas',
                'uf' => 'to',
                'complemento' => 'proximo ao extra',
            ]
        ];
    }
}
