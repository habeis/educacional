<?php

use Illuminate\Database\Seeder;
use Educacional\Models\ModoCurso;

class ModoCursoSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.modo_curso")->delete();
        foreach ($this->getModoCurso() as $modoCurso) {
            ModoCurso::create($modoCurso);
        }
    }


    private function getModoCurso()
    {
        return [
            [
                'descricao' => 'Presencial',
                'ativo' => true

            ],
            [
                'descricao' => 'Distância',
                'ativo' => true

            ],
        ];
    }
}
