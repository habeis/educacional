<?php

use Illuminate\Database\Seeder;
use Educacional\Models\Periodo;
class PeriodoSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.periodo")->delete();

        foreach ($this->getPeriodo() as $periodo) {
            Periodo::create($periodo);
        }
    }


    private function getPeriodo()
    {
        return [
            [
                'descricao' => '1° Período',
                'numero_periodo' => '1'

            ],
            [
                'descricao' => '2° Período',
                'numero_periodo' => '2'

            ],
            [
                'descricao' => '3° Período',
                'numero_periodo' => '3'

            ],
            [
                'descricao' => '4° Período',
                'numero_periodo' => '4'

            ],
            [
                'descricao' => '5° Período',
                'numero_periodo' => '5'

            ],
            [
                'descricao' => '6° Período',
                'numero_periodo' => '6'

            ],
            [
                'descricao' => '7° Período',
                'numero_periodo' => '7'

            ],
            [
                'descricao' => '8° Período',
                'numero_periodo' => '8'

            ],
            [
                'descricao' => 'Optativa',
                'numero_periodo' => '20'

            ],
        ];
    }
}
