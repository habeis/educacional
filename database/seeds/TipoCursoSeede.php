<?php

use Illuminate\Database\Seeder;
use Educacional\Models\TipoCurso;

class TipoCursoSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.tipo_curso")->delete();
        foreach ($this->getTipoCurso() as $tipoCurso) {
            TipoCurso::create($tipoCurso);
        }
    }

    private function getTipoCurso()
    {
        return [
            [
                'descricao' => 'Ensino Médio',
                'obs' => 'Cursos do ensino Médio '
            ],
            [
                'descricao' => 'Técnico',
                'obs' => 'Cursos Técnicos '

            ],
            [
                'descricao' => 'Graduação',
                'obs' => 'Cursos de Graduação'

            ],
            [
                'descricao' => 'Pós-Graduação',
                'obs' => 'Cursos de pós-graduação '

            ],
            [
                'descricao' => 'Mestrado',
                'obs' => 'Cursos atualização em cursos superiores '

            ],
        ];
    }
}
