<?php

use Illuminate\Database\Seeder;
use \Educacional\Models\AnoSemestre;

class AnoSemestresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("educacional.ano_semestre")->delete();

        foreach ($this->getAnoSemestre() as $anoSemestre) {
            AnoSemestre::create($anoSemestre);
        }
    }

    private function getAnoSemestre()
    {
        return [
            [
                'ano_semestre' => '2016/01',
                'data_inicio' => '2016-01-01',
                'data_fim' => '2016-08-01',
                'atual' => true,
                'coligada_tipo_curso_id' => 1

            ],
            /*[
                'ano_semestre' => '2016/02',
                'data_inicio' => '2016-08-01',
                'data_fim' => '2016-12-31',
                'atual' => true,
                'coligada_tipo_curso_id' => 2

            ],
            [
                'ano_semestre' => '2017/01',
                'data_inicio' => '2017-01-01',
                'data_fim' => '2016-08-01',
                'atual' => true,
                'coligada_tipo_curso_id' => 3

            ]*/
        ];
    }

}
